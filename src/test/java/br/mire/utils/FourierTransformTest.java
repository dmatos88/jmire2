package br.mire.utils;

import static org.junit.Assert.*;

import org.junit.Test;

import br.mire.utils.math.FourierTransform;

public class FourierTransformTest {

	@Test
	public void testDivision() {
		//test of 1 divided by j
				
		double[] j = {2.0,1.0};

		double[] r = FourierTransform.divide1byComplex(j);

		double[] testResult = {0.4,-0.2};
		assertArrayEquals(r, testResult, Math.ulp(0.0001)); 
	}

	@Test
	public void testExp() {

		//test exp(j*2*pi*x)

		double[] r = FourierTransform.expComplex(2*Math.PI*1.1);

		double[] testResult = {0.8090169943749471,0.587785};
		assertArrayEquals(testResult, r, 0.0001);
	}
	
	@Test
	public void testComplete() {
		double[] complex = FourierTransform.expComplex(2*Math.PI*1.1);
		double[] r = FourierTransform.divide1byComplex(complex);
		double[] testResult = {0.8090,-0.5878};
		assertArrayEquals(r, testResult, 0.0001);		
	}

}
