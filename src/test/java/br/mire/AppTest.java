package br.mire;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.mire.utils.FourierTransformTest;

//JUnit Suite Test
@RunWith(Suite.class)

@Suite.SuiteClasses({ 
	FourierTransformTest.class
})

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigourous Test :-)
     */
	@Test
    public void testApp() {
        assertTrue( true );        
    }

}
