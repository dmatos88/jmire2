package br.mire.ml.classifier.generic;

import java.util.List;

import org.openimaj.image.FImage;
import org.openimaj.image.MBFImage;

import br.mire.segmentation.contour.ComplexSignalContour2;

public interface JMIReClassifier {

	//public void loadModel(String raw_features_filepath, String normalized_features_filepath, String model_filepath, String raw_k, String raw_n) throws Exception;
	
	public double[] getFeaturesVector(MBFImage image, FImage mask, ComplexSignalContour2 contourSignal, double scale) throws Exception;
	
	public List<String> getFeaturesHeader();
}
