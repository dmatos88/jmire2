package br.mire.ml.classifier.tcruzi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openimaj.image.FImage;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.Transforms;
import org.openimaj.image.connectedcomponent.ConnectedComponentLabeler;
import org.openimaj.image.pixel.ConnectedComponent;
import org.openimaj.image.pixel.Pixel;

import br.mire.features.extraction.CCMFeatures;
import br.mire.features.extraction.CCMFeatures.CCMFeaturesStrategy;
import br.mire.features.extraction.ColourFeatures;
import br.mire.features.extraction.ColourFeatures.ColourFeaturesStrategy;
import br.mire.features.extraction.GeometricFeatures;
import br.mire.features.extraction.GeometricFeatures.GeometricFeaturesStrategy;
import br.mire.features.extraction.HuMomentsFeatures;
import br.mire.features.extraction.HuMomentsFeatures.HuMomentsStrategy;
import br.mire.ml.classifier.generic.JMIReClassifier;
import br.mire.segmentation.contour.ComplexSignalContour2;
import br.mire.segmentation.controllers.LocalThresholdingController;
import br.mire.utils.image.HistogramEq;
import br.mire.utils.image.PreprocessField;
import br.mire.utils.math.Coord2D;
import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.ASSearch;
import weka.attributeSelection.AttributeSelection;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.LibSVM;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.WekaPackageManager;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Standardize;

public class TCruzi_Tripomastigote_Giemsa implements JMIReClassifier, Serializable{

	static Logger log = Logger.getLogger(TCruzi_Tripomastigote_Giemsa.class.getName());


	static final long serialVersionUID = 5646541L;
	
	private int num_of_features;
	private double[] means;
	private double[] stdDevs;
	private double[] min_values;
	private double[] max_values;
	private List<String> csv_header;

	public static double last_scale = 0.07;

	private static boolean model_loaded = false;

	AttributeSelection attribteSelection;
	private Classifier autowekaClassifier;

	private LibSVM classifierLibSVM;

	//thresholding vars
	//private static double[] min_values;
	//private static double[] max_values;

	//private static double[] k_min_values;
	//private static double[] k_max_values;

	//private static  double[] n_min_values;
	//private static double[] n_max_values;

	private static final String class_header = "structure"; 

	private static GeometricFeaturesStrategy[] geomStrategies = {
			GeometricFeaturesStrategy.MINOR_AXIS,
			//
			GeometricFeaturesStrategy.MAJOR_AXIS,
			GeometricFeaturesStrategy.AREA,
			GeometricFeaturesStrategy.PARIMETER,
			GeometricFeaturesStrategy.CIRCULARITY,
			GeometricFeaturesStrategy.THICKNESS_RATIO,
			GeometricFeaturesStrategy.ASPECT_RATIO,
			GeometricFeaturesStrategy.MAJOR_AXIS_PERIMETER_RATIO,
			GeometricFeaturesStrategy.MAX_DIST_BOUNDARY_2_CENTROID,
			GeometricFeaturesStrategy.MIN_DIST_BOUNDARY_2_CENTROID,
			GeometricFeaturesStrategy.MEAN_DIST_BOUNDARY_2_CENTROID,
			GeometricFeaturesStrategy.BILATERAL_SYMMETRY
	};

	private static CCMFeaturesStrategy[] ccmStrategies = {
			//*/
			CCMFeaturesStrategy.CCM_ENTROPY_LL,
			CCMFeaturesStrategy.CCM_ENTROPY_AA,
			CCMFeaturesStrategy.CCM_ENTROPY_BB,
			CCMFeaturesStrategy.CCM_ENTROPY_LA,
			CCMFeaturesStrategy.CCM_ENTROPY_LB,
			CCMFeaturesStrategy.CCM_ENTROPY_AB,
			CCMFeaturesStrategy.CCM_ASM_LL,
			CCMFeaturesStrategy.CCM_ASM_AA,
			CCMFeaturesStrategy.CCM_ASM_BB,
			CCMFeaturesStrategy.CCM_ASM_LA,
			CCMFeaturesStrategy.CCM_ASM_LB,
			CCMFeaturesStrategy.CCM_ASM_AB,
			//CCMFeaturesStrategy.CCM_CONTRAST_LL, //aways zero
			//CCMFeaturesStrategy.CCM_CONTRAST_AA, //aways zero
			//CCMFeaturesStrategy.CCM_CONTRAST_BB, //aways zero
			CCMFeaturesStrategy.CCM_CONTRAST_LA,
			CCMFeaturesStrategy.CCM_CONTRAST_LB,
			CCMFeaturesStrategy.CCM_CONTRAST_AB,
			//CCMFeaturesStrategy.CCM_IDM_LL, //aways one
			//CCMFeaturesStrategy.CCM_IDM_AA, //aways one
			//CCMFeaturesStrategy.CCM_IDM_BB, /aways one
			CCMFeaturesStrategy.CCM_IDM_LA,
			CCMFeaturesStrategy.CCM_IDM_LB,
			CCMFeaturesStrategy.CCM_IDM_AB,
			CCMFeaturesStrategy.CCM_CORRELATION_LL,
			//*/
			CCMFeaturesStrategy.CCM_CORRELATION_AA,
			//*/
			CCMFeaturesStrategy.CCM_CORRELATION_BB,
			CCMFeaturesStrategy.CCM_CORRELATION_LA,
			CCMFeaturesStrategy.CCM_CORRELATION_LB,
			CCMFeaturesStrategy.CCM_CORRELATION_AB	
			//*/
	};

	private static HuMomentsStrategy[] huMomentsStrategies = {
			HuMomentsStrategy.MOMENT_1ST,
			HuMomentsStrategy.MOMENT_2ND,
			HuMomentsStrategy.MOMENT_3RD,
			HuMomentsStrategy.MOMENT_4TH,
			HuMomentsStrategy.MOMENT_5TH,
			HuMomentsStrategy.MOMENT_6TH,
			HuMomentsStrategy.MOMENT_7TH
	};

	private static ColourFeaturesStrategy[] colourStrategies = {
			ColourFeaturesStrategy.MEAN,
			ColourFeaturesStrategy.MEDIAN,
			ColourFeaturesStrategy.MODE,
			ColourFeaturesStrategy.RANGE,
			ColourFeaturesStrategy.VARIANCE
	};


	public TCruzi_Tripomastigote_Giemsa() {	
		initialize();
	}


	private void initialize() {

		log.setLevel(Level.ALL);

		num_of_features = 0;

		log.debug("Initializing ");



		log.debug("...INITIALIZING...");

		csv_header = new LinkedList<String>();

		for(CCMFeaturesStrategy s: ccmStrategies) {
			csv_header.add(s.getHeader());
			num_of_features++;
		}		


		for(GeometricFeaturesStrategy s: geomStrategies) {
			csv_header.add(s.getHeader());
			num_of_features++;
		}		

		for(ColourFeaturesStrategy s : colourStrategies) {
			csv_header.add(s.getHeader());
			num_of_features++;
		}

		for(HuMomentsStrategy s : huMomentsStrategies) {
			csv_header.add(s.getHeader());
			num_of_features++;
		}

		log.debug("# OF FEATURES: "+num_of_features);

		//adding class to header
		csv_header.add(class_header);

	}



	public double[] standardize_features(final double[] features) throws Exception{
		if(features == null || features.length < 1) {
			throw new Exception("Standardize error: features vector are either null or empty");
		}

		double[] std_features = new double[features.length];

		for(int i = 0; i < features.length; i++) {
			std_features[i] = features[i] - means[i];
			std_features[i] = std_features[i] / stdDevs[i];
		}
		return std_features;
	}


	public void saveInstaces(String filepath, Instances instances) throws IOException {
		ArffSaver saver = new ArffSaver();
		saver.setInstances(instances);
		saver.setFile(new File(filepath));
		saver.writeBatch();
	}

	public void loadData(String raw_tcruzi_features_filepath) throws Exception{

		String options_line = "-S 0 -K 2 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -W \"1.0 1.0 2.0\" -model data/ -seed 13 -num-decimal-places 8";
		String[] options = weka.core.Utils.splitOptions(options_line);

		log.debug("Loading model");

		WekaPackageManager.loadPackages( false, true, false );
		//Object obj = SerializationHelper.read(new FileInputStream(model_filepath));
		this.classifierLibSVM = new LibSVM();
		classifierLibSVM.setOptions(options);		

		CSVLoader loader = new CSVLoader();
		loader.setSource(new File(raw_tcruzi_features_filepath));
		Instances kn_structure = loader.getDataSet();

		//DataSource data_source = new DataSource(raw_tcruzi_features_filepath);

		//Instances kn_structure = data_source.getDataSet();	

		kn_structure.setClass(kn_structure.attribute("structure"));

		means = new double[kn_structure.numAttributes()];
		stdDevs = new double[kn_structure.numAttributes()];
		min_values = new double[kn_structure.numAttributes()];
		max_values = new double[kn_structure.numAttributes()];
		
		for(int i = 0; i < kn_structure.numAttributes()-1; i++) {
			means[i] = kn_structure.attributeStats(i).numericStats.mean;
			stdDevs[i] = kn_structure.attributeStats(i).numericStats.stdDev;
			min_values[i] = kn_structure.attributeStats(i).numericStats.min;
			max_values[i] = kn_structure.attributeStats(i).numericStats.max;
		}

		//

		model_loaded = true;

		Standardize filter = new Standardize();
		filter.setInputFormat(kn_structure);
		kn_structure = Filter.useFilter(kn_structure, filter);

		classifierLibSVM.buildClassifier(kn_structure);

		Evaluation evaluation = new Evaluation(kn_structure);
		evaluation.crossValidateModel(classifierLibSVM, kn_structure, 10, new Random(13));
		System.out.println(evaluation.toSummaryString());

		//evaluation.confusionMatrix();

		attribteSelection = new AttributeSelection();
		ASSearch asSearch = ASSearch.forName("weka.attributeSelection.GreedyStepwise", new String[]{"-C", "-B", "-R"});
		attribteSelection.setSearch(asSearch);
		ASEvaluation asEval = ASEvaluation.forName("weka.attributeSelection.CfsSubsetEval", new String[]{"-M"});
		attribteSelection.setEvaluator(asEval);
		attribteSelection.SelectAttributes(kn_structure);
		kn_structure = attribteSelection.reduceDimensionality(kn_structure);
		autowekaClassifier = AbstractClassifier.forName("weka.classifiers.lazy.LWL", new String[]{"-K", "120", "-A", "weka.core.neighboursearch.LinearNNSearch", "-W", "weka.classifiers.trees.RandomForest", "--", "-I", "31", "-K", "0", "-depth", "0"});
		autowekaClassifier.buildClassifier(kn_structure);

	}


	public FImage floodFillMaskFromOrigin(FImage mask) {
		List<Coord2D> pxlist = new LinkedList<Coord2D>();
		pxlist.add(new Coord2D(0,0));

		FImage newMask = new FImage(mask.getWidth(), mask.getHeight());
		newMask.fill(1.0f);

		Coord2D c;
		int x, y;
		while(!pxlist.isEmpty()) {
			c = pxlist.remove(0);
			x = (int) c.getX();
			y = (int) c.getY();
			if(mask.getPixel(x,y) == 0) {
				newMask.setPixel(x, y, 0f);
				mask.setPixel(x, y, 1f); //flag as visited
				for(int i = -1; i < 2; i++) {
					for(int j = -1; j < 2; j++) {
						if(x + i >= 0 && y + j >= 0 & x + i < mask.getWidth() && j + y < mask.getHeight()) {
							if(mask.getPixel(x+i, y+j) == 0) {
								pxlist.add(new Coord2D(x+i, y+j));
							}
						}
					}
				}
			}
		}

		return newMask;
	}

	public List<Prediction> classify_cropped(
			MBFImage field,
			double field_number,
			double objective_mag,
			double ocular_mag,
			String raw_features_filepath
			) throws Exception{

		long start = System.currentTimeMillis();

		if(!model_loaded) throw new Exception("Tcruzi Giemsa SVM model not loaded");

		DataSource source = new DataSource(raw_features_filepath);

		Instances structure = source.getDataSet();

		structure.setClass(structure.attribute("structure"));

		log.debug("# of classes "+structure.numClasses());
		log.debug("# of attrs "+structure.numAttributes());

		field = Transforms.RGB_TO_CIELabNormalised(field);

		FImage l_star = field.getBand(0).clone();
		PreprocessField processor = new PreprocessField();

		FImage thresh_sample = processor.offGanglion(l_star);
		thresh_sample.normalise();		

		FImage mask = new FImage(l_star.width, l_star.height); //processor.getFieldMask(l_star, 5);
		mask.fill(1f);

		/*
		if(useGUI) scale = JMIRe.getInstance().getScale(mask);
		else scale = appController.getScale(mask);
		 */

		double scale = 0.07;

		HistogramEq hist = new HistogramEq(101);

		//hist.processHistogram(l_star, mask);

		boolean isBimodal = hist.isBimodal(l_star, mask, 101);

		//System.out.println("Bimodal: "+isBimodal);

		if(isBimodal) {
			log.debug("Bimodal");
			thresh_sample = processor.localAdaptiveThresholdSauvola(thresh_sample, 0.5, 0.11, 15);
		} else {
			log.debug("Unimodal");
			thresh_sample = processor.localAdaptiveThresholdSauvola(thresh_sample, 0.5, 0.001, 29);
		}		
		thresh_sample.multiplyInplace(mask);

		log.debug("scale = "+scale+" um");

		log.debug("Thresholding done");

		//ImageUtilities.write(b_star_result, new File("result_threshold.jpg"));
		log.debug("Writing result threshold to result_threshold.jpg");


		ConnectedComponentLabeler cclabeler = new ConnectedComponentLabeler(
				ConnectedComponentLabeler.Algorithm.TWO_PASS,
				0.5f,
				ConnectedComponent.ConnectMode.CONNECT_8);	

		cclabeler.analyseImage(thresh_sample);

		List<ConnectedComponent>ccList = cclabeler.getComponents();	

		List<Prediction> predictions = new LinkedList<>();


		for(ConnectedComponent cc : ccList){

			if(cc.getPixels().size() >= 50) {

				MBFImage window = new MBFImage(l_star.width, l_star.height);
				mask = new FImage(l_star.width, l_star.height);

				Iterator<Pixel> pit = cc.getPixels().iterator();
				Pixel p;			

				while(pit.hasNext()) {
					p = pit.next();
					mask.setPixel(p.x, p.y, 1.0f);
					for(int i = 0; i < 3; i++)
						window.getBand(i).setPixel(p.x, p.y, field.getBand(i).getPixel(p));
				}

				Coord2D shiftPoint = new Coord2D(0, 0);

				predictions.addAll(this.execute(structure, window, mask, shiftPoint, scale));

			}

		}	


		List<Prediction> nlist = new LinkedList<>();
		List<Prediction> klist = new LinkedList<>();

		for(Prediction p: predictions) {
			switch(p.getStructure()) {
			case KINETOPLAST:
			case KINETOPLAST_CELL_BODY:
				klist.add(p);
				break;
			case NUCLEUS:
			case NUCLEUS_CELL_BODY:
				nlist.add(p);
				break;
			default:
				break;
			}
		}

		long total = System.currentTimeMillis() - start;

		log.debug("Field took :"+(total/1000.0)+"s");


		log.debug("# of predictions for K: "+klist.size());
		log.debug("# of predictions for N: "+nlist.size());


		return predictions;
	}

	public List<Prediction> classify_field_local_thresh(
			MBFImage field,
			double field_number,
			double objective_mag,
			double ocular_mag,
			String raw_features_filepath
			) throws Exception{

		long start = System.currentTimeMillis();

		if(!model_loaded) throw new Exception("Tcruzi Giemsa SVM model not loaded");

		DataSource source = new DataSource(raw_features_filepath);

		Instances structure = source.getDataSet();

		structure.setClass(structure.attribute("structure"));

		log.debug("# of classes "+structure.numClasses());
		log.debug("# of attrs "+structure.numAttributes());

		field = Transforms.RGB_TO_CIELabNormalised(field);

		FImage l_star = field.getBand(0).clone();
		PreprocessField processor = new PreprocessField();

		FImage thresh_sample = processor.offGanglion(l_star);
		thresh_sample.normalise();		

		FImage mask = processor.getFieldMask(l_star, 5);

		/*
		if(useGUI) scale = JMIRe.getInstance().getScale(mask);
		else scale = appController.getScale(mask);
		 */

		double scale = 0.07;

		HistogramEq hist = new HistogramEq(101);

		//hist.processHistogram(l_star, mask);

		boolean isBimodal = hist.isBimodal(l_star, mask, 101);

		//System.out.println("Bimodal: "+isBimodal);

		if(isBimodal) {
			log.debug("Bimodal");
			thresh_sample = processor.localAdaptiveThresholdSauvola(thresh_sample, 0.5, 0.11, 15);
		} else {
			log.debug("Unimodal");
			thresh_sample = processor.localAdaptiveThresholdSauvola(thresh_sample, 0.5, 0.001, 29);
		}		
		thresh_sample.multiplyInplace(mask);

		log.debug("scale = "+scale+" um");

		log.debug("Thresholding done");

		//ImageUtilities.write(b_star_result, new File("result_threshold.jpg"));
		log.debug("Writing result threshold to result_threshold.jpg");


		ConnectedComponentLabeler cclabeler = new ConnectedComponentLabeler(
				ConnectedComponentLabeler.Algorithm.TWO_PASS,
				0.5f,
				ConnectedComponent.ConnectMode.CONNECT_8);	

		cclabeler.analyseImage(thresh_sample);

		List<ConnectedComponent>ccList = cclabeler.getComponents();	

		List<Prediction> predictions = new LinkedList<>();

		int window_size = 400;
		int window_half_size = window_size/2;

		for(ConnectedComponent cc : ccList){

			if(cc.getPixels().size() >= 50) {

				MBFImage window = new MBFImage(window_size, window_size);
				mask = new FImage(window_size, window_size);

				Iterator<Pixel> pit = cc.getPixels().iterator();
				Pixel p = pit.next();
				int shift_x = (p.x - window_half_size);
				int shift_y = (p.y - window_half_size);			

				while(pit.hasNext()) {
					p = pit.next();

					mask.setPixel(p.x - shift_x, p.y - shift_y, 1.0f);
					for(int i = 0; i < 3; i++)
						window.getBand(i).setPixel(p.x - shift_x, p.y - shift_y, field.getBand(i).getPixel(p));
				}

				Coord2D shiftPoint = new Coord2D(shift_x, shift_y);

				predictions.addAll(this.execute(structure, window, mask, shiftPoint, scale));


			}

		}	


		List<Prediction> nlist = new LinkedList<>();
		List<Prediction> klist = new LinkedList<>();

		for(Prediction p: predictions) {
			switch(p.getStructure()) {
			case KINETOPLAST:
			case KINETOPLAST_CELL_BODY:
				klist.add(p);
				break;
			case NUCLEUS:
			case NUCLEUS_CELL_BODY:
				nlist.add(p);
				break;
			default:
				break;
			}
		}

		//FImage result = field.getBand(0);

		//String structure_predicted = "";

		long total = System.currentTimeMillis() - start;

		log.debug("Field took :"+(total/1000.0)+"s");


		log.debug("# of predictions for K: "+klist.size());
		log.debug("# of predictions for N: "+nlist.size());

		/*
		 * Features for K and N merged together with goedesic_distance
		 */

		/*
		DataSource source2  = new DataSource(raw_tcruzi_features_filepath);

		Instances structure2 = source2.getDataSet();
		structure2.setClass(structure2.attribute("class"));
		 */

		/*
		predictions.clear();
		for(Prediction k: klist) {
			for(Prediction n: nlist) {

				double dist = k.getPoint().getEuclideanDistance(n.getPoint()) * scale;
				double distThreshFloor = 2.0;
				double distThreshCeil = 8.0;
				//log.debug("Euclidean distance between "+k.getPoint().toString()+" and "+n.getPoint().toString()+" is "+dist);

				//TODO pq hardcoded? tem que haver um calculo correto
				if(distThreshFloor <= dist && dist <= distThreshCeil) {

					List<Node> minPath = new LinkedList<Node>();

					try {
						minPath = GeodesicDistance.getGeodesicPath(b_star_image, k.getSignalContour(), n.getSignalContour(), k.getPoint(), n.getPoint(), sigma);
					}catch(Exception e) {
						e.printStackTrace();
					}
					double geoDist = 0.0; 
					for(Node px : minPath) {				
						geoDist += px.dist_from_label;
					}

					geoDist *= scale;

					if(geoDist < 15.0) {					
						log.debug("Geodesic distance between "+k.getPoint().toString()+" and "+n.getPoint().toString()+" is "+geoDist);

						k.setProbability(geoDist);
						n.setProbability(geoDist);

						predictions.add(k);
						predictions.add(n);

					}

				}	
			}
		}
		//*/

		return predictions;
	}

	/*
	 * TODO reduzir tamanho de window antes de enviar para execute
	 * TODO como recuperar a posição correta depois???
	 * TODO como na busca pelo caminho de dijkstra
	 * 
	 * @param structure
	 * @param parameters 0 radius; 1 threshold; 2 sigma; 3 cutoff; 
	 * @param window
	 * @param raw_features_filepath
	 * @param normalized_features_filepath
	 * @param model_filepath
	 * @param ppi
	 * @param zoom
	 * @return
	 * @throws Exception
	 */
	public List<Prediction> execute(
			Instances structure,
			MBFImage window,
			FImage mask,
			Coord2D shiftPoint,
			double scale
			) throws Exception{


		List<Prediction> predictions = new LinkedList<>();

		//DisplayUtilities.display(window);
		//DisplayUtilities.display(mask);

		DenseInstance testInstance;	

		double[] std_features;
		double[] features;


		if(mask != null /*&& passThresholding*/){	
			//long start = System.currentTimeMillis();

			ComplexSignalContour2 contourSignal = new ComplexSignalContour2(mask, 0, 0);


			features = this.getFeaturesVector(window, mask, contourSignal, scale);		

			if(!this.threshold_raw_features(features)) return predictions;

			Coord2D centroid = GeometricFeatures.centroid(contourSignal);

			double prediction = 0;


			std_features = standardize_features(features);

			testInstance = new DenseInstance(1.0, std_features);

			testInstance.setDataset(structure);

			Instance autowekaInstance = attribteSelection.reduceDimensionality(testInstance);
			double prediction_autoweka = autowekaClassifier.classifyInstance(autowekaInstance);

			double prediction_svm = classifierLibSVM.classifyInstance(testInstance);

			prediction = prediction_autoweka;

			log.debug("svm: "+prediction_svm);
			log.debug("autoweka "+prediction_autoweka);

			Prediction.Structure struct = Prediction.Structure.KINETOPLAST;

			if(prediction == 0) {
				struct = Prediction.Structure.KINETOPLAST_CELL_BODY;
			} else if (prediction == 1) {
				struct = Prediction.Structure.KINETOPLAST;
			} else if(prediction == 2) {
				struct = Prediction.Structure.NUCLEUS_CELL_BODY;
			} else if(prediction == 3) {
				struct = Prediction.Structure.NUCLEUS;
			} else if(prediction == 4) {
				struct = Prediction.Structure.TCRUZI;
			} else if(prediction == 5) {
				struct = Prediction.Structure.ARTIFACT;
			}

			Prediction p = new Prediction(centroid, struct, contourSignal, shiftPoint);				
			log.debug(struct.toString()+": "+p.getPoint().toString());

			String features_str = "";

			for(double f : std_features) {
				features_str = features_str + ";" + f;
			}

			log.debug(features_str);

			p.setFeatures(features);
			predictions.add(p);					
		} 

		return predictions;
	}	

	public boolean threshold_raw_features(double[] features) {
		double min, max;

		for(int i = 0; i < features.length; i++) {
			if(features[i] < min_values[i] || features[i] > max_values[i]) return false;
		}
		return true;
	}

	public double[] getFeaturesVector(MBFImage image, FImage mask, ComplexSignalContour2 contourSignal, double scale) 
			throws Exception{

		double[] features = new double[num_of_features];

		if(mask == null) return null;		

		//log.debug("scale = " + scale);
		MBFImage workImage = image.clone();

		//set all pixels outside the field to 0
		for(int j = 0; j < workImage.numBands(); j++) {
			workImage.getBand(j).multiplyInplace(mask);		
		}

		CCMFeatures ccm = new CCMFeatures(workImage, mask, ccmStrategies);

		int i = 0;

		double[] ccmFeaturesVec = ccm.getFeaturesValues();
		for(int ccmCounter = 0; ccmCounter < ccmFeaturesVec.length; ccmCounter++) {
			features[i++] = ccmFeaturesVec[ccmCounter];
		}

		GeometricFeatures geomFeatures = new GeometricFeatures(mask, geomStrategies, contourSignal, scale);

		double[] geomFeaturesVec = geomFeatures.getFeaturesValues();

		for(int j = 0; j < geomFeaturesVec.length; j++) {
			features[i++] = geomFeaturesVec[j];
		}	

		ColourFeatures colourFeatures = new ColourFeatures(colourStrategies, mask, workImage);
		double[] colourFeaturesVec = colourFeatures.getFeaturesValues();

		for(int j = 0; j < colourFeaturesVec.length; j++) {
			features[i++] = colourFeaturesVec[j];
		}

		HuMomentsFeatures huFeatures = new HuMomentsFeatures(huMomentsStrategies, mask);		

		double[] huFeaturesVec = huFeatures.getFeaturesValues();

		for(int j = 0; j < huFeaturesVec.length; j++) {
			features[i++] = huFeaturesVec[j];
		}

		return features;
	}

	public List<String> getFeaturesHeader() {
		return csv_header;
	}

	public double readCentroidDistances(String kinetoplast_filepath, String nucleus_filepath) throws IOException {

		double max_dist = 0.0;

		BufferedReader kbr = new BufferedReader(new FileReader(kinetoplast_filepath));
		BufferedReader nbr = new BufferedReader(new FileReader(nucleus_filepath));

		//skip first line of each file (title)
		kbr.readLine();
		nbr.readLine();

		String linek, linen;
		while ((linek = kbr.readLine()) != null && (linen = nbr.readLine()) != null) {
			String[] kcoords = linek.split(LocalThresholdingController.csv_separator);
			String[] ncoords = linen.split(LocalThresholdingController.csv_separator);

			double kx = new Double(kcoords[0]);
			double ky = new Double(kcoords[1]);
			double nx = new Double(ncoords[0]);
			double ny = new Double(ncoords[1]);

			Coord2D k = new Coord2D(kx, ky);
			Coord2D n = new Coord2D(nx, ny);
			max_dist = Double.max(max_dist, k.getEuclideanDistance(n));
		}

		kbr.close();
		nbr.close();

		return max_dist;		
	}


	public void writeResult(String img_filename, String structure, int tp, int fp, int tn, int fn) throws IOException{

		String csv_filename = "jmire_result.csv";
		String csv_separator = ";";

		File csv = new File(csv_filename);

		if (!csv.exists()) {

			PrintWriter writer = new PrintWriter(csv_filename, "UTF-8");
			String[] header = {"filename", "structure", "count", "tp",  "fp", "tn", "fn"};
			//System.out.print(header[0]);
			writer.append(header[0]);
			writer.flush();
			for(int i = 1; i < header.length; i++) {
				writer.append(csv_separator + header[i]);
				writer.flush(); 
			}             
			writer.close();
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(csv_filename, true));

		writer.append("\n"+img_filename+csv_separator+structure);
		writer.flush();

		writer.append(csv_separator + tp + csv_separator + fp
				+ csv_separator + tn + csv_separator + fn);
		writer.flush();
		writer.close();

	}

	public double getScaleFromMicroscopicField_LStar(
			FImage img,
			double ocular_magnification, 
			double objective_magnification, 
			double field_number) throws Exception {

		img.threshold(0.5f);

		//DisplayUtilities.display(img);

		int x = 0;
		int y = img.height/2;

		ComplexSignalContour2 contourSignal = new ComplexSignalContour2(img, x, y);

		double[] axes = GeometricFeatures.principalAxis(contourSignal);

		//major axis
		//log.debug(axes[0]);
		//minor axis
		//log.debug(axes[1]);


		double real_field_diameter = field_number / objective_magnification;
		double pixel_side_in_mm = real_field_diameter / (axes[0]);
		double pixel_side_in_um = pixel_side_in_mm * objective_magnification * ocular_magnification;

		//log.debug("Pixel side: "+pixel_side_in_um+"um");
		//double mice_rbc_diameter_in_px = 81;
		//log.debug("Mice RBC diameter: "+mice_rbc_diameter_in_px * pixel_side_in_um);
		return pixel_side_in_um;

	}
}
