package br.mire.ml.classifier.tcruzi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.renderer.RenderHints;
import org.openimaj.image.typography.FontStyle;
import org.openimaj.image.typography.general.GeneralFont;
import org.openimaj.math.geometry.shape.Circle;

import br.mire.ml.classifier.tcruzi.Prediction.Structure;
import br.mire.segmentation.controllers.LocalThresholdingController;
import br.mire.utils.math.Coord2D;

public class TCruziValidation {

	class ValidationItem{
		public String filename;
		public Coord2D coord;
		public String structure;
		
		public ValidationItem(String filename, Coord2D coord, String structure) {
			this.filename = filename;
			this.coord = coord;
			this.structure = structure;
		}
	}

	static Logger log = Logger.getLogger(TCruziValidation.class.getName());

	private int true_positive;
	private int false_positive;
	private int false_negative;
	private int true_negative;

	private double scale = 0.07;

	private final double  max_dist_boundary_2_centroid = 20.0; //micrometers

	public static HashMap<String, List<ValidationItem>> positions_map = null;

	private final String output_dir = "data/output/";

	public TCruziValidation(
			String groundTruthFilepath,
			double scale) throws IOException {

		log.setLevel(Level.DEBUG);

		true_positive = 0;
		true_negative = 0;
		false_positive = 0;
		false_negative = 0;

		this.scale = scale;

		if(positions_map == null) {
			readGroundTruthFile(groundTruthFilepath);
			//this.printGroundTruth();
		}
	}
	
	//default constructor
	public TCruziValidation(){}

	public void writePredictionsToImage(String outputdir, String filename, List<Prediction> predictions, MBFImage sourceImage) throws Exception{
		log.debug("Writing image for validation of "+filename);
		
		MBFImage result = sourceImage.clone();

		String structure_predicted = "";

		for(Prediction p : predictions) {

			switch(p.getStructure()) {
			case ARTIFACT:
				structure_predicted = "A";
				break;
			case KINETOPLAST:
				structure_predicted = "K";
				break;
			case KINETOPLAST_CELL_BODY:
				structure_predicted = "KB";
				break;
			case NUCLEUS:
				structure_predicted = "N";
				break;
			case NUCLEUS_CELL_BODY:
				structure_predicted = "NB";
				break;
			case TCRUZI:
				structure_predicted = "T";
				break;
			default:
				result.drawShape(new Circle((float)p.getPoint().getX(), (float)p.getPoint().getY(), 100f), 8, RGBColour.GREEN);
				break;

			}

			final FontStyle<Float> gfs = new GeneralFont("Bank Gothic", 4).createStyle(
					result.getBand(1).createRenderer(RenderHints.ANTI_ALIASED));
			gfs.setColour(1.0f);

			result.getBand(1).drawText(structure_predicted, (int)p.getPoint().getX(), (int)p.getPoint().getY(), gfs);


			gfs.setColour(0.0f);
			result.getBand(0).drawText(structure_predicted, (int)p.getPoint().getX(), (int)p.getPoint().getY(), gfs);
			result.getBand(2).drawText(structure_predicted, (int)p.getPoint().getX(), (int)p.getPoint().getY(), gfs);

		}
		//*/

		//DisplayUtilities.display(result).setSize(1000, 1600);

		log.debug("writing file "+output_dir+"result_"+filename);
		ImageUtilities.write(result, new File(outputdir+"result_"+filename));
		log.debug("Done");
	}


	private void readGroundTruthFile(String filepath) throws IOException {

		log.debug("Reading file "+filepath+" ...");

		if(positions_map == null) {
			positions_map = new HashMap<String, List<ValidationItem>>();		

			BufferedReader reader = new BufferedReader(new FileReader(filepath));


			String line;

			//skip header
			reader.readLine();

			while ((line = reader.readLine()) != null) {
				String[] data = line.split(LocalThresholdingController.csv_separator);

				String filename = data[0];
				double x = new Double(data[1]);
				double y = new Double(data[2]);
				String structure = data[3];

				List<ValidationItem> itens_list = positions_map.get(filename);
				if(itens_list == null) itens_list = new LinkedList<ValidationItem>();
				itens_list.add(new ValidationItem(filename, new Coord2D(x, y), structure));
				positions_map.put(filename, itens_list);
			}
			reader.close();
		}	

		log.debug("DONE");
	}

	public void validatePredictions(TCruzi_Tripomastigote_Giemsa giemsa, String filename, List<Prediction> predictions, boolean printImg, MBFImage image) throws IOException {
		String csv_separator = LocalThresholdingController.csv_separator;

		log.debug("Validating "+predictions.size() +" predictions for "+filename);

		for(Prediction p : predictions) {		

			log.debug("Validating prediction for point "+p.getPoint().toString());

			if(validate(filename, p.getPoint(), p.getStructure())) {

				String structure_str = p.getStructure().toString();

				String csv_filename = structure_str+"_features.csv";


				BufferedWriter writer = new BufferedWriter(new FileWriter(output_dir+csv_filename, true));

				//log.debug("Valid "+ n_or_k +" ["+p.getPoint().getX()+";"+p.getPoint().getY()+"] with values");
				writer.append("\n"+p.getFeatures()[0]);
				for(int i = 1; i < p.getFeatures().length; i++) {
					double d = p.getFeatures()[i];
					writer.append(csv_separator+d);
					//System.out.print(csv_separator+d);
				}				
				writer.append(csv_separator+structure_str);
				writer.flush();
				writer.close();
				
				this.true_positive += 1;
			}
			else {

				List<ValidationItem> validations_list = new LinkedList<>();

				//it must not be near any K or N to be an artifact
				validations_list.addAll(positions_map.get(filename));

				boolean artifact = true;

				for(ValidationItem item : validations_list) {
					double dist = item.coord.getEuclideanDistance(p.getPoint()) * scale; 
					if(dist < max_dist_boundary_2_centroid) {
						artifact = false;
						break;
					}
				}

				if(artifact) {
					Structure artifact_structure = Structure.ARTIFACT;
					String csv_filename = artifact_structure.title+".csv";
					BufferedWriter writer = new BufferedWriter(new FileWriter(output_dir+csv_filename, true));

					log.debug("writing artifact");

					//write header
					if (!new File(csv_filename).exists()) {
						List<String> header = giemsa.getFeaturesHeader();
						writer.write(header.get(0));
						for(int i = 1; i < header.size(); i++) {
							writer.append(LocalThresholdingController.csv_separator+header.get(i));
						}
						writer.flush();
					}					
					//log.debug("Valid "+ n_or_k +" ["+p.getPoint().getX()+";"+p.getPoint().getY()+"] with values");
					writer.append("\n"+p.getFeatures()[0]);
					for(int i = 1; i < p.getFeatures().length; i++) {
						double d = p.getFeatures()[i];
						writer.append(csv_separator+d);
						//System.out.print(csv_separator+d);
					}				
					writer.append(csv_separator+artifact_structure.title);
					writer.flush();
					writer.close();

				}
				
				this.false_positive += 1;
			}
		} 

		if(printImg && image != null) {
			this.printValidationFile(filename, image, predictions);
		}

		this.false_negative = positions_map.get(filename).size() - this.true_positive;
		
		log.debug("Done for "+filename);
	}

	private void printValidationFile(String filename, MBFImage result, List<Prediction> predictions) throws IOException{

		log.debug("Writing image for validation of "+filename);

		String structure_predicted = "";

		for(Prediction p : predictions) {

			switch(p.getStructure()) {
			case ARTIFACT:
				structure_predicted = "A";
				break;
			case KINETOPLAST:
				structure_predicted = "K";
				break;
			case KINETOPLAST_CELL_BODY:
				structure_predicted = "KB";
				break;
			case NUCLEUS:
				structure_predicted = "N";
				break;
			case NUCLEUS_CELL_BODY:
				structure_predicted = "NB";
				break;
			case TCRUZI:
				structure_predicted = "T";
				break;
			default:
				result.drawShape(new Circle((float)p.getPoint().getX(), (float)p.getPoint().getY(), 100f), 8, RGBColour.GREEN);
				break;

			}

			final FontStyle<Float> gfs = new GeneralFont("Bank Gothic", 4).createStyle(
					result.getBand(1).createRenderer(RenderHints.ANTI_ALIASED));
			gfs.setColour(1.0f);

			result.getBand(1).drawText(structure_predicted, (int)p.getPoint().getX(), (int)p.getPoint().getY(), gfs);


			gfs.setColour(0.0f);
			result.getBand(0).drawText(structure_predicted, (int)p.getPoint().getX(), (int)p.getPoint().getY(), gfs);
			result.getBand(2).drawText(structure_predicted, (int)p.getPoint().getX(), (int)p.getPoint().getY(), gfs);

		}
		//*/

		//DisplayUtilities.display(result).setSize(1000, 1600);

		log.debug("writing file "+output_dir+"result_"+filename);
		ImageUtilities.write(result, new File(output_dir+"result_"+filename));
		log.debug("Done");
	}

	private boolean validate(String filename, Coord2D predicted_coord, Structure structure) {


		boolean tp = false;
		
		List<ValidationItem> validation_items = positions_map.get(filename);
		
		for(ValidationItem item : validation_items) {
			if(item.coord.getEuclideanDistance(predicted_coord) * scale < max_dist_boundary_2_centroid) {
				//TODO remover após coletar ARTIFACTS, serve apenas para garantir que nao vai pegar nada próximo aos pontos de interesse
				tp = true;
				if(item.structure.equalsIgnoreCase(structure.title)){
					return true;
				}
			}
		}
		
		
		return tp;
	}

	public int getTruePositiveCount() {
		return this.true_positive;
	}

	public int getTrueNegativeCount() {
		return this.true_negative;
	}

	public int getFalsePositiveCount() {
		return this.false_positive;
	}

	public int getFalseNegativeCount() {
		return this.false_negative;
	}

	public static void main(String args[]) throws Exception{
		/*
		String groundTruthFilepath = "ground_truth.csv";
		double ppi = 72;
		double zoom = 1000;
		try {
			TCruziValidation validation = new TCruziValidation(groundTruthFilepath, ppi, zoom);
		} catch (IOException e) {
			e.printStackTrace();
		}
		 */

		/*
		Coord2D a = new Coord2D(2088, 584);
		Coord2D b = new Coord2D(686, 403);

		log.debug(a.getEuclideanDistance(b));
		log.debug(a.getEuclideanDistance(b)/498.75);

		Coord2D c = new Coord2D(1156, 412);

		log.debug(a.getEuclideanDistance(b));
		log.debug(a.getEuclideanDistance(c)/334.4);
		 */
		MBFImage image = ImageUtilities.readMBF(new File("/home/dmatos/samples_fields/field[004]count[3].jpg"));
		ImageUtilities.write(image, new File("teste.jpg"));
		System.exit(0);
	}

}
