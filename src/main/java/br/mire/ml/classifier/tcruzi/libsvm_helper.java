package br.mire.ml.classifier.tcruzi;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;

public class libsvm_helper {
	
	private svm_model model = null;
	
	public svm_node[][] prepareData(double[][] data){
		svm_node[][] nodes = new svm_node[data.length][data[0].length];
		
		for(int i = 0; i < data.length; i++) {
			for(int j = 0; j < data[0].length; j++) {
				nodes[i][j] = new svm_node();
				nodes[i][j].index = j;
				nodes[i][j].value = data[i][j];
			}
		}
		return nodes;
	}
	
	public svm_node[] prepareInstance(double[] data) {
		svm_node[] node = new svm_node[data.length];
		for(int j = 0; j < data.length; j++) {
			node[j] = new svm_node();
			node[j].index = j;
			node[j].value = data[j];
		}
		return node;
	}
	

	public svm_model buildModel(svm_node[][] nodes, double gamma, double nu, double eps) {
	    // Build Parameters
	    svm_parameter param = new svm_parameter();
	    param.svm_type    = svm_parameter.ONE_CLASS;
	    param.kernel_type = svm_parameter.RBF;
	    param.eps 		  = eps;
	    param.gamma       = gamma;
	    param.nu          = nu;
	    param.cache_size  = 100;

	    // Build Problem
	    svm_problem problem = new svm_problem();
	    problem.x = nodes;
	    problem.l = nodes.length;
	    problem.y = prepareY(nodes.length);

	    // Build Model	    
	    String check = svm.svm_check_parameter(problem, param);
	    System.err.println(check);
	    this.model = svm.svm_train(problem, param);
	    return this.model;
	}
	
	private double[] prepareY(int size) {
	    double[] y = new double[size];

	    for (int i=0; i < size; i++)
	        y[i] = 1;

	    return y;
	}
	
	public double predict(svm_node[] node) {
		return svm.svm_predict(model, node);
	}

}
