package br.mire.ml.classifier.tcruzi;

import br.mire.segmentation.contour.ComplexSignalContour2;
import br.mire.utils.math.Coord2D;


public class Prediction implements Comparable<Prediction>{
	
	private static double radius_in_pixel  = 1;
	private Coord2D point = null;
	private ComplexSignalContour2 contour;
	private Structure structure = null;
	private Double[] features = null;
	private double probability;

	public enum Structure{
		ARTIFACT("UNKNOWN"),
		KINETOPLAST("KINETOPLAST"),		
		NUCLEUS("NUCLEUS"),
		KINETOPLAST_CELL_BODY("KINETOPLAST_CELL_BODY"),
		NUCLEUS_CELL_BODY("NUCLEUS_CELL_BODY"),
		TCRUZI("T_CRUZI");
		
		String title;
		
		Structure(String title){
			this.title = title;
		}
		
		
		public String toString() {
			return title;
		}
	}
	
	public Prediction() {}

	public Prediction(Coord2D point, Structure s, ComplexSignalContour2 contour, Coord2D shiftPoint) {
		this.point = new Coord2D(point.getX()+shiftPoint.getX(), point.getY()+shiftPoint.getY());
		this.structure = s;
		this.contour = contour;
		for(int i = 0; i < contour.getXSignal().length; i++) {
			this.contour.getXSignal()[i] = contour.getXSignal()[i]+shiftPoint.getX();
			this.contour.getYSignal()[i] = contour.getYSignal()[i]+shiftPoint.getY();
		}
		probability = 0;
	}
	
	public ComplexSignalContour2 getSignalContour() {
		return this.contour;
	}
	
	public void setProbability(double p) {
		this.probability = p;
	}
	
	public double getProbability() {
		return this.probability;
	}
	
	public void setFeatures(double[] features) {
		this.features = new Double[features.length];
		for(int i = 0; i < features.length; i++) {
			this.features[i] = features[i];
		}
	}
	
	public Double[] getFeatures() {
		return this.features;
	}
	

	public Coord2D getPoint() {
		return this.point;
	}

	public Structure getStructure() {
		return this.structure;
	}

	@Override
	public int compareTo(Prediction o) {
		if(this.structure == o.structure &&
				this.point.getEuclideanDistance(o.getPoint()) <= radius_in_pixel) {
			return 0;
		}
		return 1;
	}
	
	
}