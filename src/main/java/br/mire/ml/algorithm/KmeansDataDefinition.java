package br.mire.ml.algorithm;

public class KmeansDataDefinition {
	
	static final long serialVersionUID = 5646542L;
	
	private double[][] centers;
	private int dist_to_use = 1;
	
	public KmeansDataDefinition() {
		//centers = null;
	}
	
	public double[][] getCenters() {
		System.out.println("returning centers of length: "+centers.length);
		return centers;
	}
	
	/**
	 * 
	 * @param data
	 * @param dist_type 1 for euclidean, 2 for manhattan
	 * @return
	 */
	public double returnMinDist2Centers(double[] data) {		
		if(centers == null) {
			System.out.println("KmeansDataDefinition not build yet.");
			return Double.NaN;
		}
		if(data == null) {
			System.out.println("null data");
			return Double.NaN;
		}
		if(data.length != centers[0].length) {
			System.out.println("data length of "+data.length+"doesn't match model of legnth "+centers[0].length);
			return Double.NaN;
		}
		double minDist = Double.MAX_VALUE;
		for(int i = 0; i < centers.length; i++) {
			if(dist_to_use == 1) {
				double dist = Double.min(euclidean_dist(centers[i], data), minDist);
				minDist = Double.isNaN(dist)?minDist:dist;
			} else if(dist_to_use == 2) {
				double dist = Double.min(manhattan_dist(centers[i], data), minDist);
				minDist = Double.isNaN(dist)?minDist:dist;
			} 
			
		}
		//System.out.println("returning min dist of "+minDist+" to any of "+centers.length+" centers");
		return minDist;
	}
	
	private double euclidean_dist(double[] point_a, double[] point_b) {

		if(point_a.length != point_b.length) {
			System.out.println(point_a.length+" is not equals "+point_b.length);
			return Double.NaN;
		}
		
		double dist = 0;
		
		for(int i = 0; i < point_a.length; i++) {
			dist += ((point_a[i]-point_b[i])*(point_a[i]-point_b[i]));
		}
		
		return Math.sqrt(dist);
	}
	
	private double manhattan_dist(double[] point_a, double[] point_b) {
		if(point_a.length != point_b.length) {
			System.out.println(point_a.length+" is not equals "+point_b.length);
			return Double.NaN;
		}
		
		double dist = 0;
		
		for(int i = 0; i < point_a.length; i++) {
			dist += (Math.abs(point_b[i]-point_a[i]));
		}		
		return dist;
	}
	
	private double[][] clusterize(double[][] centers, int[] data_2_center, double[][] dataset){
		
		for(int i = 0; i < dataset.length; i++) {
			double old_dist = Double.MAX_VALUE;	
			
			for(int j = 0; j < centers.length; j++) {
				double new_dist = 0;
				if(dist_to_use == 1 && (new_dist = euclidean_dist(dataset[i], centers[j]) ) < old_dist )  {
					data_2_center[i] = j;
					old_dist = new_dist;
				}
				else if(dist_to_use == 2 && (new_dist = manhattan_dist(dataset[i], centers[j]) ) < old_dist )  {
					data_2_center[i] = j;
					old_dist = new_dist;
				}
			}
		}
		
		double[][] new_centers = new double[centers.length][centers[0].length];
		for(int i = 0; i < centers.length; i++) {
			int counter = 0;
			for(int j = 0; j < data_2_center.length; j++) {
				if(data_2_center[j] == i) {
					for(int m = 0; m < new_centers[i].length; m++) {
						new_centers[i][m] += dataset[j][m]; 
					}
					counter++;
				}
			}
			for(int m = 0; m < new_centers[i].length; m++) {
				new_centers[i][m] /= (double)counter; 
			}
		}		
		return new_centers;
	}
	
	public String toString() {
		if(centers == null) return "Kmeans not built yet"; 
		return "Kmeans with "+centers.length+" centers";
	}
	
	public  int hashCode() {
		return 123456789;
	}
	
	public boolean check_for_convergence(double[][] old_centers, double[][] new_centers, double error) {
		for(int i = 0; i < old_centers.length; i++) {
			if(dist_to_use == 1 && euclidean_dist(old_centers[i], new_centers[i]) > error) return false;
			else if(dist_to_use == 2 && manhattan_dist(old_centers[i], new_centers[i]) > error) return false;
		}
		return true;
	}
	
	public void build(int K, final double[][] dataset, int dist_type) {
		this.dist_to_use = dist_type;
		centers = new double[K][dataset[0].length];	
		int[] data_2_center = new int[dataset.length];	
		
		for(int i = 0; i < K; i++) {
			int center_index = (int)(Math.random()*dataset.length);
			centers[i] = dataset[center_index]; 
		}
				
		double error = 0.001;
		double[][] new_centers = centers;
		do {			
			centers = new_centers;
			new_centers = clusterize(centers, data_2_center, dataset);			
		}while(!check_for_convergence(centers, new_centers, error));
		centers = new_centers;
	}
}
