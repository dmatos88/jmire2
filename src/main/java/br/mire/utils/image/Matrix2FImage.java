package br.mire.utils.image;

import org.openimaj.image.FImage;

import com.frank.math.Complex;

public class Matrix2FImage {

	public static FImage matrix2Image(Complex[][] matrix2) {
		FImage img2 = new FImage(matrix2.length, matrix2[0].length);

		for(int i = 0; i < matrix2.length; i++) {
			for(int j = 0; j < matrix2[0].length; j++) {
				img2.setPixel(i, j, (float) matrix2[i][j].real);
			}
		}

		return img2;
	}
	
	public static FImage matrix2Image(double[][] matrix2) {
		FImage img2 = new FImage(matrix2.length, matrix2[0].length);

		for(int i = 0; i < matrix2.length; i++) {
			for(int j = 0; j < matrix2[0].length; j++) {
				img2.setPixel(i, j, (float) matrix2[i][j]);
			}
		}

		return img2;
	}
	
	public static FImage matrix2Image(int[][] matrix2) {
		FImage img2 = new FImage(matrix2.length, matrix2[0].length);

		for(int i = 0; i < matrix2.length; i++) {
			for(int j = 0; j < matrix2[0].length; j++) {
				img2.setPixel(i, j, (float) matrix2[i][j]);
			}
		}

		return img2;
	}
	
}
