package br.mire.utils.image;

import java.io.File;
import java.io.IOException;

import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.processing.convolution.FGaussianConvolve;

public class RGB2Gray {

	private CIELab[][] imageLab; 
	private double[][] graph;
	private double[][][] quant;
	private MBFImage  image;

	public RGB2Gray(MBFImage  img) {
		this.image = img.clone();
		this.imageLab = new CIELab[image.getWidth()][image.getHeight()];
		this.quant = new double[100][256][256];
		this.graph = new double[100][100]; //L* max value
	}

	public FImage convert() {

		int width = image.getWidth();
		int height = image.getHeight();

		double X = 0.0;
		double Y = 0.0;
		double Z = 0.0;
		double u_line = 0.0;
		double v_line = 0.0;
		double theta = 0.0;
		double qTheta = 0.0; 
		double suv = 0.0;
		double Y_div_Yn = 0.0;
		double La = Math.pow(20, 0.4495);
		final double KBr = 0.2717 * ( (6.469+6.362*La) / (6.469+La));
		double temp = 0.0;

		/*  SMPTE-C RGB // D65 white
		 * 	CIE chromaticity coordinates:
		 */ 
		double xn = 0.312713;
		double yn = 0.329016;
		// CIE lumninance
		double Yn = 100.0;

		double u_line_n = 4.0*xn / (-2.0*xn + 12.0*yn + 3.0);
		double v_line_n = 9.0*yn / (-2.0*xn + 12.0*yn + 3.0);

		double _L_star = 0.0;
		double _L_star_VAC = 0.0;
		double u_star = 0.0;
		double v_star = 0.0;

		FImage grayImg = new FImage(image.getWidth(), image.getHeight());

		for(int x = 0; x < image.getWidth(); x++) {
			System.out.println(x);
			for(int y = 0; y < image.getHeight(); y++) {
				float red = image.getBand(0).getPixel(x,y).floatValue();
				float green = image.getBand(1).getPixel(x,y).floatValue();
				float blue = image.getBand(2).getPixel(x,y).floatValue();
				/*
				 * First we convert from RGB to XYZ, where
				 *  X = 0.412453*R + 0.35758 *G + 0.180423*B
				 *  Y = 0.212671*R + 0.71516 *G + 0.072169*B
				 *  Z = 0.019334*R + 0.119193*G + 0.950227*B
				 */
				X = 0.412453 * red + 0.35758 * green + 0.180423 * blue;
				Y = 0.212671 * red + 0.71516 * green + 0.072169 * blue;
				Z = 0.019334 * red + 0.119193 * green + 0.950227 * blue;

				/*
				 * Then, from XYZ to CIELUV
				 */
				u_line = 4.0 * X / (X+15.0*Y+3*Z);
				v_line = 9.0 * Y / (X+15.0*Y+3*Z);

				Y_div_Yn = Y/Yn; 
				if(Y_div_Yn > 0.008856 )
					_L_star = 116.0 * Math.pow(Y_div_Yn, 1.0/3.0) - 16.0;
				else
					_L_star = 903.296296296 * Y_div_Yn;

				theta = 1.0 / Math.tan((v_line - v_line_n) / (u_line - u_line_n));
				qTheta = -0.01585
						-0.03017 * Math.cos(theta) - 0.04556 * Math.cos(2*theta)
						-0.02667 * Math.cos(3*theta) - 0.00295 * Math.cos(4*theta)
						+0.14592 * Math.sin(theta) + 0.05084 * Math.sin(2*theta)
						-0.01900 * Math.sin(3*theta) - 0.00764 * Math.sin(4*theta);

				suv = 13.0 * Math.sqrt(((u_line-u_line_n)*(u_line-u_line_n))+((v_line-v_line_n)*(v_line-v_line_n)));


				_L_star_VAC = _L_star + (-0.1340 * qTheta + 0.0872 * KBr) * suv * _L_star;

				//u_star = 13.0 * _L_star_VAC * (u_line - u_line_n);
				//v_star = 13.0 * _L_star_VAC * (v_line - v_line_n);

				/*
				 * CIELUV back to XYZ
				 */
				if(_L_star_VAC <= 8.0) {
					temp = (3.0/29.0);
					Y = Yn * _L_star_VAC * ( temp * temp * temp);
				} else {
					temp = (_L_star_VAC + 16.0)/ 116.0;
					Y = Yn * (temp * temp * temp);
				}
				//				
				//				u_line = u_star / (13.0 * _L_star_VAC);
				//				v_line = v_star / (13.0 * _L_star_VAC);
				//				
				//				X = Y * ((9*u_line) / (4*v_line));
				//				Z = Y * ((12 - 3*u_line-20*v_line) / 4*v_line );
				/*
				 * Finally XYZ to grayscale with gama correction and contrast adjustment
				 */
				//System.out.println("x = "+x+" y = "+y);
				//System.out.println(Y);
				grayImg.setPixel(x, y, new Float(Y));

			}
		}

		return grayImg;
	}

	private double ft(double t) {

		if(t > 0.008856) {
			return Math.pow(t, 1.0/3.0);
		} else {
			return t*7.787+16.0/116.0;
		}
	}

	private double sRGBcorrection(double c) {
		if(c  < 0.03928) return c/12.92;
		else return Math.pow(((0.055+c)/1.055), 2.4);
	}
	
	
	public FImage convert2() {
		double X = 0.0;
		double Y = 0.0;
		double Z = 0.0;

		//Illuminant D65
		double Xn = 0.95047;
		double Yn = 1.000;
		double Zn = 1.08833;

		FImage grayImg = new FImage(image.getWidth(), image.getHeight());

		double maxL, maxA, maxB, minL, minA, minB;
		maxL = maxA = maxB = Double.MIN_VALUE;
		minL = minA = minB = Double.MAX_VALUE;

		for(int x = 0; x < image.getWidth(); x++) {
			for(int y = 0; y < image.getHeight(); y++) {
				double red = sRGBcorrection(image.getBand(0).getPixel(x,y).floatValue());
				double green = sRGBcorrection(image.getBand(1).getPixel(x,y).floatValue());
				double blue = sRGBcorrection(image.getBand(2).getPixel(x,y).floatValue());
				/*
				 * First we convert from sRGB to XYZ, where
				 *  0.4124564  0.3575761  0.1804375
 			 	 *	0.2126729  0.7151522  0.0721750
 				 *	0.0193339  0.1191920  0.9503041
				 */
				X = 0.4124564 * red + 0.3575761 * green + 0.1804375 * blue;
				Y = 0.2126729 * red + 0.7151522 * green + 0.0721750 * blue;
				Z = 0.0193339 * red + 0.1191920 * green + 0.9503041 * blue;
				
				this.imageLab[x][y] = new CIELab();

				this.imageLab[x][y].setL_star(116.0 * ft(Y/Yn) - 16.0);
				grayImg.setPixel(x, y, (float)(this.imageLab[x][y].getL_star()));
				this.imageLab[x][y].setA_star(500.0 * (ft(X/Xn)- ft(Y/Yn)));
				this.imageLab[x][y].setB_star(200.0 * (ft(Y/Yn) - ft(Z/Zn)));
				
				this.quant[(int)Math.round(this.imageLab[x][y].getL_star())]
						  [(int)Math.round(this.imageLab[x][y].getA_star() + 128)]
						  [(int)Math.round(this.imageLab[x][y].getB_star() + 128)] += 1.0;

				if(this.imageLab[x][y].getL_star() > maxL) maxL = imageLab[x][y].getL_star();
				if(this.imageLab[x][y].getL_star() < minL) minL = imageLab[x][y].getL_star();
				if(this.imageLab[x][y].getA_star() > maxA) maxA = imageLab[x][y].getA_star();
				if(this.imageLab[x][y].getA_star() < minA) minA = imageLab[x][y].getA_star();
				if(this.imageLab[x][y].getB_star() > maxB) maxB = imageLab[x][y].getB_star();
				if(this.imageLab[x][y].getB_star() < minB) minB = imageLab[x][y].getB_star();	
				
			}
		}
		
		for(int x = 0; x < image.getWidth(); x++) {
			for(int y = 0; y < image.getHeight(); y++) {
				float l = grayImg.getPixel(x, y).floatValue();
				grayImg.setPixel(x, y, l/(float)maxL);
			}
		}

		System.out.println("(minL,maxL) = ("+minL+","+maxL+")");
		System.out.println("(minA,maxA) = ("+minA+","+maxA+")");
		System.out.println("(minB,maxB) = ("+minB+","+maxB+")");

		return grayImg;
	}
	
	public FImage logOfGreen() {
		int width = image.getWidth();
		int height = image.getHeight();
		FImage newImage = image.getBand(1);
		
		FGaussianConvolve gauss = new FGaussianConvolve(1f);
		gauss.processImage(newImage);
		
		float green = 0.0f;
		
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				green = newImage.getPixel(x,y).floatValue();
				green = (float)Math.pow(green, 2);
				if(1.0-green > 0.920)
					newImage.setPixel(x, y, 1.0f-green);
				else
					newImage.setPixel(x, y, green);
			}
		}
		return newImage;
	}

	public static void main(String[] args) throws IOException {
		String absolutePath = "/home/dmatos/workspace2/JMIRe/samples/s147.jpg";
		File f = new File(absolutePath);
		MBFImage image = ImageUtilities.readMBF(f);
		RGB2Gray rgb2gray = new RGB2Gray(image);
		//FImage grayImg = rgb2gray.convert2().clone();
		FImage grayImg = rgb2gray.logOfGreen().clone();
		DisplayUtilities.display(grayImg).setSize(800, 600);;
	}

}
