/**
 * 
 */
package br.mire.utils.image;

import org.openimaj.image.FImage;

/**
 * @author dmatos
 *
 */
public class HistogramEntropy {

	int nbins;
	
	public HistogramEntropy(int nbins) {
		this.nbins = nbins;
	}
	
	public float entropy(float[] histogram) {
		double sum = 0;
		for(double h : histogram) {
			if(h > 0) sum = sum + (h * Math.log(h));
		}
		return (float)-sum;
	}
	
	public float analyze(FImage image, FImage mask) {
		
		float[] histogram = new float[nbins];
		
		int h_index;
		float count = 0f;
		
		for(int i = 0; i < image.width; i++) {
			for(int j = 0; j < image.height; j++) {
				if(mask.getPixel(i,j) > 0) {
					h_index = (int)  (image.getPixel(i,j) * 100);
					histogram[h_index] = histogram[h_index] + 1.0f;
					count += 1.0f;
				}				
			}
		}
		
		//relative frequencies
		for(int i = 0; i < nbins; i++) {
			histogram[i] = histogram[i] / count;
		}
		
		
		return entropy(histogram);
	}
	
	
}
