package br.mire.utils.image;

import org.openimaj.image.FImage;

public class GlobalMedian {
	
	int nbins = 101;
	
	public GlobalMedian() {}

	public float analyze(FImage image, FImage mask) {
		float[] histogram = new float[nbins];

		int h_index;
		float count = 0f;

		for(int i = 0; i < image.width; i++) {
			for(int j = 0; j < image.height; j++) {
				if(mask.getPixel(i,j) > 0) {
					h_index = (int)  (image.getPixel(i,j) * 100);
					histogram[h_index] = histogram[h_index] + 1.0f;
					count = count + 1.0f;
				}				
			}
		}
		
		float median_index = count/2f;
		
		float sum = 0f;
		for(int i = 0; i < nbins; i++) {
			sum = sum + histogram[i];
			if(sum >= median_index) return ((float)i/100.f); 
		}
		
		return 0f;
	}
}
