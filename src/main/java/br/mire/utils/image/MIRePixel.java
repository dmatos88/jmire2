package br.mire.utils.image;

import br.mire.utils.math.Coord2D;

public class MIRePixel{

	private CIELab color;
	private Coord2D coord;
	
	public CIELab getColor() {
		return color;
	}
	public void setColor(CIELab color) {
		this.color = color;
	}
	public Coord2D getCoord() {
		return coord;
	}
	public void setCoord(Coord2D coord) {
		this.coord = coord;
	}
}
