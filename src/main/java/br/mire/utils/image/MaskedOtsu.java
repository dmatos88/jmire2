package br.mire.utils.image;

import java.util.ArrayList;

import org.openimaj.image.FImage;
import org.openimaj.image.processing.threshold.OtsuThreshold;

public class MaskedOtsu {

	public MaskedOtsu() {};
	
	/**
	 * 
	 * @param image
	 * @param mask 1 to include, 0 to ignore 
	 * @return otsu's threshold for the mask
	 */
	public float analyze(final FImage image, final FImage mask, int nbins) {
		ArrayList<Float> pixelList = new ArrayList<Float>((int)mask.sum());
		for(int i = 0; i < image.width;  i++) {
			for(int j = 0; j < image.height;  j++) {
				if(mask.getPixel(i,j) > 0) {
					pixelList.add(image.getPixel(i,j));
				}
			}
		}	
		float[] pixelArray = new float[pixelList.size()];
		int index = 0;
		for(Float f : pixelList) {
			pixelArray[index++] = f;
		}
		pixelList.clear();
		pixelList = null;
		return OtsuThreshold.calculateThreshold(pixelArray, nbins);
	}

}
