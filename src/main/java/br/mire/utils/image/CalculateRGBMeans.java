package br.mire.utils.image;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;

public class CalculateRGBMeans {

	public static void main(String[] args) {
		
		String image_src_dir = "/home/dmatos/workspace/deeplearning/data/tcruzi_data_train";
		
		double[] sum = {0,0,0};
		double counter = 0.0;
		
		File dir = new File(image_src_dir);
		for(String subdir_path : dir.list()) {			
			
			File subdir = new File(image_src_dir+File.separator+subdir_path);
			
			for(String filename : subdir.list()) {

				//ignore subdirs
				File file = new File(image_src_dir+File.separator+subdir_path+File.separator+filename); 
				if(!file.isDirectory()) {
					try {
						MBFImage image = ImageUtilities.readMBF(file);
						sum[0] = sum[0] + image.getBand(0).sum() / (image.getHeight()*image.getWidth());
						sum[1] = sum[1] + image.getBand(1).sum() / (image.getHeight()*image.getWidth());
						sum[2] = sum[2] + image.getBand(2).sum() / (image.getHeight()*image.getWidth());
						counter = counter + 1.0;
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
		}
		
		sum[0] = sum[0] / counter * 255;
		sum[1] = sum[1] / counter * 255;
		sum[2] = sum[2] / counter * 255;
		
		System.out.println("RGB means: "+Arrays.toString(sum));
	}
}
