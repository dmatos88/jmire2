package br.mire.utils.image;

import java.io.File;
import java.io.IOException;

import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;

public class HistogramEq {

	private double[] histogram;
	private int nbins;

	public HistogramEq(int nbins) {
		this.nbins = nbins;		
	}

	public void processHistogram(FImage image) {
		this.histogram = new double[nbins];		
		for(int x = 0; x < image.width; x++ ) {
			for(int y = 0; y < image.height; y++) {
				double pixel_value = image.getPixel(x, y).floatValue();
				int index = new Float(pixel_value*(float)nbins).intValue();
				if(index >= nbins) index = nbins-1;
				histogram[index] += 1.0;
			}
		}
	}
	
	public double[] getNormalizedHistogram() {
		double[] normal_histogram = new double[nbins];
		double sum = 0.0;
		for(int i = 0; i < nbins; i++) {
			sum = sum + histogram[i];
		}
		for(int i = 0; i < nbins; i++) {
			normal_histogram[i] = histogram[i] / sum;
		}
		return normal_histogram;
	}
	
	public int getPeakIndex() {
		double max = 0;
		int max_index = -1;
		for(int i = 0; i < nbins; i++) {
			if(histogram[i] > max) {
				max = histogram[i];
				max_index = i;
			}
		}
		return max_index;
	}

	public void processHistogram(FImage image, FImage mask) {
		this.histogram = new double[nbins];		
		for(int x = 0; x < image.width; x++ ) {
			for(int y = 0; y < image.height; y++) {
				if(mask.getPixel(x,y) > 0) {
					double pixel_value = image.getPixel(x, y).floatValue();
					int index = new Float(pixel_value*(float)nbins).intValue();
					if(index < nbins) //index = nbins-1;
					histogram[index] += 1.0;
				}
			}
		}
	}

	public float getMedian(double[] norm_histogram) {
		double sum = 0.0;
		int i = 0;
		for(; i < norm_histogram.length; i++) {
			sum = sum + norm_histogram[i];
			if(sum >= 0.5) break;
		}		
		return (float)i/(norm_histogram.length-1);
	}
	
	public float getPercentile(double[] norm_histogram, double percentile) {
		double sum = 0.0;
		int i = 0;
		for(; i < norm_histogram.length; i++) {
			sum = sum + norm_histogram[i];
			if(sum >= percentile) break;
		}		
		return (float)i/(norm_histogram.length-1);
	}
	
	public boolean isBimodal(double[] norm_histogram) {
		double sum = 0.0;
		int left_max_index = 0;
		double left_max = 0;
		int right_max_index = 0;
		double right_max = 0;
		for(int i = 0; i < norm_histogram.length; i++) {
			sum = sum + norm_histogram[i];
			if(sum < 0.45) {
				if(norm_histogram[i] > left_max) {
					left_max = norm_histogram[i];
					left_max_index = i;
				}
			} else if(sum > 0.55){
				if(norm_histogram[i] > right_max) {
					right_max = norm_histogram[i];
					right_max_index = i;
				}
			}
		}
		
		if(right_max_index - left_max_index > 15) return true;
		return false;
	}
	
	public boolean isBimodal(FImage image, FImage mask, int nbins) {
		HistogramEq hist = new HistogramEq(nbins);

		hist.processHistogram(image, mask);

		double[] histogram = hist.getNormalizedHistogram();
		
		double sum = 0.0;
		int left_max_index = 0;
		double left_max = 0;
		int right_max_index = 0;
		double right_max = 0;
		int median_index = 0;
		for(int i = 0; i < histogram.length; i++) {
			sum = sum + histogram[i];
			if(sum <= 0.475) {
				if(histogram[i] > left_max) {
					left_max = histogram[i];
					left_max_index = i;
				}
			} else if (median_index == 0){
				median_index = i;
			} else if(sum >= 0.525){
				if(histogram[i] > right_max) {
					right_max = histogram[i];
					right_max_index = i;
				}
			}
		}
		
		double diff1 = histogram[median_index] - histogram[left_max_index];
		double diff2 = histogram[median_index] - histogram[right_max_index];
		
		/*
		System.out.println("median index "+median_index);
		System.out.println("left_max_index "+left_max_index);
		System.out.println("right_max_index "+right_max_index);
		System.out.println("diff1 "+diff1);
		System.out.println("diff2 "+diff2);
		*/
		
		if(	right_max_index - left_max_index > 10 &&  
			(diff1 < -0.00001 || diff2 < -0.00001) )
			return true;
		
		return false;
		
	}

	public double[] getHistogram() {
		return this.histogram;
	}

	public void equalize(FImage image, FImage mask) {

		this.processHistogram(image, mask);

		float min = 1;
		float max = nbins-1;
		
		double[] normal_hist = getNormalizedHistogram();
		
		double sum = 0;
		for(int i = 0; i < nbins; i++) {
			sum = sum + normal_hist[i];
			if(min == 1 && sum >= 0.0001) min = i;
			if(max == 255 && sum >= 0.999) max = i;
		}
		
		float max_minus_min = max-min;
		
		for(int i = 0; i < image.width; i++) {
			for(int j = 0; j < image.height; j++) {
				if(mask.getPixel(i,j) > 0) {
					float pxOut = (image.getPixel(i,j)*(float)nbins-min)*((nbins)/(max_minus_min));
					
					if(pxOut < min) pxOut = min;
					else if(pxOut > max) pxOut = max;
					
					image.setPixel(i, j, pxOut/nbins);
				}
			}
		}
		
	}

	public static void main(String[] args) throws IOException {
		String absolutePath = "/home/dmatos/workspace2/JMIRe/samples/s152.jpg";
		File f = new File(absolutePath);
		MBFImage image = ImageUtilities.readMBF(f);

		System.out.println(image.bands.size());

		HistogramEq eq = new HistogramEq(100);

		RGB2Gray rgb2gray = new RGB2Gray(image);

		FImage greenBand = rgb2gray.logOfGreen();

		//eq.equalize(greenBand);

		System.out.println(image.colourSpace);
		DisplayUtilities.display(greenBand);
	}

}
