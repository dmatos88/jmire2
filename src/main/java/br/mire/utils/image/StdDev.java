package br.mire.utils.image;

import org.openimaj.image.FImage;

public class StdDev {

	public StdDev() {}
	
	public float analyze(FImage image, FImage mask, float mean) {
		float count = 0f;
		float sum = 0f;
		for(int i = 0; i < image.width; i++) {
			for(int j = 0; j < image.height; j++) {
				if(mask.getPixel(i,j) > 0) {
					count = count + 1f;
					 sum = sum + ((image.getPixel(i,j) - mean) * (image.getPixel(i,j) - mean));
				}
			}
		}
		
		if(count < 1) return 0f;
		
		sum = sum / count;
		
		return (float)Math.sqrt(sum);
	}
	
	public FImage threshold(FImage image, float lowerbound, float upperbound) {
		FImage result = new FImage(image.width, image.height);
		for(int i = 0; i < image.width; i++) {
			for(int j = 0; j < image.height; j++) {
				if(image.getPixel(i,j) >= lowerbound && image.getPixel(i,j) <= upperbound) {
					result.setPixel(i, j, 1f);
				}
			}
		}
		return result;
	}
	
}
