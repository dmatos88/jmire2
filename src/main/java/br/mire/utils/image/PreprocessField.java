package br.mire.utils.image;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JFrame;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.Transforms;
import org.openimaj.image.processing.algorithm.GammaCorrection;
import org.openimaj.image.processing.convolution.FGaussianConvolve;
import org.openimaj.image.processing.threshold.OtsuThreshold;

import antlr.preprocessor.Preprocessor;
import br.mire.segmentation.controllers.LocalThresholdingController;
import br.mire.utils.math.Coord2D;
import br.mire.utils.ploters.BarChartPlotter;

public class PreprocessField {
	
	static Logger log = Logger.getLogger(PreprocessField.class.getName());
	/*
	 static {
	        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	 }
	 */

	public void displayFImage(FImage image, String title) {
		JFrame frame = DisplayUtilities.display(image);
		frame.setResizable(true);
		frame.setSize(1000, 800);
		frame.setTitle(title);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void displayMBFImage(MBFImage image, String title) {
		JFrame frame = DisplayUtilities.display(image);
		frame.setResizable(true);
		frame.setSize(1000, 800);
		frame.setTitle(title);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void applyGammaCorrection(FImage image, double gamma) {
		GammaCorrection gammaCorr = new GammaCorrection(gamma);
		float v;
		for(int i = 0; i < image.width; i++) {
			for(int j = 0; j < image.height; j++) {
				v = gammaCorr.processPixel(image.getPixel(i,j)); 
				image.setPixel(i, j, v);
			}
		}
	}


	private FImage floodFillMaskFromOrigin(FImage mask) {
		List<Coord2D> pxlist = new LinkedList<Coord2D>();
		pxlist.add(new Coord2D(0,0));

		FImage newMask = new FImage(mask.getWidth(), mask.getHeight());
		newMask.fill(1.0f);

		Coord2D c;
		int x, y;
		while(!pxlist.isEmpty()) {
			c = pxlist.remove(0);
			x = (int) c.getX();
			y = (int) c.getY();
			if(mask.getPixel(x,y) == 0) {
				newMask.setPixel(x, y, 0f);
				mask.setPixel(x, y, 1f); //flag as visited
				for(int i = -1; i < 2; i++) {
					for(int j = -1; j < 2; j++) {
						if(x + i >= 0 && y + j >= 0 & x + i < mask.getWidth() && j + y < mask.getHeight()) {
							if(mask.getPixel(x+i, y+j) == 0) {
								pxlist.add(new Coord2D(x+i, y+j));
							}
						}
					}
				}
			}
		}

		return newMask;
	}

	public FImage getFieldMask(FImage image, int blur_window_size) {
		FImage field_mask = image.clone();
		float otsu_thresh = OtsuThreshold.calculateThreshold(field_mask, 100);
		field_mask.threshold(otsu_thresh);
		field_mask = floodFillMaskFromOrigin(field_mask);
		return field_mask;
	}

	public FImage applyMaskedOtsuThreshold(FImage image, FImage mask, int nbins) {
		FImage temp = image.clone();
		MaskedOtsu maskedOtsu = new MaskedOtsu();
		float threshold = maskedOtsu.analyze(temp, mask, nbins);
		temp.threshold(threshold);
		temp.inverse();
		temp.multiplyInplace(mask);
		return temp;
	}

	public FImage circleErosion(FImage image, int radius) {

		float min = 1.1f;
		int xi, yj;

		FImage new_image = image.clone();

		for(int x = radius+1; x < image.width-radius; x++) {
			for(int y = radius+1; y < image.height-radius; y++){
				min = 1.1f;
				for(int i = -radius; i < radius; i++) {
					for(int j = -radius; j < radius; j++) {
						xi = x + i;
						yj = y + j;						

						double e = (xi-x)*(xi-x)+(yj-y)*(yj-y);
						if(Math.sqrt(e) < radius) {
							min = Float.min(image.getPixel(xi,yj), min);
						}
					}
				}
				new_image.setPixel(x, y, min);
			}
		}
		return new_image;
	}

	public FImage circleDilation(FImage image, int radius) {

		float max = -1f;
		int xi, yj;

		FImage new_image = image.clone();

		for(int x = radius+1; x < image.width-radius; x++) {
			for(int y = radius+1; y < image.height-radius; y++){
				max = -1f;
				for(int i = -radius; i < radius; i++) {
					for(int j = -radius; j < radius; j++) {
						xi = x + i;
						yj = y + j;						

						double e = (xi-x)*(xi-x)+(yj-y)*(yj-y);
						if(Math.sqrt(e) < radius) {
							max = Float.max(image.getPixel(xi,yj), max);
						}
					}
				}
				new_image.setPixel(x, y, max);
			}
		}
		return new_image;
	}

	public FImage circleMedianFilter(FImage image, int radius) {

		int xi, yj;

		FImage new_image = image.clone();

		Set<Float> values = new TreeSet<Float>();

		for(int x = radius+1; x < image.width-radius; x++) {
			for(int y = radius+1; y < image.height-radius; y++){
				values.clear();
				for(int i = -radius; i < radius; i++) {
					for(int j = -radius; j < radius; j++) {
						xi = x + i;
						yj = y + j;						

						double e = (xi-x)*(xi-x)+(yj-y)*(yj-y);
						if(Math.sqrt(e) < radius) {
							values.add(image.getPixel(xi,yj));
						}
					}
				}				
				new_image.setPixel(x, y, (Float)values.toArray()[values.size()/2]);
			}
		}
		return new_image;
	}

	public FImage circleMeanFilter(FImage image, int radius) {

		int xi, yj;

		FImage new_image = image.clone();

		float mean;
		float count;

		for(int x = radius+1; x < image.width-radius; x++) {
			for(int y = radius+1; y < image.height-radius; y++){
				mean = 0;
				count = 0;
				for(int i = -radius; i < radius; i++) {
					for(int j = -radius; j < radius; j++) {
						xi = x + i;
						yj = y + j;						

						double e = (xi-x)*(xi-x)+(yj-y)*(yj-y);
						if(Math.sqrt(e) < radius) {
							mean = mean + image.getPixel(xi,yj);
							count = count + 1;
						}
					}
				}				
				new_image.setPixel(x, y, mean/count);
			}
		}
		return new_image;
	}

	public FImage getHueImage(FImage a_star, FImage b_star) {
		FImage hue = new FImage(a_star.width, a_star.height);
		for(int i = 0; i < a_star.width; i++) {
			for(int j = 0; j < a_star.height; j++) {
				hue.setPixel(i, j, (float)Math.atan2(b_star.getPixel(i,j), a_star.getPixel(i,j)));				
			}
		}
		return hue;
	}

	public FImage getChromaImage(FImage a_star, FImage b_star) {
		FImage chroma = new FImage(a_star.width, a_star.height);
		for(int i = 0; i < a_star.width; i++) {
			for(int j = 0; j < a_star.height; j++) {
				chroma.setPixel(i, j, (a_star.getPixel(i,j)+b_star.getPixel(i,j))/2f );				
			}
		}
		return chroma;
	}

	public void displayHistogram(FImage image, FImage mask, int nbins) {

		HistogramEq hist = new HistogramEq(nbins);

		hist.processHistogram(image, mask);

		double[] histogram = hist.getNormalizedHistogram();

		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		for(int i = 0; i < histogram.length; i++) {
			dataset.addValue(histogram[i], "["+((float)i+(0.99f))+"]", "value");
		}

		BarChartPlotter barPlot = new BarChartPlotter("Histogram");
		barPlot.renderChart(dataset, "Histogram", "Bins", "Count");
		barPlot.pack();
		RefineryUtilities.centerFrameOnScreen(barPlot);
		barPlot.setVisible(true);

		final DefaultCategoryDataset dataset2 = new DefaultCategoryDataset();

		double sum = 0.0;
		for(int i = 0; i < histogram.length; i++) {
			sum = sum + histogram[i];
			dataset2.addValue(sum, "["+((float)i+(0.99f))+"]", "value");
		}

		BarChartPlotter barPlot2 = new BarChartPlotter("Left to right");
		barPlot2.renderChart(dataset2, "Histogram", "Bins", "Count");
		barPlot2.pack();
		RefineryUtilities.centerFrameOnScreen(barPlot2);
		barPlot2.setVisible(true);

		/*
		final DefaultCategoryDataset dataset3 = new DefaultCategoryDataset();

		sum = 0.0;
		for(int i = histogram.length - 1; i >= 0; i--) {
			sum = sum + histogram[i];
			dataset3.addValue(sum, "["+((float)i+(0.99f))+"]", "value");
		}

		BarChartPlotter barPlot3 = new BarChartPlotter("Right to left");
		barPlot3.renderChart(dataset3, "Histogram", "Bins", "Count");
		barPlot3.pack();
		RefineryUtilities.centerFrameOnScreen(barPlot3);
		barPlot3.setVisible(true);
		 */
	}

	public FImage integralImage(FImage image) {
		FImage integral = new FImage(image.width, image.height);
		FImage sum = new FImage(image.width, image.height);

		for(int i = 1; i < image.width; i++) {
			for(int j = 1; j < image.height; j++) {
				sum.setPixel(i,j, sum.getPixel(i,j-1) + image.getPixel(i,j));
				integral.setPixel(i, j, integral.getPixel(i-1,j)+sum.getPixel(i,j)); 
			}
		}
		return integral;
	}

	public FImage integralBlur(FImage image, int window_size) {

		FImage result = new FImage(image.width, image.height);

		FImage integral = integralImage(image);


		float blurredPixel;

		float w = window_size;
		int w_by_2 = window_size/2;
		float ww =  w*w;

		for(int x = window_size; x < image.width - w; x++) {
			for(int y = window_size; y < image.height - w; y++) {
				blurredPixel = (integral.getPixel(x+w_by_2, y+w_by_2) +  integral.getPixel(x-w_by_2, y-w_by_2)
				- integral.getPixel(x+w_by_2,y-w_by_2) - integral.getPixel(x-w_by_2,y+w_by_2)) / ww;
				result.setPixel(x, y, blurredPixel/(2*w+1)*(2*w+1));
			}
		}

		return result;
	}

	/**
	 * 
	 * @param image
	 * @param maximum maximum gray value (usually 0.5 or 180 for hue)
	 * @param k a constant between 0.2 and 0.5
	 * @return
	 */
	public FImage localAdaptiveThresholdSauvola(FImage image, double maximum, double k, int window_size) {

		FImage result = new FImage(image.width, image.height);

		double[][] integral = new double[image.width][image.height];
		double[][] squaredIntegral = new double[image.width][image.height];
		double[][] sum = new double[image.width][image.height];
		double[][] sum2 = new double[image.width][image.height];
		for(int i = 1; i < image.width; i++) {
			for(int j = 1; j < image.height; j++) {
				sum[i][j] = sum[i][j-1] + image.getPixel(i,j);
				integral[i][j] = integral[i-1][j]+sum[i][j];
				
				sum2[i][j] = sum2[i][j-1] + image.getPixel(i,j)*image.getPixel(i,j);
				squaredIntegral[i][j] = integral[i-1][j]+sum2[i][j]; 
			}
		}

		double m, g2, s2, t;

		double w = window_size;
		int w_by_2 = window_size/2;
		double ww = w*w;

		for(int x = window_size; x < image.width - w; x++) {
			for(int y = window_size; y < image.height - w; y++) {
				m = (integral[x+w_by_2][y+w_by_2] +  integral[x-w_by_2][y-w_by_2]
				- integral[x+w_by_2][y-w_by_2] - integral[x-w_by_2][y+w_by_2]) / ww;

				g2 = (squaredIntegral[x+w_by_2][y+w_by_2] +  squaredIntegral[x-w_by_2][y-w_by_2]
				- squaredIntegral[x+w_by_2][y-w_by_2] - squaredIntegral[x-w_by_2][y+w_by_2]) / ww;

				s2 = g2 - (m*m);

				t = m * (1.0 + k * (Math.sqrt(s2)/maximum - 1.0));

				if(image.getPixel(x,y) <= t) result.setPixel(x, y, 1f);
			}
		}

		return result;
	}


	/**
	 * Https://arxiv.org/pdf/1201.5227.pdf)
	 * @param image
	 * @param k a constant between 0.2 and 0.5
	 * @return
	 */
	public FImage localAdaptiveThresholdSingh(FImage image, float k, int window_size) {

		FImage result = new FImage(image.width, image.height);

		FImage integral = integralImage(image);

		float m, t, px;

		float w = window_size;
		int w_by_2 = window_size/2;
		float ww = w*w;

		for(int x = window_size; x < image.width - w; x++) {
			for(int y = window_size; y < image.height - w; y++) {
				m = (integral.getPixel(x+w_by_2, y+w_by_2) +  integral.getPixel(x-w_by_2, y-w_by_2)
				- integral.getPixel(x+w_by_2,y-w_by_2) - integral.getPixel(x-w_by_2,y+w_by_2)) / ww;

				px = integral.getPixel(x, y) - m;

				t = m * (1f + k * (px/(1-px) - 1f));

				if(image.getPixel(x,y) <= t) result.setPixel(x, y, 1f);
			}
		}

		return result;
	}

	public FImage differenceOfMeans(FImage image, int w_1, int w_2) {
		FImage result = new FImage(image.width, image.height);

		FImage integral = integralImage(image);


		float blurredPixel1, blurredPixel2;

		float w1 = w_1;
		int w1_by_2 = w_1/2;
		float ww1 =  w1*w1;

		float w2 = w_2;
		int w2_by_2 = w_2/2;
		float ww2 = w2*w2;

		for(int x = w_1; x < image.width - w_1; x++) {
			for(int y = w_1; y < image.height - w_1; y++) {

				blurredPixel1 = (integral.getPixel(x+w1_by_2, y+w1_by_2) +  integral.getPixel(x-w1_by_2, y-w1_by_2)
				- integral.getPixel(x+w1_by_2,y-w1_by_2) - integral.getPixel(x-w1_by_2,y+w1_by_2)) / ww1;


				blurredPixel1 = blurredPixel1/(2*w1+1)*(2*w1+1);

				blurredPixel2 = (integral.getPixel(x+w2_by_2, y+w2_by_2) +  integral.getPixel(x-w2_by_2, y-w2_by_2)
				- integral.getPixel(x+w2_by_2,y-w2_by_2) - integral.getPixel(x-w2_by_2,y+w2_by_2)) / ww2;


				blurredPixel2 = blurredPixel2/(2*w2+1)*(2*w2+1);


				result.setPixel(x, y, blurredPixel1-blurredPixel2);
			}
		}

		return result;

	}


	/**
	 * https://pdfs.semanticscholar.org/8a6d/87aa870f737c1484acd4f21641899022e6fd.pdf
	 * @param image
	 * @return
	 */
	public void offGanglionInplace(FImage image) {

		int[][] patterns = {{15,7},{7,1},{7,3},{3,1}};


		double[][] integral = new double[image.width][image.height];
		double[][] sum = new double[image.width][image.height];
		for(int i = 1; i < image.width; i++) {
			for(int j = 1; j < image.height; j++) {
				sum[i][j] = sum[i][j-1] + image.getPixel(i,j);
				integral[i][j] = integral[i-1][j]+sum[i][j];
			}
		}

		double max, maxS, g;

		for(int x = 15; x < image.width - 15; x++) {
			for(int y = 15; y < image.height - 15; y++) {
				max = 0;
				maxS = 0;
				for(int[] pattern : patterns) {
					int w_1 = pattern[0];
					double w1 = w_1;
					int w1_by_2 = w_1/2;
					double ww1 =  w1*w1;

					int w_2 = pattern[1];
					double w2 = w_2;
					int w2_by_2 = w_2/2;
					double ww2 = w2*w2;		

					double blurredPixel1 = (integral[x+w1_by_2][ y+w1_by_2] +  integral[x-w1_by_2][ y-w1_by_2]
					- integral[x+w1_by_2][y-w1_by_2] - integral[x-w1_by_2][y+w1_by_2]) / ww1;


					blurredPixel1 = blurredPixel1/(2*w1+1)*(2*w1+1);

					double blurredPixel2 = (integral[x+w2_by_2][y+w2_by_2] +  integral[x-w2_by_2][y-w2_by_2]
					- integral[x+w2_by_2][y-w2_by_2] - integral[x-w2_by_2][y+w2_by_2]) / ww2;


					blurredPixel2 = blurredPixel2/(2*w2+1)*(2*w2+1);

					if((blurredPixel1 - blurredPixel2) > max) {
						max = blurredPixel1 - blurredPixel2;
						maxS = blurredPixel1;
					}
				}

				//TODO apply equation 2
				g = 0;
				if(max > 0) {
					g = max*maxS / (maxS + max);
				}
				image.setPixel(x, y, (float)g);
			}
		}
	}
	


	/**
	 * https://pdfs.semanticscholar.org/8a6d/87aa870f737c1484acd4f21641899022e6fd.pdf
	 * @param image
	 * @return
	 */
	public FImage offGanglion(FImage image) {

		FImage result = new FImage(image.width, image.height);

		int[][] patterns = {{15,7},{7,1},{7,3},{3,1}};


		double[][] integral = new double[image.width][image.height];
		double[][] sum = new double[image.width][image.height];
		for(int i = 1; i < image.width; i++) {
			for(int j = 1; j < image.height; j++) {
				sum[i][j] = sum[i][j-1] + image.getPixel(i,j);
				integral[i][j] = integral[i-1][j]+sum[i][j];
			}
		}

		double max, maxS, g;

		for(int x = 15; x < image.width - 15; x++) {
			for(int y = 15; y < image.height - 15; y++) {
				max = 0;
				maxS = 0;
				for(int[] pattern : patterns) {
					int w_1 = pattern[0];
					double w1 = w_1;
					int w1_by_2 = w_1/2;
					double ww1 =  w1*w1;

					int w_2 = pattern[1];
					double w2 = w_2;
					int w2_by_2 = w_2/2;
					double ww2 = w2*w2;		

					double blurredPixel1 = (integral[x+w1_by_2][ y+w1_by_2] +  integral[x-w1_by_2][ y-w1_by_2]
					- integral[x+w1_by_2][y-w1_by_2] - integral[x-w1_by_2][y+w1_by_2]) / ww1;


					blurredPixel1 = blurredPixel1/(2*w1+1)*(2*w1+1);

					double blurredPixel2 = (integral[x+w2_by_2][y+w2_by_2] +  integral[x-w2_by_2][y-w2_by_2]
					- integral[x+w2_by_2][y-w2_by_2] - integral[x-w2_by_2][y+w2_by_2]) / ww2;


					blurredPixel2 = blurredPixel2/(2*w2+1)*(2*w2+1);

					if((blurredPixel1 - blurredPixel2) > max) {
						max = blurredPixel1 - blurredPixel2;
						maxS = blurredPixel1;
					}
				}

				//TODO apply equation 2
				g = 0;
				if(max > 0) {
					g = max*maxS / (maxS + max);
				}
				result.setPixel(x, y, (float)g);
			}
		}

		return result;
	}

	public FImage integralEdgeDetection(FImage image, int width, float alpha, float threshold) {


		double[][] integral = new double[image.width][image.height];
		double[][] sum = new double[image.width][image.height];

		double[][] integral_x = new double[image.width][image.height];
		double[][] sum_x = new double[image.width][image.height];

		double[][] integral_y = new double[image.width][image.height];
		double[][] sum_y = new double[image.width][image.height];


		for(int i = 1; i < image.width; i++) {
			for(int j = 1; j < image.height; j++) {
				sum_x[i][j] = sum_x[i][j-1] + image.getPixel(i,j) * i;
				sum_y[i][j] = sum_y[i][j-1] + image.getPixel(i,j) * j;
				sum[i][j] = sum[i][j-1] + image.getPixel(i,j);
				integral[i][j] = integral[i-1][j]+sum[i][j];
				integral_x[i][j] = integral_x[i-1][j]+sum_x[i][j];
				integral_y[i][j] = integral_y[i-1][j]+sum_y[i][j];
			}
		}


		int w_by_2 = width/2;
		double a,b,c;

		FImage edges = new FImage(image.width, image.height);
		double[][] gx = new double[image.width][image.height];
		double[][] gy = new double[image.width][image.height];
		double[][] mag = new double[image.width][image.height];
		double[][] theta = new double[image.width][image.height];

		for(int i = w_by_2; i < image.width-w_by_2; i++) {
			for(int j = w_by_2; j < image.height-w_by_2; j++) {
				a = integral[i+w_by_2][j+w_by_2] + integral[i-w_by_2][j-w_by_2] - integral[i+w_by_2][j-w_by_2] - integral[i-w_by_2][j+w_by_2];
				b = integral_x[i+w_by_2][j+w_by_2] + integral_x[i-w_by_2][j-w_by_2] - integral_x[i+w_by_2][j-w_by_2] - integral_x[i-w_by_2][j+w_by_2];
				c = integral_y[i+w_by_2][j+w_by_2] + integral_y[i-w_by_2][j-w_by_2] - integral_y[i+w_by_2][j-w_by_2] - integral_y[i-w_by_2][j+w_by_2];

				gx[i][j] = alpha*(b/a - i)*a;
				gy[i][j] = alpha*(c/a - j)*a;

				mag[i][j] = Math.sqrt(gx[i][j]*gx[i][j]+gy[i][j]*gy[i][j]);

				theta[i][j] = (Math.atan2(gy[i][j], gx[i][j])) * 180.0 / Math.PI;

				//edges.setPixel(i,j,(float)mag[i][j]);
			}
		}

		//*
		for(int i = 1; i < image.width-1; i++) {
			for(int j = 1; j < image.height-1; j++) {	
				if(mag[i][j] > threshold) {
					if( theta[i][j] > -22.5	|| theta[i][j] < 22.5 || 
							theta[i][j] > 157.5  || theta[i][j] < -157.5) {
						//sector 0
						if(mag[i][j] >= mag[i-1][j] && mag[i][j] >= mag[i+1][j]) edges.setPixel(i, j, 1f);
					}

					if( theta[i][j] < -112.5 && theta[i][j] >= -157.5 ||
							theta[i][j] >= 22.5 && theta[i][j] < 67.5) {
						//sector 1
						if(mag[i][j] >= mag[i+1][j-1] && mag[i][j] >= mag[i-1][j+1]) edges.setPixel(i, j, 1f);
					}

					if( theta[i][j] >= 67.5 && theta[i][j] < 112.5 || 
							theta[i][j] <= -67.5 && theta[i][j] > -122.5) {
						//sector 2
						if(mag[i][j] >= mag[i][j-1] && mag[i][j] >= mag[i][j+1]) edges.setPixel(i, j, 1f);
					}

					if(theta[i][j] <= -22.5 && theta[i][j] > -67.5 ||
							theta[i][j] >= 112.5 && theta[i][j] < 157.5) {
						//setctor 3
						if(mag[i][j] >= mag[i-1][j-1] && mag[i][j] >= mag[i+1][j+1]) edges.setPixel(i, j, 1f);
					}
				}

			}
		}
		//*/


		edges.normalise();	

		return edges;
	}
	
	public void unsharpMaskInPlace(FImage image, FImage blurred, float amount) {
		image.addInplace(image.subtract(blurred).multiply(amount));
	}


	public static void main(String[] args) throws Exception{
		
		log.setLevel(Level.ALL);
		
		//testGammaCorrection();
		//testMaskedOtsuThreshold();
		//testSauvolaMethod();
		//testIntegralBlur();
		//FImage a = testIntegralEdgesDetection();
		testIntegralBlur();
		//testOffGanglion4AllCIELabLayers();
		
	}
	
	public static void testOffGanglion4AllCIELabLayers() throws Exception{
		
		String index = "field";
		index = index + "_0400";
		String src_dir = "/home/dmatos/samples_fields_test/";
		String filename = index+".jpg";
		//String filename = index+".png";

		String filepath = src_dir + filename;

		MBFImage image = ImageUtilities.readMBF(new File(filepath));

		long start = System.currentTimeMillis();

		MBFImage cielab = Transforms.RGB_TO_CIELabNormalised(image);
		
		PreprocessField processor = new PreprocessField();

		/*
		processor.displayFImage(image.getBand(0), "pre L");
		processor.displayFImage(image.getBand(1), "pre a");
		processor.displayFImage(image.getBand(2), "pre b");
		*/
		
		FImage l_star_blur = processor.offGanglion(cielab.getBand(0));
		FImage a_star_blur = processor.offGanglion(cielab.getBand(1));
		FImage b_star_blur = processor.offGanglion(cielab.getBand(2));
		
		/*
		processor.displayFImage(image.getBand(0).normalise(), "pos L");
		processor.displayFImage(image.getBand(1).normalise(), "pos a");
		processor.displayFImage(image.getBand(2).normalise(), "pos b");
		*/
		
		processor.unsharpMaskInPlace(cielab.getBand(0), l_star_blur, 1f);
		processor.unsharpMaskInPlace(cielab.getBand(1), a_star_blur, 1f);
		processor.unsharpMaskInPlace(cielab.getBand(2), b_star_blur, 1f);
		
		/*
		processor.displayFImage(image.getBand(0).normalise(), "pos L");
		processor.displayFImage(image.getBand(1).normalise(), "pos a");
		processor.displayFImage(image.getBand(2).normalise(), "pos b");		
		*/
		
		///*
		MBFImage result = new MBFImage(
				cielab.getBand(0).normalise(),
				cielab.getBand(1).normalise(),
				cielab.getBand(2).normalise());
		//*/
		
		/*
		MBFImage result = new MBFImage(
				l_star_blur.normalise(),
				a_star_blur.normalise(),
				b_star_blur.normalise());
		*/
		
		//result = Transforms.CIELabNormalised_TO_RGB(result);
		
		processor.displayMBFImage(image, "original");
		//processor.displayMBFImage(result, "result");
		
		float mean = image.getBand(0).sum()/(image.getWidth()*image.getHeight());
		image.getBand(0).subtractInplace(mean);
		
		mean = image.getBand(1).sum()/(image.getWidth()*image.getHeight());
		image.getBand(1).subtractInplace(mean);
		
		mean = image.getBand(2).sum()/(image.getWidth()*image.getHeight());
		image.getBand(2).subtractInplace(mean);
		
		processor.displayMBFImage(image, "original");
		
		ImageUtilities.write(image, new File("mean_subtraction.jpg"));
	}

	public static FImage testIntegralEdgesDetection() throws Exception{
		String index = "field";
		index = index + "_0020";
		String src_dir = "/home/dmatos/samples_fields_test/";
		String filename = index+".jpg";
		//String filename = index+".png";

		String filepath = src_dir + filename;

		MBFImage image = ImageUtilities.readMBF(new File(filepath));

		long start = System.currentTimeMillis();

		image = Transforms.RGB_TO_CIELabNormalised(image);

		FImage l_star = image.getBand(0).clone();

		PreprocessField processor = new PreprocessField();

		l_star = processor.offGanglion(l_star);

		//_star = processor.integralBlur(l_star, 5);

		final float alpha = 1f;
		float threshold = .0f;

		FImage edges = processor.integralEdgeDetection(l_star, 11, alpha, threshold);

		long end = System.currentTimeMillis() - start;
		System.out.println("Took "+(end/1000)+"s");

		processor.displayFImage(edges, index+" edges");
		
		return edges;
	}

	public static FImage testIntegralBlur() throws Exception{

		String index = "field0650";
		String src_dir = "/home/dmatos/tcruzi_data/fields/";
		String filename = index+".jpg";
		/*
		String index = "[356]count[1]";
		String src_dir = "/home/dmatos/samples_fields/";
		String filename = "field"+index+".jpg";
		 */

		String filepath = src_dir + filename;

		MBFImage image = ImageUtilities.readMBF(new File(filepath));

		long start = System.currentTimeMillis();

		image = Transforms.RGB_TO_CIELabNormalised(image);

		//FImage b_star = image.getBand(2).clone();

		//FImage a_star = image.getBand(1).clone();

		FImage l_star = image.getBand(0).clone();

		//l_star.normalise();

		PreprocessField processor = new PreprocessField();		

		FImage l_star_thresh_image = processor.offGanglion(l_star);

		l_star_thresh_image.normalise();

		/*
		FImage mask = new FImage(l_star.width, l_star.height);
		mask.fill(1f);
		//FImage mask = processor.getFieldMask(l_star_thresh_image, 5);

		//processor.displayFImage(l_star_thresh_image, index+" contrast enhacement");

		//processor.displayHistogram(l_star_thresh_image, mask, 101);

		HistogramEq hist = new HistogramEq(101);

		hist.processHistogram(l_star_thresh_image, mask);

		//processor.displayHistogram(l_star_thresh_image, mask, 101);
		
		//double[] histogram = hist.getNormalizedHistogram();		

		boolean isBimodal = hist.isBimodal(l_star, mask, 101);

		//processor.circleErosion(l_star_thresh_image, 5);

		log.info("Bimodal: "+isBimodal);
		FImage result;

		if(isBimodal) {
			//float median_thresh_value = hist.getPercentile(histogram, 0.5);
			//System.out.println("Thresh value: "+median_thresh_value);
			//FImage median_mask = l_star_thresh_image.clone();
			//median_mask.threshold(median_thresh_value).inverse();
			result = processor.localAdaptiveThresholdSauvola(l_star_thresh_image, 0.5f, 0.001, 29);
			//result2 = processor.localAdaptiveThresholdSauvola(l_star_thresh_image, 0.5f, 0.11, 27);
			//result3 = processor.localAdaptiveThresholdSauvola(l_star_thresh_image, 0.5f, 0.11, 51);
			//result.multiplyInplace(median_mask);
		} else {
			//float median_thresh_value = hist.getPercentile(histogram, 0.2);
			//System.out.println("Thresh value: "+median_thresh_value);
			//FImage median_mask = l_star_thresh_image.clone();
			//median_mask.threshold(median_thresh_value).inverse();
			result = processor.localAdaptiveThresholdSauvola(l_star_thresh_image, 0.5f, 0.001f, 29);
			//result2 = processor.localAdaptiveThresholdSauvola(l_star_thresh_image, 0.5f, 0.001f, 51);
			//result3 = processor.localAdaptiveThresholdSauvola(l_star_thresh_image, 0.5f, 0.001f, 75);
			//result.multiplyInplace(median_mask);
		}
		//FImage result_singh = processor.localAdaptiveThresholdSingh(l_star_thresh_image, 0.2f, 131);
		*/

		float maximum_value = 0.5f;
		float k = 0.001f;
		int w = 33;

		log.info("[max "+maximum_value+"]");

		log.info("[k "+k+"]");

		log.info("[w "+w+"]");

		FImage result = processor.localAdaptiveThresholdSauvola(l_star_thresh_image, maximum_value, k, w);

		long end = System.currentTimeMillis() - start;
		log.info("Took "+(end/1000)+"s");


		processor.displayFImage(result, index+" 1st threshold Sauvola");
		//processor.displayFImage(result2, index+" 2nd threshold Sauvola");
		//processor.displayFImage(result3, index+" 3rd threshold Sauvola");

		/*
		FImage hue = processor.getHueImage(a_star, b_star);
		hue.normalise();

		FImage hue_thresh_image = processor.offGanglion(hue);

		hue_thresh_image = processor.localAdaptiveThresholdSauvola(hue_thresh_image, 0.5f, 0.05f, 21);

		processor.displayFImage(hue_thresh_image, index+"hue threshold");
		 */
		
		return result;

	}

	public static void testSauvolaMethod() throws Exception{
		String src_dir = "/home/dmatos/samples_fields_test/";
		//String index = "[046]count[1]";

		//String src_dir = "/home/dmatos/samples_fields2/fora_de_foco/";
		String index = "_0345";

		String filename = "field"+index+".jpg";//"field_"+index+".jpg";

		String filepath = src_dir + filename;

		MBFImage image = ImageUtilities.readMBF(new File(filepath));

		long start = System.currentTimeMillis();

		image = Transforms.RGB_TO_CIELabNormalised(image);

		FImage b_star = image.getBand(2).clone();

		FImage a_star = image.getBand(1).clone();

		FImage l_star = image.getBand(0).clone();

		/*
		GaussianConvolve gauss = new FGaussianConvolve(1.5f);

		gauss.processImage(b_star);

		gauss.processImage(a_star);

		gauss.processImage(l_star);
		 */

		b_star.normalise();

		a_star.normalise();

		l_star.normalise();

		PreprocessField processor = new PreprocessField();		

		//processor.displayFImage(b_star, "original");

		FImage mask = processor.getFieldMask(l_star, 5);

		//l_star.multiplyInplace(mask);

		//processor.displayFImage(mask, "mask");

		//processor.displayFImage(b_star, "b_star");

		//processor.displayFImage(a_star, "a_star");

		//processor.displayFImage(l_star, "l_star");

		FImage hue = processor.getHueImage(a_star, b_star);
		hue.normalise();

		//FGaussianConvolve gauss = new FGaussianConvolve(1.5f);
		//gauss.processImage(hue);

		//hue.multiplyInplace(mask);

		//erosion
		//hue = processor.circleErosion(hue,3);
		//l_star = processor.circleErosion(l_star,3);

		//HistogramEq histEq = new HistogramEq(360);

		//histEq.equalize(hue, mask);

		FImage hue_thresh_image = hue.clone();

		hue_thresh_image = processor.localAdaptiveThresholdSauvola(hue_thresh_image, 0.5f, 0.525f, 500);

		//FImage l_thresh_image = l_star.clone();				

		//l_thresh_image = processor.localAdaptiveThresholdSauvola(l_thresh_image, 0.5f, 0.25f, 60);		

		long end = System.currentTimeMillis() - start;

		System.out.println("Preprocessing took "+(end/1000)+"s");

		//processor.displayFImage(l_star, index+" L*");

		processor.displayFImage(hue, index+" hue");

		processor.displayFImage(hue_thresh_image, index+" hue thresh");

		//processor.displayFImage(l_thresh_image, index+" L* thresh");

	}

	public static void testMaskedOtsuThreshold() throws Exception{	

		String src_dir = "/home/dmatos/tcruzi_data/tcruzi_data_train/tcruzi/";
		//String index = "[046]count[1]";

		//String src_dir = "/home/dmatos/samples_fields2/fora_de_foco/";
		String index = "0001";

		String filename = "field"+index+".jpg";//"field_"+index+".jpg";

		String filepath = src_dir + filename;

		MBFImage image = ImageUtilities.readMBF(new File(filepath));

		long start = System.currentTimeMillis();

		image = Transforms.RGB_TO_CIELabNormalised(image);

		FImage b_star = image.getBand(2).clone();

		FImage a_star = image.getBand(1).clone();

		FImage l_star = image.getBand(0).clone();

		/*
		FGaussianConvolve gauss = new FGaussianConvolve(1.5f);

		gauss.processImage(b_star);

		gauss.processImage(a_star);

		gauss.processImage(l_star);
		 */

		b_star.normalise();

		a_star.normalise();

		l_star.normalise();


		PreprocessField processor = new PreprocessField();		

		//processor.displayFImage(b_star, "original");

		FImage mask = processor.getFieldMask(l_star, 5);

		//processor.displayFImage(mask, "mask");

		//processor.displayFImage(b_star, "b_star");

		//processor.displayFImage(a_star, "a_star");

		//processor.displayFImage(l_star, "l_star");

		FImage hue = processor.getHueImage(a_star, b_star);

		hue = processor.circleErosion(hue,5);

		HistogramEq histEq = new HistogramEq(360);

		histEq.equalize(hue, mask);

		hue.normalise();

		MBFImage clusters = new MBFImage(hue.width, hue.height);

		for(int i = 0; i < hue.width; i++) {
			for(int j = 0; j < hue.height; j++) {
				if(hue.getPixel(i,j) < 0.1) {
					clusters.getBand(0).setPixel(i, j,.5f);
				}					
				else if(hue.getPixel(i,j) < 0.18) {
					clusters.getBand(0).setPixel(i, j, 1f);
				} 
				else if(hue.getPixel(i,j) < 0.225) {
					clusters.getBand(1).setPixel(i, j, .5f);
				}
				else if(hue.getPixel(i,j) < 0.27) { 
					clusters.getBand(1).setPixel(i, j, 1f);
				}
				else if(hue.getPixel(i,j) < 0.315) { 
					clusters.getBand(2).setPixel(i, j, .5f);
				}
				else if(hue.getPixel(i,j) < 0.36) {
					clusters.getBand(2).setPixel(i, j, 1f);
				}
				else if(hue.getPixel(i,j) < 0.50) {
					clusters.getBand(1).setPixel(i, j, 1f);
					clusters.getBand(2).setPixel(i, j, 1f);
					clusters.getBand(0).setPixel(i, j, 1f);
				}

			}
		}


		long end = System.currentTimeMillis() - start;

		System.out.println("Preprocessing took "+(end/1000)+"s");

		processor.displayFImage(hue, "hue");

		processor.displayMBFImage(clusters, index);

	}


	public static void testGammaCorrection() throws Exception{
		String src_dir = "/home/dmatos/samples_fields_test/";

		String index = "0333";

		String filename = "field_"+index+".jpg";

		String filepath = src_dir + filename;

		MBFImage image = ImageUtilities.readMBF(new File(filepath));

		FImage green = image.getBand(1).clone();
		green.normalise();

		double gamma = 0.9;

		PreprocessField processor = new PreprocessField();

		processor.displayFImage(image.getBand(1), "green");

		processor.applyGammaCorrection(green, gamma);

		processor.displayFImage(green, "gamma correciton");
	}

}
