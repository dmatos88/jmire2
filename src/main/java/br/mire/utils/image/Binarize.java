package br.mire.utils.image;

import org.openimaj.image.FImage;

public class Binarize {
		
	public static void processInPlace(FImage image, float threshold) {
		
		for(int i = 0; i < image.height; i++) {
			for(int j = 0; j < image.width; j++) {
				Float p = image.getPixel(j, i);
				if(p <= threshold)
					image.setPixel(j, i, 1.0f);
				else
					image.setPixel(j, i, 0.0f);
			}
		}
	}
	
	public static void main(String[] args) {
		double x = 100;
		
		System.out.println("Hello "+x);
	}
}
