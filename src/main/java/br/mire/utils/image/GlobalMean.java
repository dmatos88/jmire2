package br.mire.utils.image;

import org.openimaj.image.FImage;

public class GlobalMean {

	public GlobalMean() {}
	
	public float analyze(FImage image, FImage mask) {
		
		float sum = 0;
		float count = 0f;
		
		for(int i = 0; i < image.width; i++) {
			for(int j = 0; j < image.height; j++) {
				if(mask.getPixel(i,j) > 0) {
					sum = sum + image.getPixel(i, j);
					count = count + 1.0f;
				}				
			}
		}
		
		return sum / count;
		
	}
	
}
