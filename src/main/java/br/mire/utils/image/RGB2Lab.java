package br.mire.utils.image;

import br.mire.utils.math.Coord2D;

public class RGB2Lab {

	/**
	 * Accepts only perfect square images
	 * @param rgb 3-band matrix in RGB color space
	 * @return 3-band matrix in CIEL*a*b* color space where band[0] is L*,
	 *  		band[1] is a and band[2] is b 
	 */
	public static float[][][] convert(float[][][] rgb){

		if(rgb == null || rgb.length < 3) return null;

		int width = rgb[0][0].length;
		int height = rgb[0].length;

		float[][][] matrixLab = new float[3][width][height];
		MIRePixel pixel_xy;
		CIELab labSpace = new CIELab();

		for(int band = 0; band < 3; band++) {
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					pixel_xy = new MIRePixel();
					pixel_xy.setCoord(new Coord2D(x, y));
					pixel_xy.setColor(
							labSpace.convertFromRGB(
									rgb[0][y][x], //red
									rgb[1][y][x], //green
									rgb[2][y][x] //blue
									));

					matrixLab[0][x][y] = (float)pixel_xy.getColor().getL_star();
					matrixLab[1][x][y] = (float)pixel_xy.getColor().getA_star();
					matrixLab[2][x][y] = (float)pixel_xy.getColor().getB_star();
				}
			}
		}
		
		return matrixLab;

	}

}
