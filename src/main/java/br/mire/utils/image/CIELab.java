package br.mire.utils.image;

public class CIELab {

	private double L_star = 0.0;
	private double a_star = 0.0;
	private double b_star = 0.0;

	public  double getL_star() {
		return L_star;
	}


	public void setL_star(double l_star) {
		L_star = l_star;
	}

	public double getA_star() {
		return a_star;
	}

	public void setA_star(double a_star) {
		this.a_star = a_star;
	}

	public double getB_star() {
		return b_star;
	}

	public void setB_star(double b_star) {
		this.b_star = b_star;
	}

	//Illuminant D65
	final private double Xn = 0.95047;
	final private double Yn = 1.000;
	final private double Zn = 1.08833;

	private double ft(double t) {

		if(t > 0.008856) {
			return Math.pow(t, 1.0/3.0);
		} else {
			return t*7.787+16.0/116.0;
		}
	}

	private float inverse_ft(double t) {
		if(t > 0.206897) return (float) (t*t*t);
		return 0.128419f*((float)t - 0.137931034f);
	}

	public float[] convert2RGB() {
		float X = (float)Xn * inverse_ft(((float)L_star + 16.0f)/116.0f + (float)a_star/500.0f);
		float Y = (float)Yn * inverse_ft(((float)L_star + 16.0f)/116.0f);
		float Z = (float)Zn * inverse_ft(((float)L_star+16.0f)/116.0f - (float)b_star/200.0f);

		float[] rgb = new float[3];

		rgb[0] = 3.2404542f * X -1.5371385f*Y -0.4985314f*Z;
		rgb[1] = -0.9692660f * X + 1.8760108f*Y+ 0.0415560f*Z;
		rgb[2] = 0.0556434f* X -0.2040259f*Y+ 1.0572252f*Z;
		
		rgb[0] = (float) xyz2rgbGammaCorrection(rgb[0]);
		rgb[1] = (float) xyz2rgbGammaCorrection(rgb[0]);
		rgb[2] = (float) xyz2rgbGammaCorrection(rgb[0]);
		
		return rgb;
	}
	
	// https://en.wikipedia.org/wiki/SRGB#The_forward_transformation_(CIE_XYZ_to_sRGB)
	public double xyz2rgbGammaCorrection(double u) {
		if(u < 0.0031308) return 12.92*u;
		else return 1.055*Math.pow(u, (1.0/2.4))-0.055;
	}
	
	public double rgb2xyzGammaCorrection(double u) {
		if(u < 0.04045)return u/12.92;
		else return Math.pow((u+0.055/1.055), 2.4);
	}

	public CIELab convertFromRGB(double red, double green, double blue) {
		/*
		 * First we convert from sRGB to XYZ, where
		 *  0.4124564  0.3575761  0.1804375
		 *	0.2126729  0.7151522  0.0721750
		 *	0.0193339  0.1191920  0.9503041
		 */	
		
		red = rgb2xyzGammaCorrection(red);
		green = rgb2xyzGammaCorrection(green);
		blue = rgb2xyzGammaCorrection(blue);
		
		double X = 0.4124564 * red + 0.3575761 * green + 0.1804375 * blue;
		double Y = 0.2126729 * red + 0.7151522 * green + 0.0721750 * blue;
		double Z = 0.0193339 * red + 0.1191920 * green + 0.9503041 * blue;

		CIELab px = new CIELab();

		px.L_star = 116.0 * ft(Y/Yn) - 16;
		px.a_star = 500.0 * (ft(X/Xn)- ft(Y/Yn));
		px.b_star = 200.0 * (ft(Y/Yn) - ft(Z/Zn));

		return px;
	}

	public double getColorEuclideanDistance(CIELab pxB) {
		double x = this.L_star - pxB.L_star;
		double y = this.a_star - pxB.a_star;
		double z = this.b_star - pxB.b_star;
		return Math.sqrt(x*x + y*y + z*z);
	}
	
	public double getColorChebyshevDistance(CIELab pxB) {
		double x = this.L_star - pxB.L_star;
		double y = this.a_star - pxB.a_star;
		double z = this.b_star - pxB.b_star;
		return Double.max(Double.max(x, y), z);
	}
}
