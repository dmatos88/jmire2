package br.mire.utils.math;

public class Coord2D {

	private double x;
	private double y;
	
	public Coord2D(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Coord2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public void setY(double y) {
		this.y = y;
	}
	
	public double getEuclideanDistance(Coord2D otherCoord) {
		double delta_x = this.x - otherCoord.getX();
		double delta_y = this.y - otherCoord.getY();
		
		return Math.sqrt(delta_x*delta_x+delta_y*delta_y);
	}
	
	public String toString() {
		return "["+x+","+y+"]";
	}
}
