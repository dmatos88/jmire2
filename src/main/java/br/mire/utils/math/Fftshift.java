package br.mire.utils.math;

import com.frank.math.Complex;;

public class Fftshift {

	/**
	 * 
	 * @param vector of complex where vector[i%2] = real and vector[i%2+1] = imaginary
	 * @return shifted vector
	 */
	public static double[] fftshift(final double[] vector) {
		double[] shiftVec = new double[vector.length];
		int K = new Double(Math.floor((double)vector.length/2.0)).intValue();

		for(int i = 0; i < vector.length; i++) {
			shiftVec[(i+K)%vector.length] = vector[i];
		}

		return shiftVec;
	}

	public static double[][] fftshift(final double[][] matrix){

		double[][] resultMatrix = new double[matrix.length][matrix[0].length];

		int nlin = matrix.length;

		int yK = new Double(Math.floor((double)nlin/2.0)).intValue();


		for(int i = 0; i < nlin; i++) {			
			resultMatrix[i] = Fftshift.fftshift(matrix[ (i+yK) % nlin]);
		}

		return resultMatrix;
	}

	public static double[][] ifftshift(final double[][] matrix){
		if(matrix.length%2 == 0) {
			return Fftshift.fftshift(matrix);
		} else {
			double[][] resultMatrix = new double[matrix.length][matrix[0].length];

			int nlin = matrix.length;

			int yK = new Double(Math.ceil((double)nlin/2.0)).intValue();			

			for(int i = 0; i < nlin; i++) {			
				resultMatrix[i] = Fftshift.fftshift(matrix[ (i+yK) % nlin]);
			}
			return resultMatrix;
		}
	}

	public static Complex[][] ifftshift(final Complex[][] matrix) {
		Complex[][] resultMatrix = new Complex[matrix.length][matrix[0].length];

		int nlin = matrix.length;

		int yK = new Double(Math.ceil((double)nlin/2.0)).intValue();			

		for(int i = 0; i < nlin; i++) {			
			resultMatrix[i] = Fftshift.ifftshift(matrix[ (i+yK) % nlin]);
		}
		return resultMatrix;
	}

	public static Complex[] ifftshift(final Complex[] vector) {
		if(vector.length%2 == 0) {
			return fftshift(vector);
		} else {
			Complex[] shiftVec = new Complex[vector.length];
			int K = new Double(Math.ceil((double)vector.length/2.0)).intValue();

			for(int i = 0; i < vector.length; i++) {
				shiftVec[(i+K)%vector.length] = vector[i];
			}

			return shiftVec;
		}
	}

	public static Complex[] fftshift(final Complex[] vector) {

		Complex[] shiftVec = new Complex[vector.length];
		int K = new Double(Math.floor((double)vector.length/2.0)).intValue();

		for(int i = 0; i < vector.length; i++) {
			shiftVec[(i+K)%vector.length] = vector[i];
		}

		return shiftVec;
	}

	public static Complex[][] fftshift(final Complex[][] matrix){

		Complex[][] resultMatrix = new Complex[matrix.length][matrix[0].length];

		int nlin = matrix.length;

		int yK = new Double(Math.floor((double)nlin/2.0)).intValue();


		for(int i = 0; i < nlin; i++) {			
			resultMatrix[i] = Fftshift.fftshift(matrix[ (i+yK) % nlin]);
		}

		return resultMatrix;
	}


	public static void main(String[] args) {

		int nlin = 5;
		int ncol = 5;

		double[][] matrix = new double[nlin][ncol];

		System.out.println(matrix.length);
		System.out.println(matrix[0].length);

		for(int i = 0; i < nlin; i++) {
			for(int j = 0; j < ncol; j++) {
				matrix[i][j] = (i+1)*(j+1);
				System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}

		double[][] shiftedMatrix = Fftshift.fftshift(matrix);

		System.out.println("=======shift=========");

		for(int i = 0; i < nlin; i++) {
			for(int j = 0; j < ncol; j++) {
				System.out.print(shiftedMatrix[i][j]+" ");
			}
			System.out.println();
		}

		double[][] unshiftedMatrix = Fftshift.ifftshift(shiftedMatrix);

		System.out.println("=======unshift=========");

		for(int i = 0; i < nlin; i++) {
			for(int j = 0; j < ncol; j++) {
				System.out.print(unshiftedMatrix[i][j]+" ");
			}
			System.out.println();
		}
	}

}
