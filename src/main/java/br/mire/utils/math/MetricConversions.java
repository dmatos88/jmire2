package br.mire.utils.math;

import org.openimaj.image.FImage;

import br.mire.features.extraction.GeometricFeatures;
import br.mire.segmentation.contour.ComplexSignalContour2;

public class MetricConversions {
	
	private final static double inch = 25.4;
	
	public static double convertPixel2milimeter(double length_in_pixels, double ppi) {
		double px_in_mm = inch/ppi;
		return length_in_pixels * px_in_mm;
	}

	public static double rescaleFromZoom(double value, double zoom) {
		return value / zoom;
	}

	public static double convert2micrometer(double length_in_pixels, double ppi) {
		return convertPixel2milimeter(length_in_pixels, ppi) * 1000.0;
	}

	public static double scaleAndConvert2micrometer(double length_in_pixels, double ppi, double zoom) {
		return rescaleFromZoom(convert2micrometer(length_in_pixels, ppi), zoom);
	}
	
	public static double getScale(FImage img, double field_number, double objective_mag, double ocular_mag) throws Exception {
		int x = 1;
		int y = img.height / 2;

		//DisplayUtilities.display(img);
		
		ComplexSignalContour2 contourSignal = new ComplexSignalContour2(img, x, y);

		/*			

		final XYSeries contour = new XYSeries( "contour" );          
		for(int i = 0; i < contourSignal.getXSignal().length; i++ ) {
			contour.add( (double)contourSignal.getXSignal()[i] ,(double) contourSignal.getYSignal()[i] );
		}

		final XYSeriesCollection dataset = new XYSeriesCollection( );          
		dataset.addSeries( contour );
		ScatterPlot.scatterPlotDataset(dataset, "JMIRe.getScale(FImage field)", "x", "y");
		//*/

		double[] axes = GeometricFeatures.principalAxis(contourSignal);

		double real_field_diameter = field_number / objective_mag;
		double pixel_side_in_mm = real_field_diameter / axes[0]; ///((axes[0] + axes[1]) / 2.0);
		double pixel_side_in_um = pixel_side_in_mm * objective_mag * ocular_mag;

		//double mice_rbc_diameter_in_px = 80; 

		return pixel_side_in_um;
	}
}
