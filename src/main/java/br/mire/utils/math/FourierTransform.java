package br.mire.utils.math;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import br.mire.features.extractor.utils.FourierBasedCurvatureEstimation;
import br.mire.utils.ploters.LinePlot;

public class FourierTransform {

	public static double[] multiplyComplex(final double[] complexA, final double[] complexB) {
		double[] result = new double[2];

		result[0] = complexA[0] * complexB[0] - complexA[1] * complexB[1];
		result[1] = complexA[0] * complexB[1] + complexA[1] * complexB[0]; 

		return result;

	}

	public static double[] divide1byComplex(double[] complex) {
		double[] conjugate = {complex[0], -complex[1]};
		double[] result = new double[2];
		double[] complexOne = {1.0,0.0};

		double[] numerator = multiplyComplex(complexOne,conjugate);
		double[] denominator = multiplyComplex(complex, conjugate);

		result[0] = numerator[0]/denominator[0];
		result[1] = numerator[1]/denominator[0];

		return result;
	}

	//e^j*x
	public static double[] expComplex(double x) {
		double[] result = new double[2];
		result[0] = Math.cos(x);
		result[1] = Math.sin(x);
		return result;
	}
	

	/**
	 * 
	 * @param matrix complex fourier transform of matrix
	 * @return
	 */
	public static double[][] inverseDft2d(final double[][] matrix){
		
		int nlin = matrix.length;
		int mcol = matrix[0].length;
		
		int M = mcol/2;
		int N = nlin;
		
		double[][] Fuy = new double[nlin][mcol];
		
		//1st we get the inversedft1d of each column
		for(int x = 0; x < M; x++) {
			double[] auxVec = new double[nlin*2];
			for(int y = 0; y < nlin; y++) {
				auxVec[y*2] = matrix[y][x*2];
				auxVec[y*2+1] = matrix[y][2*x+1];
			}
			double[] ift = FourierTransform.inverseDft1d(auxVec);
			for(int i = 0; i < nlin; i++) {
				Fuy[i][2*x] = ift[2*i];
				Fuy[i][2*x+1] = ift[2*i+1];
			}
		}
		
		double[][] fuv = new double[nlin][mcol];
		double[] sum = {0,0};
		
		for(int i = 0; i < Fuy.length; i++) {
			for(int s = 0; s < M; s++) {
				double sDouble = s;
				sum[0] = 0.0; //real
				sum[1] = 0.0; //imag
				for(int n = 0; n < M; n++) {
					double nDouble = n;
					double[] complexU = {Fuy[i][2*n],Fuy[i][2*n+1]};
					double[] complexResult = multiplyComplex(complexU, expComplex(2.0*Math.PI*nDouble*sDouble/M));			
					sum[0] += complexResult[0];
					sum[1] += complexResult[1];
				}
				fuv[i][s*2] = 1.0 / M * sum[0];
				fuv[i][s*2+1] = 1.0 / M * sum[1];				
			}
		}
						
		return fuv;
	}
	
	/**
	 * 
	 * @param U complex fourier transform of u
	 * @return
	 */
	public static double[] inverseDft1d(double[] U) {
		int N = U.length/2;
		double[] u = new double[U.length];

		for(int s = 0; s < N; s++) {
			double sDouble = s;
			double[] sum = new double[2];
			sum[0] = 0.0; //real
			sum[1] = 0.0; //imag
			for(int n = 0; n < N; n++) {
				double nDouble = n;
				double[] complexU = {U[2*n], U[2*n+1]};
				double[] complexResult = multiplyComplex(complexU, expComplex(2.0*Math.PI*nDouble*sDouble/N));			
				sum[0] += complexResult[0];
				sum[1] += complexResult[1];
			}
			u[s*2] = 1.0/N * sum[0];
			u[s*2+1] = 1.0/N * sum[1];
		}	
		
		return u;
	}

	/**
	 * 
	 * @param u signal
	 * @return U[2*i] real, U[2*i+1] imaginary
	 */
	public static double[] dft1D(final double[] u) {

		int N = u.length;
		double[] U = new double[u.length*2];

		double[] sum = new double[2];
		
		for(int s = 0; s < N; s++) {
			double sDouble = s;
			
			sum[0] = 0.0; //real
			sum[1] = 0.0; //imag
			for(int n = 0; n < N; n++) {
				double nDouble = n;
				double[] complexU = {u[n], 0};
				double[] complexResult = multiplyComplex(complexU, divide1byComplex(expComplex(2.0*Math.PI*nDouble*sDouble/N)));			
				sum[0] += complexResult[0];
				sum[1] += complexResult[1];
			}
			U[s*2] = sum[0];
			U[s*2+1] = sum[1];
		}	
		
		return U;
	}
	
	public static double[][] dft2D(final double[][] matrix){
		
		int nlin = matrix.length;
		int mcol = matrix[0].length;
		
		double[][] Fuy = new double[nlin][2*mcol];
		
		//1st we get the fft1D of each column
		for(int x = 0; x < mcol; x++) {
			double[] auxVec = new double[nlin];
			for(int y = 0; y < nlin; y++) {
				auxVec[y] = matrix[y][x];
			}
			double[] ft = FourierTransform.dft1D(auxVec);
			for(int i = 0; i < nlin; i++) {
				Fuy[i][2*x] = ft[2*i];
				Fuy[i][2*x+1] = ft[2*i+1];
			}
		}
		
		double[][] Fuv = new double[nlin][2*mcol];
		double[] sum = {0,0};
		
		for(int i = 0; i < Fuy.length; i++) {
			for(int s = 0; s < mcol; s++) {
				double sDouble = s;
				sum[0] = 0.0; //real
				sum[1] = 0.0; //imag
				for(int n = 0; n < mcol; n++) {
					double nDouble = n;
					double[] complexU = {Fuy[i][2*n],Fuy[i][2*n+1]};
					double[] complexResult = multiplyComplex(complexU, divide1byComplex(expComplex(2.0*Math.PI*nDouble*sDouble/mcol)));			
					sum[0] += complexResult[0];
					sum[1] += complexResult[1];
				}
				Fuv[i][s*2] = sum[0];
				Fuv[i][s*2+1] = sum[1];				
			}
		}
						
		return Fuv;
	}
	
	public static double[] fft1d(final double[] X, int n, int k) {

		
		
		return null;
	}
	
	public static void test01d() {
		int N = 256;
		double T = 2*Math.PI/(N-1);
		
		double[] gt = new double[N];

		double radius = 1.0;
		
		final XYSeriesCollection dataset_a = new XYSeriesCollection();
		final XYSeries signal_a = new XYSeries( "g(t)" );
		
		//Cesar & Costa Figure 7.6(a)
		int i = 0;
		for(double t = 0; t < 2*Math.PI; t+=T, i++) {
			gt[i] = Math.cos(2*t) + Math.sin(t*t);
			signal_a.add(i, gt[i]);
		}
		dataset_a.addSeries(signal_a);
		
		LinePlot.linePlotDataset("Ploter", "g(t) = cos(2t)+sin(t^2)", "t", "g(t)", dataset_a);

		//Cesar & Costa Figure 7.6(c)
		FourierBasedCurvatureEstimation estimate_Gf = new FourierBasedCurvatureEstimation(gt, 1.0/1000);
		
		
		double[] du  = estimate_Gf.getdU();
		
		final XYSeries signal = new XYSeries( "dg/dt" );
		double t = 0.0;
		for(int j = 0; j < N; j++, t+=T) {
			signal.add(t, estimate_Gf.getdU()[j]);
		}
		
		final XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries( signal );
		
		LinePlot.linePlotDataset("Ploter", "Fourier Based Curvature Estimation", "t", "dg/dt", dataset);
		
	}
	
	public static void test02d() {
		double[][] matrix2 = {
				{3,3,3,3},
				{2,2,2,2},
				{1,1,1,1},
				{1,1,1,1},
				{1,1,1,3}};
		
		double[][] matrix = FourierTransform.dft2D(matrix2); 
		
		printMatrix(matrix);
		
		matrix = Fftshift.fftshift(matrix); 	

		printMatrix(matrix);
		
		matrix = Fftshift.ifftshift(matrix);
		
		printMatrix(matrix);
		
		matrix = FourierTransform.inverseDft2d(matrix);
					
		printMatrix(matrix);
	}
	
	private static void printMatrix(double[][] matrix) {
		System.out.println("Matrix ["+matrix.length+","+matrix[0].length+"]");
		for(int i = 0; i < matrix.length; i++) {
			for(int j = 0; j < matrix[i].length; j++) {
				System.out.printf("%.3f ", matrix[i][j]);
			}
			System.out.println();
		}
		for(int i = 0; i < matrix[0].length; i++) {
			System.out.print("=");
		}
		System.out.println();
	}
	
	public static void main(String args[]) {
		FourierTransform.test01d();
		//FourierTransform.test02d();
	}

}
