package br.mire.utils.math;

public class Ifftshift {
	public static double[] ifftshift(double[] vector) {

		double[] shiftVec = new double[vector.length];
		int K = new Double(Math.ceil((double)vector.length/2.0)).intValue();

		for(int i = 0; i < vector.length; i++) {
			shiftVec[(i+K)%vector.length] = vector[i];
		}

		return shiftVec;		
	}
	
	public static int[] ifftshift(int[] vector) {

		int[] shiftVec = new int[vector.length];
		int K = new Double(Math.ceil((double)vector.length/2.0)).intValue();

		for(int i = 0; i < vector.length; i++) {
			shiftVec[(i+K)%vector.length] = vector[i];
		}

		return shiftVec;		
	} 
	
	public static Object[] ifftshift(Object[] vector) {

		Object[] shiftVec = new Object[vector.length];
		int K = new Double(Math.ceil((double)vector.length/2.0)).intValue();

		for(int i = 0; i < vector.length; i++) {
			shiftVec[(i+K)%vector.length] = vector[i];
		}

		return shiftVec;		
	}
}
