package br.mire.utils.math;

import com.frank.math.Complex;

public class FrequencyGaussian {


	public static double[][] getFourierGaussian2d( int nlines, int mcols,double sigma) {

		int N = nlines;
		int M = mcols;

		double[][] gaussian = new double[N][M];

		int j = 0;
		for(double r = -Math.floor(M/2); r < M - Math.floor(M/2); r+=1, j++) {
			int i = 0;
			for(double s = -Math.floor(N/2); s < N - Math.floor(N/2); s+=1, i++) {
				
				gaussian[i][j] = Math.pow(Math.exp(1),-2.0*Math.PI*Math.PI*(r*r+s*s)*sigma*sigma);
			}
		}


		return gaussian;
	}
	
	public static Complex[][] getFourierLoG2d(int nlines, int mcols, double sigma){
		
		Complex[][] LoG = new Complex[nlines][mcols];
		
		int N = nlines;
		int M = mcols;
		
		int j = 0;
		for(double r = -Math.floor(M/2); r < M - Math.floor(M/2); r+=1, j++) {
			int i = 0;
			for(double s = -Math.floor(N/2); s < N - Math.floor(N/2); s+=1, i++) {	
				double R = (s*s+r*r);
				LoG[i][j] = new Complex(  
						-1.0 * (1.0 / (Math.PI * sigma*sigma*sigma*sigma))
						* (1.0-(R/(2*sigma*sigma)))
						* Math.exp(-R/(2*sigma*sigma)), 0);
						//(1.0-(R/(sigma*sigma)))
						//* Math.exp(-R/(2*sigma*sigma)));
			}
		}
		
		return LoG;
	}
	
public static Complex[][] getDeformedFourierLoG2d(int nlines, int mcols, double sigma, double stretch_s, double stretch_r, double theta){
		
		Complex LoG[][] = new Complex[nlines][mcols];
		
		int N = nlines;
		int M = mcols;
		double aux_s;
		double aux_r;
		
		double cos_theta = Math.cos(theta);
		double sin_theta = Math.sin(theta);
		 
		int j = 0;
		for(double r = -Math.floor(M/2); r < M - Math.floor(M/2); r+=1, j++) {
			int i = 0;
			for(double s = -Math.floor(N/2); s < N - Math.floor(N/2); s+=1, i++) {	
				aux_s = s * stretch_s;
				aux_r = r * stretch_r;
				
				aux_s = cos_theta * aux_s - sin_theta * aux_r;
				aux_r = sin_theta * aux_s + cos_theta * aux_r;
				
				double R = (aux_s*aux_s+aux_r*aux_r);
				
				LoG[i][j] = new Complex(  
						-1.0 * (1.0 / (Math.PI * sigma*sigma*sigma*sigma))
						* (1.0-(R/(2*sigma*sigma)))
						* Math.exp(-R/(2*sigma*sigma)), 0);
				
			}
		}
		
		return LoG;
	}

}
