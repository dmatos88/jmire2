package br.mire.utils.ploters;

import org.jzy3d.analysis.AbstractAnalysis;
import org.jzy3d.analysis.AnalysisLauncher;
import org.jzy3d.chart.AWTChart;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapGrayscale;
import org.jzy3d.colors.colormaps.ColorMapHotCold;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.javafx.JavaFXChartFactory;
import org.jzy3d.maths.Range;
import org.jzy3d.plot3d.builder.Builder;
import org.jzy3d.plot3d.builder.Mapper;
import org.jzy3d.plot3d.builder.concrete.OrthonormalGrid;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.rendering.canvas.Quality;

import br.mire.features.extractor.Curvegram;

public class CurvegramSurface extends AbstractAnalysis{
	
	private static double[][] matrix;

	private static int numOfPoints;
	private static int numOfContours;
	public  static int stepsY;

	
	public CurvegramSurface(Curvegram c) {
		super();
		numOfContours = c.getCurvegram().length;
		numOfPoints = c.getCurvegram()[0].length;
		stepsY = numOfPoints;
		matrix = new double[numOfContours][numOfPoints];
		
		for(int i = 0; i < numOfContours; i++) {
			for(int j = 0; j < numOfPoints; j++) {
				matrix[i][j] = c.getCurvegram()[i][j];
			}
		}
	}
	
	public static double getK(int i, int j) {
		return matrix[i][j];
	}
	
	@Override
	public void init() throws Exception {
			// Define a function to plot
			Mapper mapper = new Mapper() {
				@Override
				public double f(double x, double y) {
					//System.out.println("getting from ("+X+","+Y+") = "+Curvegram.curvegramSurface[X][Y]);
					return getK((int)x,(int)y);
				}
			};

			// Define range and precision for the function to plot
			Range rangeX = new Range(0, numOfContours-1);
			int stepsX = numOfContours;		
			Range rangeY = new Range(0, numOfPoints-1);
			int stepsY = numOfPoints;


			// Create the object to represent the function over the given range.
			final Shape surface = Builder.buildOrthonormal(new OrthonormalGrid(rangeX, stepsX, rangeY, stepsY), mapper);
			surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(), surface.getBounds().getZmax(), new Color(1, 1, 1, .5f)));
			surface.setFaceDisplayed(true);
			surface.setWireframeDisplayed(true);
			surface.setWireframeColor(Color.BLACK);
			
		}
		

}
