package br.mire.utils.ploters;

import java.awt.BasicStroke;
import java.awt.Color;

import javax.swing.WindowConstants;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.ui.ApplicationFrame;
import org.jfree.data.xy.XYSeriesCollection;

public class LinePlot extends ApplicationFrame{

		   /**for test purposes only */
		private static final long serialVersionUID = 1L;

		public LinePlot( String applicationTitle, String chartTitle,
				String x_axis, String y_axys, XYSeriesCollection dataset, boolean legend) {
		      super(applicationTitle);
		      JFreeChart xylineChart = ChartFactory.createXYLineChart(
		         chartTitle ,
		         x_axis ,
		         y_axys ,
		         dataset ,
		         PlotOrientation.VERTICAL ,
		         legend , true , false);
		         
		      ChartPanel chartPanel = new ChartPanel( xylineChart );
		      chartPanel.setPreferredSize( new java.awt.Dimension( 560 , 367 ) );
		      final XYPlot plot = xylineChart.getXYPlot( );
		      
		      XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer( );
		      renderer.setAutoPopulateSeriesPaint(true); 
		      renderer.setSeriesStroke( 0 , new BasicStroke( 4.0f ) );
		      plot.setRenderer( renderer ); 
		      setContentPane( chartPanel );
		      this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		      
		   }
		
		public static void linePlotDataset(String applicationTitle, String chartTitle,
				String x_axis, String y_axys, final XYSeriesCollection dataset) {
			LinePlot chart = new LinePlot(applicationTitle,  chartTitle,
					 x_axis,  y_axys, dataset, true);
			chart.setSize(800, 800);
			chart.setLocationRelativeTo(null);
			chart.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
			chart.setVisible(true);
		}
		
		public static void linePlotDataset(String applicationTitle, String chartTitle,
				String x_axis, String y_axys, final XYSeriesCollection dataset,
				boolean legend) {
			LinePlot chart = new LinePlot(applicationTitle,  chartTitle,
					 x_axis,  y_axys, dataset, legend);
			chart.setSize(800, 800);
			chart.setLocationRelativeTo(null);
			chart.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
			chart.setVisible(true);
		}

}
