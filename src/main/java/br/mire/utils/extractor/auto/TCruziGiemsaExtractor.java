package br.mire.utils.extractor.auto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openimaj.image.connectedcomponent.ConnectedComponentLabeler;
import org.openimaj.image.pixel.ConnectedComponent;
import org.openimaj.image.pixel.Pixel;

import br.mire.ml.classifier.tcruzi.Prediction;
import br.mire.segmentation.controllers.LocalThresholdingController;
import br.mire.utils.math.Coord2D;

public class TCruziGiemsaExtractor  {
	
	static Logger log = Logger.getLogger(TCruziGiemsaExtractor.class.getName());
	
	public void execute() throws Exception{
		int radius = 10;
		float sigma = 1.5f;
		float threshold = 0.6f;
		float cutoff = 0.5f;

		String input_csv_dir_path = "/home/dmatos/workspace2/jmire/data/input/";
		String output_csv_dir_path = "/home/dmatos/workspace2/jmire/data/";		


		LocalThresholdingController appController = LocalThresholdingController.getInstance();
		appController.setGUI(false);

		appController.setObjectiveMagnification(100);
		appController.setOcularMagnification(10);
		appController.setFieldNumber(20);
		appController.readAppConfigFile();		


		System.out.println("===============");
		System.out.println("Reading and processing files."
				+ "\nPlease wait");
		System.out.println("===============");	

		BufferedReader groundTruthReader = new BufferedReader(new FileReader(input_csv_dir_path+"ground_truth.csv"));
		System.out.println("ground_truth.csv OK");

		//skip header
		groundTruthReader.readLine();
		
		String gt_line = null;

		double x,y;
		String cluster;
		String filename;

		System.out.println("===============");
		System.out.println("Initializing main loop");
		System.out.println("===============");	

		boolean continue_here = false;
		String start_from = "0001";

		while((gt_line = groundTruthReader.readLine()) != null) {


			String[] csv_row = gt_line.split(LocalThresholdingController.csv_separator);
			
			filename = csv_row[0];
			cluster = csv_row[3];
			
			x = new Double(csv_row[1]);
			y = new Double(csv_row[2]);
			
			
			appController.setSelectedCluster(cluster);
			
			
			if(!continue_here && filename.contains(start_from)) {
				continue_here = true;
				System.out.println("continuing from "+filename);
			}

			if(continue_here) {
				//3para cada imagem contida no ground truth (não no diretório como está abaixo)
				System.out.println("====");
				System.out.println("Processing: "+filename);
				appController.selectImage(filename, radius, sigma, threshold, cutoff);

				appController.selectCoordinates((int)x, (int)y); //Kinetoplast

				appController.extractFeatures(output_csv_dir_path);
				
				System.out.println(filename+" DONE!");
				System.out.println("====");
			}


		}	

		groundTruthReader.close();

	}
	
	public void extractUnknownFeatures(String ground_truth_path, String dest_dir, int skip_count) throws Exception{
				
		BufferedReader groundTruthReader = new BufferedReader(new FileReader(ground_truth_path));
		System.out.println("ground_truth.csv OK");

		HashMap<String, List<Coord2D>> positions_map = new HashMap<String, List<Coord2D>>();
		
		//1st retrieve list of coordinates of interest for each field
		//skip header
		String gt_line = groundTruthReader.readLine();
		
		while((gt_line = groundTruthReader.readLine()) != null) {


			String[] csv_row = gt_line.split(LocalThresholdingController.csv_separator);
			
			String filename = csv_row[0];
			
			double x = new Double(csv_row[1]);
			double y = new Double(csv_row[2]);
			
			List<Coord2D> coords = positions_map.get(filename);
			
			if(coords == null) coords = new LinkedList<Coord2D>();
			
			coords.add(new Coord2D(x,y));
			
			positions_map.put(filename, coords);
		}
		
		groundTruthReader.close();
		
		log.info("number of itens "+positions_map.keySet().size());
		
		//if(true) return;
		
		LocalThresholdingController appController = LocalThresholdingController.getInstance();
		appController.setGUI(false);
		
		appController.setObjectiveMagnification(100);
		appController.setOcularMagnification(10);
		appController.setFieldNumber(20);
		appController.readAppConfigFile();	
		
		appController.setSelectedCluster(Prediction.Structure.ARTIFACT.toString());
		
		int radius = 29;
		float sigma = 1.5f;
		float threshold = 0.01f;
		float cutoff = 0.5f;
		
		int skip_counter = 0;
		
		for(String filename : positions_map.keySet()) {
			
			skip_counter++;
			
			if(skip_counter < skip_count) continue;
			
			log.info("Extracting UNKNOWN features for image "+filename+" # "+skip_counter);
			
			List<Coord2D> coords = positions_map.get(filename);
			
			try {
				appController.selectImage(filename, radius, sigma, threshold, cutoff);
			} catch(Exception e) {
				e.printStackTrace();
				continue;
			}
			
			ConnectedComponentLabeler cclabeler = new ConnectedComponentLabeler(
					ConnectedComponentLabeler.Algorithm.TWO_PASS,
					0.5f,
					ConnectedComponent.ConnectMode.CONNECT_8);	

			cclabeler.analyseImage(appController.thresh_sample);

			List<ConnectedComponent>ccList = cclabeler.getComponents();	

			int cclist_size = ccList.size();
			
			log.info("Counting "+cclist_size+" objects");
			
			int objects_counter = 0;
			int counter = 0;

			for(ConnectedComponent cc : ccList){

				log.info("Object "+(counter++)+" of "+cclist_size);;
				
				boolean ignore = false;
				
				if(cc.getPixels().size() >= 80) {					
					
					for(Pixel pixel : cc.getPixels()) {
						for(Coord2D coord: coords) {
							if(pixel.x == coord.getX() && pixel.y == coord.getY()) {
								ignore = true;
								break;
							}								
						}
					}
					
					if(!ignore) {
						
						objects_counter++;
						
						Pixel px = (Pixel) cc.getPixels().toArray()[0];
						appController.selectCoordinates(px.x, px.y);
						appController.extractFeatures(dest_dir);
					}
				}
				
			}
			
			log.info("Features extracted for "+objects_counter+" objects.");
		}
		
	}


	/**
	 * caderno de notas em 2018-09-30
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		log.setLevel(Level.ALL);

		TCruziGiemsaExtractor extractor = new TCruziGiemsaExtractor();
		//extractor.execute();
		
		extractor.extractUnknownFeatures("/home/dmatos/dest2/2019-08-11-train/ground_truth.csv", "/home/dmatos/dest2", 114);

		System.exit(0);

	}


}
