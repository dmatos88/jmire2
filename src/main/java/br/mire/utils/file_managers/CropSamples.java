package br.mire.utils.file_managers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.imageio.IIOException;

import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.processing.transform.AffineSimulation;

import br.mire.segmentation.controllers.LocalThresholdingController;

public class CropSamples {

public void cropUnknown(String datadir, String imagedir, String outputdir) throws Exception{
		
		//TODO 
		// abrir ground truth
		// ler posição 
		// fazer crop na posição com raio de 127 pixels
		
		BufferedReader bf = new BufferedReader(new FileReader(datadir+"ground_truth.csv"));
		
		//skip header
		bf.readLine();

		Integer xpos, ypos;
		String line, filename;		
		String[] values;
		
		int side = 256;

		int tilt = side/2;
		
		int field_count = 0;

		while((line = bf.readLine()) != null) {
			values = line.split(LocalThresholdingController.csv_separator);
			
			filename = values[0];
			
			xpos = new Double(values[1]).intValue();
			ypos = new Double(values[2]).intValue();
			
			System.out.println("Processing: "+filename);
			
			MBFImage image = null;
			
			try {
				image = ImageUtilities.readMBF(new File(imagedir+filename));
			}catch(IIOException ex) {
				ex.printStackTrace();
			}
			
			if(image == null) continue;
			
			if(field_count < 10) filename = "000"+(field_count++)+".jpg";
			else if(field_count < 100) filename = "00"+(field_count++)+".jpg";
			else if(field_count < 1000) filename = "0"+(field_count++)+".jpg";
			else filename = "u"+(field_count++)+".jpg";
			
			//upper right
			MBFImage crop = image.extractROI(xpos-tilt, ypos+tilt, side, side);
			ImageUtilities.write(crop, new File(outputdir+filename));
			
			if(field_count < 10) filename = "000"+(field_count++)+".jpg";
			else if(field_count < 100) filename = "00"+(field_count++)+".jpg";
			else if(field_count < 1000) filename = "0"+(field_count++)+".jpg";
			else filename = (field_count++)+".jpg";
			
			//lower left
			crop = image.extractROI(xpos+tilt, ypos-tilt, side, side);
			ImageUtilities.write(crop, new File(outputdir+"u"+filename));
			
		}
		
		bf.close();
	}
	
	public void cropTcruzi(String datadir, String imagedir, String outputdir) throws Exception{
		
		//TODO 
		// abrir ground truth
		// ler posição 
		// fazer crop na posição com raio de 127 pixels
		
		BufferedReader bf = new BufferedReader(new FileReader(datadir+"ground_truth.csv"));
		
		//skip header
		bf.readLine();

		Integer xpos, ypos;
		String line, filename;		
		String[] values;
		
		int tilt = 100;
		int side = 256;
		
		int field_count = 1;
		
		while((line = bf.readLine()) != null) {
			values = line.split(LocalThresholdingController.csv_separator);
			
			filename = values[0];
			
			xpos = new Double(values[1]).intValue();
			ypos = new Double(values[2]).intValue();
			
			System.out.println("Processing: "+filename);
			
			MBFImage image = null;
			
			try {
				image = ImageUtilities.readMBF(new File(imagedir+filename));
			}catch(IIOException ex) {
				ex.printStackTrace();
			}
			
			if(image == null) continue;
			
			if(field_count < 10) filename = "000"+(field_count++)+".jpg";
			else if(field_count < 100) filename = "00"+(field_count++)+".jpg";
			else if(field_count < 1000) filename = "0"+(field_count++)+".jpg";
			else filename = (field_count++)+".jpg";
			
			MBFImage crop = image.extractROI(xpos-tilt, ypos-tilt, side, side);
			ImageUtilities.write(crop, new File(outputdir+"t"+filename));
			
		}
		
		bf.close();
	}
	
	public void resize_2_224(String image_src_dir) throws IOException {
		File dir = new File(image_src_dir);
		for(String file : dir.list()) {
			//ignore subdirs
			File f = new File(image_src_dir+File.separator+file); 
			if(!f.isDirectory()) {
				System.out.println("Resizing "+file);
				MBFImage image = ImageUtilities.readMBF(f);
				image = image.extractROI(0, 0, 224, 224);
				ImageUtilities.write(image, f);
			}
		}
	}
	
	public static void main(String[] args) throws Exception{
		
		String root_dir_csbl = "/home/CSBL/diogo/DeepLearning";
		
		String datadir = "/home/dmatos/workspace2/jmire/data/";
		String imagedir = "/home/dmatos/samples_fields_test/";
		String outputdir = "/home/dmatos/tcruzi_test_data/unknown/";
		
		CropSamples cropper = new CropSamples();
		
		//cropper.cropUnknown(datadir, imagedir, outputdir);
		
		cropper.resize_2_224(root_dir_csbl+"/data/tcruzi_data_train_sample/tcruzi/");
		cropper.resize_2_224(root_dir_csbl+"/data/tcruzi_data_train_sample/unknown/");
		cropper.resize_2_224(root_dir_csbl+"/data/tcruzi_data_train/tcruzi/");
		cropper.resize_2_224(root_dir_csbl+"/data/tcruzi_data_train/unknown/");
		cropper.resize_2_224(root_dir_csbl+"/data/tcruzi_data_test/tcruzi/");
		cropper.resize_2_224(root_dir_csbl+"/data/tcruzi_data_test/unknown/");
	}
	
}
