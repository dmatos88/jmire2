package br.mire.utils.file_managers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class MoveFiles {

	public void copyFields(String image_src_dir, String image_dest_dir) throws Exception{

		File dir = new File(image_src_dir);

		//List<String> image_files = new LinkedList<>();


		int field_count = 500;
		String filename;
		
		for(String file : dir.list()) {
			//ignore subdirs
			if(!(new File(image_src_dir+file).isDirectory())) {
				System.out.println("Copying file: "+file);
				//image_files.add(file);			
				if(field_count < 10) filename = "field_000"+(field_count++)+".jpg";
				else if(field_count < 100) filename = "field_00"+(field_count++)+".jpg";
				else filename = "field_0"+(field_count++)+".jpg";
				
				Runtime.getRuntime().exec("cp "+(image_src_dir+file)+" "+image_dest_dir+filename);
			}
		}
	}
	
	public void copyFiles(String image_src_dir, String image_dest_dir) throws Exception{
		File dir = new File(image_src_dir);
		for(String file : dir.list()) {
			//ignore subdirs
			if(!(new File(image_src_dir+file).isDirectory())) {
				System.out.println("Copying file: "+file);
				Runtime.getRuntime().exec("cp "+(image_src_dir+file)+" "+image_dest_dir+file);
			}
		}
	}
	
	public void randomMove(String image_src_dir, String image_dest_dir, double percentage) throws Exception{

		File dir = new File(image_src_dir);
		
		String[] filenames = dir.list();		

		int amount = (int) (percentage * filenames.length);
		
		ArrayList<Integer> random_indexes = new ArrayList<Integer>(amount);
		
		int random;
		
		for(int i = 0; i < amount; i++) {
			do {
				random = (int)(Math.random() * filenames.length);
			}while(random_indexes.contains(random));
			random_indexes.add(random);			
		}
		
		Collections.sort(random_indexes);
		
		String file;
		
		for(int i = 0; i < filenames.length; i++) {
			file = filenames[i];
			//ignore subdirs
			if(random_indexes.contains(i) && !(new File(image_src_dir+file).isDirectory())) {
				System.out.println("Moving file: "+file);				
				Runtime.getRuntime().exec("mv "+(image_src_dir+file)+" "+image_dest_dir+file);
			}
			
		}
	}
	
	public void renameFiles(String src_dir, String dest_dir, String prefix) throws IOException{

		File dir = new File(src_dir);

		//List<String> image_files = new LinkedList<>();


		int field_count = 1;
		String filename;
		
		for(String file : dir.list()) {
			//ignore subdirs
			if(!(new File(src_dir+file).isDirectory())) {
				System.out.println("Copying file: "+file);
				//image_files.add(file);			
				if(field_count < 10) filename = "000"+(field_count++)+".jpg";
				else if(field_count < 100) filename = "00"+(field_count++)+".jpg";
				else if(field_count < 1000) filename = "0"+(field_count++)+".jpg";
				else filename = ""+(field_count++)+".jpg";
				
				Runtime.getRuntime().exec("mv "+(src_dir+file)+" "+dest_dir+prefix+filename);
			}
		}
	}
	
	public static void main(String[] args) throws Exception {

		MoveFiles fileMover = new MoveFiles();

		/*
		String image_src_dir = "/home/dmatos/samples_fields2/2019-07-01/";
		String image_dest_dir = "/home/dmatos/samples_fields_test/";
		fileMover.move(image_src_dir, image_dest_dir);
		*/
		
		String srcdir = "/home/dmatos/tcruzi_data/fields/";
		//fileMover.randomMove(srcdir, destdir, 0.05);
		//fileMover.copyFiles(srcdir, destdir);
		fileMover.renameFiles(srcdir, srcdir, "field");
		
	}
	
}
