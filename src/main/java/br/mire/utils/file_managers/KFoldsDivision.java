package br.mire.utils.file_managers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;

public class KFoldsDivision {

	public static void main(String[] args) throws Exception{

		final String datadir = "/home/dmatos/workspace2/jmire/data/";
		final String kinetoplast_filename = "KINETOPLAST_features.csv";
		final String nucleus_filename = "NUCLEUS_features.csv";
		final String other_filename = "other_artifacts.csv";
		final String input_images_dir = "/home/dmatos/samples_fields/";
		final String output_filename = "KNO_features.csv";

		final int numFolds = 5;

		int parasite_counter = 0;

		//abrir ground truth
		BufferedReader gtReader = new BufferedReader(new FileReader(datadir+"ground_truth.csv"));
		//abrir K
		//abrir N
		//abrir other

		//consumir linha de header
		gtReader.readLine();
		while(gtReader.readLine() != null) {
			parasite_counter++;
		}
		gtReader.close();

		parasite_counter = parasite_counter / 2 + numFolds;	

		//abrir ground truth
		gtReader = new BufferedReader(new FileReader(datadir+"ground_truth.csv"));
		//abrir K
		BufferedReader kReader = new BufferedReader(new FileReader(datadir+kinetoplast_filename));
		//abrir N
		BufferedReader nReader = new BufferedReader(new FileReader(datadir+nucleus_filename));
		//abrir other
		BufferedReader oReader = new BufferedReader(new FileReader(datadir+other_filename));

		//skip headers
		String header = kReader.readLine();
		nReader.readLine();
		oReader.readLine(); 

		//criar diretórios de output
		for(int i = 0; i < numFolds; i++) {
			new File(datadir+"fold_"+i).mkdir();
		}
		
		//20% de amostras = numero de linhas / 5

		int fold_size = parasite_counter / numFolds;
		
		int kFold = 0;
		int counter = 0;

		String fold_dir = datadir + "fold_"+kFold+"/";
		BufferedWriter  output_writer = new BufferedWriter(new FileWriter(fold_dir+output_filename));
		output_writer.write(header);
		while(gtReader.readLine() != null) {
			//get each second line
			String gt_line =  gtReader.readLine();
			String k_line = kReader.readLine();
			String n_line = nReader.readLine();
			String o_line = oReader.readLine();			
			
			String image_filename = gt_line.split(",")[0];
			String image_path = input_images_dir+image_filename;
			MBFImage image = ImageUtilities.readMBF(new File(image_path));
			
			//write output
			ImageUtilities.write(image, new File(fold_dir+image_filename));
			
			output_writer.append("\n"+k_line);
			output_writer.append("\n"+n_line);
			output_writer.append("\n"+o_line);
			output_writer.flush();		
			
			counter++;
			if(counter >= fold_size){
				output_writer.close();
				kFold++;
				fold_dir = datadir + "fold_"+kFold+"/";
				output_writer = new BufferedWriter(new FileWriter(fold_dir+output_filename));
				output_writer.write(header);
				counter = 0;
			}
		}
		

		output_writer.close();
		kReader.close();
		nReader.close();
		oReader.close();
		gtReader.close();	
	}

}
