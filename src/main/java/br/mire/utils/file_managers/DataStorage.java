package br.mire.utils.file_managers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DataStorage {

	public static synchronized void storeData(Object data, String absolutePath) throws Exception {
		// store groups
		FileOutputStream f_out = new FileOutputStream(absolutePath);
		ObjectOutputStream output = new ObjectOutputStream(f_out);
		output.writeObject(data);
		output.flush();
		f_out.close();
	}

	public static synchronized Object restoreData(String absolutePath) throws IOException, ClassNotFoundException {
		FileInputStream f_in = new FileInputStream(absolutePath);
        ObjectInputStream input = new ObjectInputStream(f_in);
        Object o = input.readObject();
        f_in.close();
        return o;
	}
}
