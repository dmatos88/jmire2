package br.mire.utils.file_managers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class TcruziCSVFileManager {

	public static final String csv_separator = ",";
	
	public static int write(String csv_filename, String[] data, String[] header) throws IOException {
		
		int counter = 0;
		
		File csv = new File(csv_filename);

		if (!csv.exists() && header != null) {

			PrintWriter writer = new PrintWriter(csv_filename, "UTF-8");
			//System.out.print(header[0]);
			writer.append(header[0]);
			writer.flush();
			for(int i = 1; i < header.length; i++) {
				writer.append(csv_separator + header[i]);
				writer.flush(); 
			}             
			writer.close();
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(csv_filename, true));

		writer.append("\n"+data[0]);
		for (int i = 1; i < data.length; i++) {
			writer.append(csv_separator + data[i]);
			counter++;
		}
		writer.flush();
		writer.close();
		return counter;
	}
	
	public static void main(String[] args) throws IOException {

		String csv_filename_k = "data/KINETOPLAST_features.csv";
		String csv_filename_n = "data/NUCLEUS_features.csv";
		
		String csv_filename_gd = "data/geodesic_distances.csv";
		
		String new_csv_filename = "data/T_cruzi_cepa_Y_features.csv";
		
		BufferedReader brK = new BufferedReader(new FileReader(csv_filename_k));
		BufferedReader brN = new BufferedReader(new FileReader(csv_filename_n));
		BufferedReader brGd = new BufferedReader(new FileReader(csv_filename_gd));
				
		//header
		String lineK = brK.readLine();
		String lineN = brN.readLine();
		String lineGd = brGd.readLine();
		
		String[] headerK = lineK.split(csv_separator);
		String[] headerN = lineK.split(csv_separator);
		
		String header = "";
		
		for(int i = 0; i < headerK.length - 1; i++) {
			header = header + headerK[i]+"_K" + csv_separator;
		}
		for(int i = 0; i < headerN.length - 1; i++) {
			header = header + headerN[i]+"_N" + csv_separator;
		}
		
		header = header+"geodesic_distance"+csv_separator+"class";
		
		BufferedWriter writer = new BufferedWriter(new FileWriter(new_csv_filename, true));
		
		writer.write(header);
		writer.flush();
		
		lineK = brK.readLine();
		
		lineN = brN.readLine();
		
		lineGd = brGd.readLine();
		
		while(lineK != null && lineN != null && lineGd != null) {			
			String[] valuesK  = lineK.split(csv_separator);
			String[] valuesN  = lineN.split(csv_separator);
			String[] valuesGd = lineGd.split(csv_separator);
			
			int featuresLength = valuesK.length - 1 + valuesN.length - 1 + 2;
			
			String[] featuresVector = new String[featuresLength];
			
			int index = 0;
			
			for(int i = 0; i < valuesK.length - 1; i++) {
				featuresVector[index++] = valuesK[i];
			}
			for(int i = 0; i < valuesN.length - 1; i++) {
				featuresVector[index++] = valuesN[i];
			}
			
			featuresVector[index++] = valuesGd[1];
			featuresVector[index] = "TcruziCepaY";

			writer.append("\n"+featuresVector[0]);
			for (int i = 1; i < featuresVector.length; i++) {
				writer.append(csv_separator + featuresVector[i]);
			}
			writer.flush();
			
			lineK = brK.readLine();
			
			lineN = brN.readLine();
			
			lineGd = brGd.readLine();
			
		}		
		
		writer.close();
		brK.close();
		brN.close();
		brGd.close();
		
		System.exit(0);

	}
	
}
