package br.mire.utils.file_managers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.mire.ml.classifier.tcruzi.TCruzi_Tripomastigote_Giemsa;
import br.mire.segmentation.controllers.LocalThresholdingController;

public class KNMergeCSVFileManager {
	
	public static void mergeUnknown() throws Exception{
		
		String tcruziFilepath = "data/tcruzi_features.csv";
		String unknownFilepath = "data/UNKNOWN.csv";
		String newFilepath = "data/big_tcruzi_features.csv";
		
		BufferedReader tcruzi_br = new BufferedReader(new FileReader(tcruziFilepath));
		
		BufferedWriter writer = new BufferedWriter(new FileWriter(newFilepath));
		
		int num_of_others = 500;
		List<Integer> random_lines = new ArrayList<>(num_of_others);
		for(int i = 0; i < num_of_others; i++) {
			int line_n = (int) (125000.0 * Math.random());
			random_lines.add(line_n);
		}
		Collections.sort(random_lines);
		
		String header = tcruzi_br.readLine();
		
		String[] values = header.split(LocalThresholdingController.csv_separator);
		
		writer.write(values[0]);
		
		for(int i = 1; i < values.length; i++) {
			writer.append(LocalThresholdingController.csv_separator+values[i]);
		}
		writer.flush();
		
		String line;
		
		while((line = tcruzi_br.readLine()) != null) {
			
			values = line.split(LocalThresholdingController.csv_separator);
			
			writer.append("\n"+values[0]);
			
			for(int i = 1; i < values.length; i++) {
				writer.append(LocalThresholdingController.csv_separator+values[i]);
			}
			
			writer.flush();	
		}
		
		tcruzi_br.close();
		
		BufferedReader unknown_br = new BufferedReader(new FileReader(unknownFilepath));
		
		//skip header
		unknown_br.readLine();
		
		int index = 0;
		int counter = -1;
		while((line = unknown_br.readLine()) != null && index < num_of_others) {

			counter += 1;
			
			if(random_lines.get(index) != counter) continue;
			
			index++;
			
			values = line.split(LocalThresholdingController.csv_separator);
			
			writer.append("\n"+values[0]);
			
			for(int i = 1; i < values.length; i++) {
				writer.append(LocalThresholdingController.csv_separator+values[i]);
			}
			
			writer.flush();	
		}
		unknown_br.close();
		
		writer.close();
	}
	
	public static void mergeKNT() throws Exception{
		String[] filenames = {
				"data/KINETOPLAST_CELL_BODY_features.csv",
				"data/KINETOPLAST_features.csv",
				"data/NUCLEUS_CELL_BODY_features.csv",
				"data/NUCLEUS_features.csv",
				"data/T_CRUZI_features.csv"
				
		};
		
		String new_csv_filename = "data/tcruzi_features.csv";
		new File(new_csv_filename).delete();

		BufferedWriter writer = new BufferedWriter(new FileWriter(new_csv_filename, true));

		BufferedReader br = new BufferedReader(new FileReader(filenames[0]));
		
		String header = br.readLine();
		
		String[] values = header.split(LocalThresholdingController.csv_separator);
		
		writer.write(values[0]);
		
		for(int i = 1; i < values.length; i++) {
			writer.append(LocalThresholdingController.csv_separator+values[i]);
		}
		writer.flush();
		
		br.close();
		
		String line;

		for(String filename : filenames) {
			br = new BufferedReader(new FileReader(filename));

			//skip header
			line = br.readLine();
			
			while((line = br.readLine()) != null) {
				values = line.split(LocalThresholdingController.csv_separator);
				
				writer.append("\n"+values[0]);
				
				for(int i = 1; i < values.length; i++) {
					writer.append(LocalThresholdingController.csv_separator+values[i]);
				}
				
				writer.flush();
			}
			
		
			br.close();
		}
		
		writer.close();
	}

	public static void main(String[] args) throws Exception {
		//mergeKNT();
		mergeUnknown();
		System.exit(0);
	}


}
