package br.mire.utils.tests;

import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;

import org.netlib.blas.Lsame;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;

import com.frank.math.Complex;
import com.frank.math.FFT2D;

import br.mire.utils.image.RGB2Lab;
import br.mire.utils.math.Fftshift;
import br.mire.utils.math.FrequencyGaussian;

public class TP02_LoG {

	public static FImage matrix2Image(Complex[][] matrix2) {
		FImage img2 = new FImage(matrix2.length, matrix2[0].length);

		for(int i = 0; i < matrix2.length; i++) {
			for(int j = 0; j < matrix2[0].length; j++) {
				img2.setPixel(i, j, (float) matrix2[i][j].real);
			}
		}

		return img2;
	}

	public static Complex[][] test01(final FImage img, double sigma) {

		Complex[][] matrix2 = new Complex[img.width][img.height];

		for(int i = 0; i < img.width; i++) {
			for(int j = 0; j < img.height; j++) {
				matrix2[i][j] = new Complex(img.getPixel(i,j));
			}
		}

		Complex[][] maskB = FrequencyGaussian.getFourierLoG2d(matrix2.length, matrix2[0].length, sigma);

		maskB = FFT2D.fft(maskB);

		maskB = Fftshift.fftshift(maskB);
		
		//DisplayUtilities.display(TP02_LoG.matrix2Image(maskB)).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		matrix2 = FFT2D.fft(matrix2);

		matrix2 = Fftshift.fftshift(matrix2);
		
		//DisplayUtilities.display(TP02_LoG.matrix2Image(matrix2)).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		for(int i = 0; i < img.width; i++) {
			for(int j = 0; j < img.height; j++) {
				matrix2[i][j] =  matrix2[i][j].multiply(maskB[i][j]);
			}
		}

		//DisplayUtilities.display(TP02_LoG.matrix2Image(matrix2)).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);;
		
		matrix2 = FFT2D.ifft( Fftshift.fftshift(matrix2) );

		matrix2 = Fftshift.ifftshift(matrix2);
		
		return matrix2;
	}
	
	public static void zeroCrossBorders(Complex[][] matrix, double threshold) {
		for(int i = 0; i < matrix.length-1; i++)
			for(int j = 0; j < matrix[0].length-1; j++)
				if(matrix[i][j].real < threshold && matrix[i][j+1].real >= threshold
					|| matrix[i][j].real >= threshold && matrix[i][j+1].real < threshold
					|| matrix[i][j].real >= threshold && matrix[i+1][j].real < threshold
					|| matrix[i][j].real < threshold && matrix[i+1][j].real >= threshold
						)
					matrix[i][j].real = 1.0;
				else
					matrix[i][j].real = 0.0;
	}

	public static void main(String[] args) throws IOException {
		//FImage img = ImageUtilities.readF(new File("/home/dmatos/workspace2/VisionUSP/lenna_gray512.jpg"));
		//FImage img = ImageUtilities.readF(new File("/home/dmatos/workspace2/VisionUSP/testSegment.jpeg"));
		
		MBFImage img;
		String samplesPath = "/home/dmatos/workspace2/vcr/data/target/";
		String field = "";
		String sample = "target2018h";
		String imgName = field + sample + ".jpg";
		img = ImageUtilities.readMBF(new File(samplesPath + imgName)); //.extractCenter(1500, 1000, 400, 400);;

		int width = img.getWidth();
		int height = img.getHeight();

		float[][][] rgb = new float[3][width][height];

		for(int band = 0; band < 3; band++) {
			for(int x = 0; x < width; x++) {
				for(int y = 0; y < height; y++) {
					rgb[band][x][y] = img.getBand(band).getPixel(x,y);
				}
			}
		}

		long start = System.currentTimeMillis();

		float[][][] matrixLab = RGB2Lab.convert(rgb);

		FImage L_star_image = new FImage(matrixLab[0]);
		L_star_image.normalise();

		FImage a_star_image = new FImage(matrixLab[1]);
		a_star_image.normalise();		
		a_star_image.inverse();

		FImage b_star_image = new FImage(matrixLab[2]);
		b_star_image.normalise();	

		double sigma = 3.0;
		float threshold = 0.000001f;

		Complex[][] matrix = TP02_LoG.test01(L_star_image, sigma);//b_star_image.add(a_star_image).add(L_star_image), sigma);
		
		TP02_LoG.zeroCrossBorders(matrix, threshold);
		
		FImage img2 = TP02_LoG.matrix2Image(matrix);
		
		DisplayUtilities.display(img2).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
	}

}
