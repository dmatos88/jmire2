package br.mire.utils.tests;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;

import br.mire.ml.classifier.tcruzi.Prediction;
import br.mire.ml.classifier.tcruzi.TCruziValidation;
import br.mire.ml.classifier.tcruzi.TCruzi_Tripomastigote_Giemsa;

public class TCruziGiemsaTester {
	
	static Logger log = Logger.getLogger(TCruziGiemsaTester.class.getName());
	
	
	public static void main(String[] args) {

		String cwd = System. getProperty("user.dir");
		System. out. println("Current working directory : " + cwd);

		double objective_mag = 100.0;
		double ocular_mag = 10.0;
		double field_number = 20.0;

		String data_dir = "data/";

		log.debug("DATA @ "+data_dir);

		//arff files
		String raw_tcruzi_file = data_dir+"big_tcruzi_features.csv";

		//samples files
		String image_src_dir = "/home/dmatos/tcruzi_test_data/tcruzi/";//"fields/";
		
		String outputdir = "data/output/";

		List<String> image_files = new LinkedList<>();

		/*
		File dir = new File(image_src_dir);
		for(String file : dir.list()) {
			//ignore subdirs
			if(!(new File(image_src_dir+File.separator+file).isDirectory())) {
				image_files.add(file);
			}
		}
		Collections.sort(image_files);
		//*/

		String index = "000_225_2218_field_0055";

		image_files.add(index+".jpg");
		//image_files.add("field[048]count[3].jpg");
		/*
		image_files.add("field[005]count[2].jpg");
		image_files.add("field[048]count[3].jpg");
		image_files.add("field[525]count[2].jpg");		
		image_files.add("field[043]count[1].jpg");
		image_files.add("field[110]count[1].jpg");
		//*/
		

		TCruzi_Tripomastigote_Giemsa giemsa = new TCruzi_Tripomastigote_Giemsa();

		log.debug("TCruzi_Tripomastigote_Giemsa registrado com sucesso.");

		try {
			giemsa.loadData(raw_tcruzi_file);
		} catch (Exception e1) {
			e1.printStackTrace();
		}


		//TCruzi_Tripomastigote_Giemsa giemsa = new TCruzi_Tripomastigote_Giemsa();

		long start = System.currentTimeMillis();

		boolean continue_from_here = false;
		String start_from = index;

		for(String  image_file_name : image_files) {

			if(!continue_from_here && image_file_name.contains(start_from)) continue_from_here = true;

			if(continue_from_here) {
				try {

					log.debug("Running for: "+image_file_name);

					MBFImage image = ImageUtilities.readMBF(new File(image_src_dir+image_file_name));

					//double scale = TCruzi_Tripomastigote_Giemsa.getScaleFromMicroscopicField_LStar(image.getBand(0), ocular_magnification, objective_magnification, field_number);

					/*
					List<Prediction> predictions = giemsa.classify_field_local_thresh(
							image,							
							field_number,
							objective_mag,
							ocular_mag,
							raw_tcruzi_file);
					*/
					List<Prediction> predictions = giemsa.classify_cropped(
							image,							
							field_number,
							objective_mag,
							ocular_mag,
							raw_tcruzi_file);
					
					TCruziValidation validation = new TCruziValidation();
					
					validation.writePredictionsToImage(outputdir, image_file_name, predictions, image);

					/*
					log.debug("Validating "+image_file_name+"...");
					TCruziValidation validation = new TCruziValidation(data_dir+groundTruthFilename, TCruzi_Tripomastigote_Giemsa.last_scale);

					validation.validatePredictions(giemsa, image_file_name, predictions, true, image);

					int number_of_predictions = predictions.size();

					log.debug(number_of_predictions+" predictions made for "+image_file_name);
					int tn = number_of_predictions - validation.getFalseNegativeCount() - validation.getTruePositiveCount() - validation.getFalsePositiveCount();
					log.debug("TN: "+tn);
					log.debug("TP: "+validation.getTruePositiveCount());
					log.debug("FP: "+validation.getFalsePositiveCount());
					log.debug("FN: "+validation.getFalseNegativeCount());
					giemsa.writeResult(image_file_name, "tcruzi", validation.getTruePositiveCount(), validation.getFalsePositiveCount(), 
							validation.getTrueNegativeCount(), validation.getFalseNegativeCount());
					*/

					/*
						log.debug("Waiting ENTER to close displayers");
						System.in.read();
					 */

				} catch(InterruptedException ie) {
					log.debug(ie.getMessage());
					ie.printStackTrace();
					System.exit(1);
				}
				catch(Exception e) {
					log.debug(e.getMessage());
					e.printStackTrace();		

				}
			}
		}

		long total = System.currentTimeMillis() - start;
		log.debug("Total time: "+(total/1000));

		System.exit(0);


	}
}
