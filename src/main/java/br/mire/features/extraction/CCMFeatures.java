package br.mire.features.extraction;

import org.openimaj.image.FImage;
import org.openimaj.image.MBFImage;

import br.mire.segmentation.controllers.LocalThresholdingController;
import br.mire.utils.image.PreprocessField;

/**
 * 
 * @author Diogo Matos dmatos88<at>gmail.com
 *
 */
public class CCMFeatures implements JMIReFeatures{
	
	public enum CCMFeaturesStrategy{
		CCM_ENTROPY_LL("ccm_entropy_ll"),
		CCM_ENTROPY_AA("ccm_entropy_aa"),
		CCM_ENTROPY_BB("ccm_entropy_bb"),
		CCM_ENTROPY_LA("ccm_entropy_la"),
		CCM_ENTROPY_LB("ccm_entropy_lb"),
		CCM_ENTROPY_AB("ccm_entropy_ab"),
		CCM_ASM_LL("ccm_asm_ll"),
		CCM_ASM_AA("ccm_asm_aa"),
		CCM_ASM_BB("ccm_asm_bb"),
		CCM_ASM_LA("ccm_asm_la"),
		CCM_ASM_LB("ccm_asm_lb"),
		CCM_ASM_AB("ccm_asm_ab"),
		CCM_CONTRAST_LL("ccm_contrast_ll"),
		CCM_CONTRAST_AA("ccm_contrast_aa"),
		CCM_CONTRAST_BB("ccm_contrast_bb"),
		CCM_CONTRAST_LA("ccm_contrast_la"),
		CCM_CONTRAST_LB("ccm_contrast_lb"),
		CCM_CONTRAST_AB("ccm_contrast_ab"),
		CCM_IDM_LL("ccm_idm_ll"),
		CCM_IDM_AA("ccm_idm_aa"),
		CCM_IDM_BB("ccm_idm_bb"),
		CCM_IDM_LA("ccm_idm_la"),
		CCM_IDM_LB("ccm_idm_lb"),
		CCM_IDM_AB("ccm_idm_ab"),
		CCM_CORRELATION_LL("ccm_corr_ll"),
		CCM_CORRELATION_AA("ccm_corr_aa"),
		CCM_CORRELATION_BB("ccm_corr_bb"),
		CCM_CORRELATION_LA("ccm_corr_la"),
		CCM_CORRELATION_LB("ccm_corr_lb"),
		CCM_CORRELATION_AB("ccm_corr_ab");
		
		private String header;
		private double value;
		
		CCMFeaturesStrategy(String title) {
			this.header = title;
		}
		
		public double getValue() {
			return value;
		}

		public void setValue(double value) {
			this.value = value;
		}
		
		public String getHeader() {
			return header;
		}
		
	}
	
	public class CoocurrenceMatrix {

		private double[][] corrmat;

		private double entropy;
		private double inverseDifferenceMoment;
		private double angularSecondMoment;
		private double contrast;
		private double correlation;
		private double sum;
		private int numOfLevelsA;
		private int numOfLevelsB;
		private boolean done;
		private boolean doneCorr;
	
		public CoocurrenceMatrix(int numOfLevelsA, int numOfLevelsB) {
			this.numOfLevelsA = numOfLevelsA;
			this.numOfLevelsB = numOfLevelsB;
			this.corrmat = new double[numOfLevelsA][numOfLevelsB];

			this.sum = 0.0;
			done = false;
			doneCorr = false;
		}
		
		public void printCorrMatrix() {
			for(int i = 0; i < numOfLevelsA; i++) {
				System.out.print(corrmat[i][0]);
				for(int j = 1; j < numOfLevelsB; j++) {
					System.out.print("|"+corrmat[i][j]);
				}
				System.out.println();
			}
		}

		private boolean calcFeatures() {
			if(!done) {
				this.normalize();
				
				entropy = 0;
				inverseDifferenceMoment = 0.0;
				angularSecondMoment = 0.0;
				contrast = 0.0;
				int numOfKvalues = (int) Double.max(numOfLevelsA, numOfLevelsB);
				double[] contrastK = new double[numOfKvalues];
				for(int i = 0; i < numOfLevelsA; i++) {
					for(int j = 0; j < numOfLevelsB; j++) {
						
						if(corrmat[i][j] > 0.000) {
							entropy += (corrmat[i][j] * Math.log10(corrmat[i][j]));
						}
						
						inverseDifferenceMoment += (corrmat[i][j] / (1.0 + Math.abs(i - j)*Math.abs(i - j)));
						angularSecondMoment += (corrmat[i][j]*corrmat[i][j]);
						contrastK[Math.abs(i-j)] += corrmat[i][j];
					}
				}
				
				entropy *= -1;
				
				for(int i = 0; i < numOfKvalues; i++) {
					this.contrast += (i*i*contrastK[i]);
				}
				
				this.inverseDifferenceMoment = Math.log10(this.inverseDifferenceMoment);

				done = true;
			}
			return done;			
		}

		public double insertValue(double valueA, double valueB) {
			int indexA = ((int) (valueA*(numOfLevelsA-1)));
			int indexB = ((int) (valueB*(numOfLevelsB-1)));
			corrmat[indexA][indexB] += 1.0;
			sum += 1.0;
			return corrmat[indexA][indexB];
		}

		public void normalize() {
			for(int i = 0; i < numOfLevelsA; i++) {
				for(int j = 0; j < numOfLevelsB; j++) {
					corrmat[i][j] /= sum;
				}
			}
		}

		public double getEntropy() {
			this.calcFeatures();
			return entropy;
		}

		public double getIDM() {
			this.calcFeatures();
			return inverseDifferenceMoment;
		}

		public double getASM() {
			this.calcFeatures();
			return angularSecondMoment;
		}

		public double getContrast() {
			this.calcFeatures();
			return contrast;
		}

		public double getCorrelation() {
			if(!doneCorr) {
				double[] pX = new double[numOfLevelsA];
				double[] pY = new double[numOfLevelsB];
				double meanX = 0;
				double meanY = 0;
				double stdDevX = 0;
				double stdDevY = 0;
				
				for(int i = 0; i < numOfLevelsA; i++) {
					for(int j = 0; j < numOfLevelsB; j++) {
						pX[i] += corrmat[i][j];
						pY[j] += corrmat[i][j];
					}
				}

				for(int i = 0; i < numOfLevelsA; i++) {
					meanX = meanX + ( (double)(i+1) * pX[i]); 
				}
				
				
				for(int i = 0; i < numOfLevelsA; i++) {
					stdDevX = stdDevX + ((pX[i] - meanX)*(pX[i] - meanX)); 
				}
				
				for(int j = 0; j < numOfLevelsB; j++) {
					meanY = meanY + ( (double)(j+1) * pY[j]); 
				}
				
				for(int j = 0; j < numOfLevelsB; j++) {
					stdDevY = stdDevY + ((pY[j] - meanY)*(pY[j] - meanY));
				}

				correlation = 0.0;

				double stdDevXxY = stdDevY * stdDevX;
				
				for(int i = 0; i < numOfLevelsA; i++) {
					for(int j = 0; j < numOfLevelsB; j++) {
						correlation = correlation + corrmat[i][j] * ( (i-meanX)*(j-meanY)  / stdDevXxY);
					}
				}
				
				if(Double.isNaN(correlation)) correlation = 0;
				
				doneCorr = true;
			}
			return correlation;
		}


	}


	private final int numOfLlevels = 100;
	private final int numOfAlevels = 200;
	private final int numOfBlevels = 200;
	
	private CoocurrenceMatrix matLL;
	private CoocurrenceMatrix matAA;
	private CoocurrenceMatrix matBB;
	private CoocurrenceMatrix matLA;
	private CoocurrenceMatrix matLB;
	private CoocurrenceMatrix matAB;
	
	private CCMFeaturesStrategy[] strategies;

	public CCMFeatures(MBFImage normalized_cielab_image, FImage mask, CCMFeaturesStrategy[] ccm_strategies) {
		matLL = new CoocurrenceMatrix(numOfLlevels, numOfLlevels);
		matAA = new CoocurrenceMatrix(numOfAlevels, numOfAlevels);
		matBB = new CoocurrenceMatrix(numOfBlevels, numOfBlevels);
		matLA = new CoocurrenceMatrix(numOfLlevels, numOfAlevels);
		matLB = new CoocurrenceMatrix(numOfLlevels, numOfBlevels);
		matAB = new CoocurrenceMatrix(numOfAlevels, numOfBlevels);
		
		process(normalized_cielab_image, mask);
		
		//matAB.printCorrMatrix();
		
		this.strategies = new CCMFeaturesStrategy[ccm_strategies.length];
		
		for(int i = 0; i < ccm_strategies.length; i++) {
			this.strategies[i] = ccm_strategies[i];
			
			switch(ccm_strategies[i]) {
			case CCM_ASM_AA:
				this.strategies[i].setValue(matAA.getASM());
				break;
			case CCM_ASM_AB:
				this.strategies[i].setValue(matAB.getASM());
				break;
			case CCM_ASM_BB:
				this.strategies[i].setValue(matBB.getASM());
				break;
			case CCM_ASM_LA:
				this.strategies[i].setValue(matLA.getASM());
				break;
			case CCM_ASM_LB:
				this.strategies[i].setValue(matLB.getASM());
				break;
			case CCM_ASM_LL:
				this.strategies[i].setValue(matLL.getASM());
				break;
			case CCM_CONTRAST_AA:
				this.strategies[i].setValue(matAA.getContrast());
				break;
			case CCM_CONTRAST_AB:
				this.strategies[i].setValue(matAB.getContrast());
				break;
			case CCM_CONTRAST_BB:
				this.strategies[i].setValue(matBB.getContrast());
				break;
			case CCM_CONTRAST_LA:
				this.strategies[i].setValue(matLA.getContrast());
				break;
			case CCM_CONTRAST_LB:
				this.strategies[i].setValue(matLB.getContrast());
				break;
			case CCM_CONTRAST_LL:
				this.strategies[i].setValue(matLL.getContrast());
				break;
			case CCM_CORRELATION_AA:
				this.strategies[i].setValue(matAA.getCorrelation());
				break;
			case CCM_CORRELATION_AB:
				this.strategies[i].setValue(matAB.getCorrelation());
				break;
			case CCM_CORRELATION_BB:
				this.strategies[i].setValue(matBB.getCorrelation());
				break;
			case CCM_CORRELATION_LA:
				this.strategies[i].setValue(matLA.getCorrelation());
				break;
			case CCM_CORRELATION_LB:
				this.strategies[i].setValue(matLB.getCorrelation());
				break;
			case CCM_CORRELATION_LL:
				this.strategies[i].setValue(matLL.getCorrelation());
				break;
			case CCM_ENTROPY_AA:
				this.strategies[i].setValue(matAA.getEntropy());
				break;
			case CCM_ENTROPY_AB:
				this.strategies[i].setValue(matAB.getEntropy());
				break;
			case CCM_ENTROPY_BB:
				this.strategies[i].setValue(matBB.getEntropy());
				break;
			case CCM_ENTROPY_LA:
				this.strategies[i].setValue(matLA.getEntropy());
				break;
			case CCM_ENTROPY_LB:
				this.strategies[i].setValue(matLB.getEntropy());
				break;
			case CCM_ENTROPY_LL:
				this.strategies[i].setValue(matLL.getEntropy());
				break;
			case CCM_IDM_AA:
				this.strategies[i].setValue(matAA.getIDM());
				break;
			case CCM_IDM_AB:
				this.strategies[i].setValue(matAB.getIDM());
				break;
			case CCM_IDM_BB:
				this.strategies[i].setValue(matBB.getIDM());
				break;
			case CCM_IDM_LA:
				this.strategies[i].setValue(matLA.getIDM());
				break;
			case CCM_IDM_LB:
				this.strategies[i].setValue(matLB.getIDM());
				break;
			case CCM_IDM_LL:
				this.strategies[i].setValue(matLL.getIDM());
				break;
			}
		}
	}
	
	@Override
	public String[] getFeaturesHeaders() {
		String[] header = new String[this.strategies.length];
		
		for(int i = 0; i < strategies.length; i++) {
			header[i] = strategies[i].getHeader();
		}
		
		return header;
	}

	@Override
	public double[] getFeaturesValues() {
		double[] values = new double[this.strategies.length];

		for(int i = 0; i < strategies.length; i++) {
			values[i] = this.strategies[i].getValue();
		}

		return values;
	}


	private void insertAll(double L, double a, double b) {
		matLL.insertValue(L, L);
		matAA.insertValue(a, a);
		matBB.insertValue(b, b);
		matLA.insertValue(L, a);
		matLB.insertValue(L, b);
		matAB.insertValue(a, b);
	}

	private void process(MBFImage image, FImage mask){
				
		for (int x = 0; x < image.getWidth(); x++) {
			for (int y = 0; y < image.getHeight(); y++) {
				if(mask.getPixel(x, y) > 0f) {					
					insertAll(
							image.getBand(0).getPixel(x,y),
							image.getBand(1).getPixel(x,y),
							image.getBand(2).getPixel(x,y));
				}
			}			
		}
	}

	/**
	 * 
	 * @return An array with all 6 entropy values, corresponding to the 6 CCM matrices 
	 */
	public double[] getEntropy() {
		double[] entropy = new double[6];
		entropy[0] = matLL.getEntropy();
		entropy[1] = matAA.getEntropy();
		entropy[2] = matBB.getEntropy();
		entropy[3] = matLA.getEntropy();
		entropy[4] = matLB.getEntropy();
		entropy[5] = matAB.getEntropy();
		return entropy;
	}
	
	public double[] getIDM() {
		double[] idm = new double[6];
		idm[0] = matLL.getIDM();
		idm[1] = matAA.getIDM();
		idm[2] = matBB.getIDM();
		idm[3] = matLA.getIDM();
		idm[4] = matLB.getIDM();
		idm[5] = matAB.getIDM();
		return idm;
	}
	
	public double[] getASM() {
		double[] asm = new double[6];
		asm[0] = matLL.getASM();
		asm[1] = matAA.getASM();
		asm[2] = matBB.getASM();
		asm[3] = matLA.getASM();
		asm[4] = matLB.getASM();
		asm[5] = matAB.getASM();
		return asm;
	}
	
	public double[] getContrast() {
		double[] contrast = new double[6];
		contrast[0] = matLL.getContrast();
		contrast[1] = matAA.getContrast();
		contrast[2] = matBB.getContrast();
		contrast[3] = matLA.getContrast();
		contrast[4] = matLB.getContrast();
		contrast[5] = matAB.getContrast();
		return contrast;
	}
	
	public double[] getCorrelation() {
		double[] correlation = new double[6];
		correlation[0] = matLL.getCorrelation();
		correlation[1] = matAA.getCorrelation();
		correlation[2] = matBB.getCorrelation();
		correlation[3] = matLA.getCorrelation();
		correlation[4] = matLB.getCorrelation();
		correlation[5] = matAB.getCorrelation();
		return correlation;
	}
}
