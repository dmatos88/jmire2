package br.mire.features.extraction;

import java.io.File;
import java.util.HashMap;

import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;

import br.mire.features.extractor.Curvegram;
import br.mire.segmentation.contour.ComplexSignalContour2;

public class CurvatureFeatures implements JMIReFeatures{

	private Curvegram curvegram;
	private double minK_std_dev; 
	private double maxK_std_dev; 
	private double mean;
	private double stdDev;
	private double variance;
	private double entropy;
	private double bendingEnergy;
	private boolean done = false;	

	private CurvatureFeaturesStrategy[] strategies;
	
	private double getMeanK() {
		if(done) 
			return mean;
		return Double.NEGATIVE_INFINITY;
	}

	private double getMinK() {
		if(done)
			return minK_std_dev;
		return Double.NEGATIVE_INFINITY;
	}


	private double getMaxK() {
		if(done)
			return maxK_std_dev;
		return Double.POSITIVE_INFINITY;
	}


	private double getStdDev() {
		if(done)
			return stdDev;
		return Double.POSITIVE_INFINITY;
	}


	private double getVariance() {
		if(done)
			return variance;
		return Double.POSITIVE_INFINITY;
	}


	private double getEntropy() {
		if(done)
			return entropy;
		return Double.POSITIVE_INFINITY;
	}


	private double getBendingEnergy() {
		if(done)
			return bendingEnergy;
		return Double.POSITIVE_INFINITY;
	}

	public enum CurvatureFeaturesStrategy{		
		MIN_STD_DEV_VALUE("curvature_min_std"),
		MAX_STD_DEV_VALUE("curvature_max_std"),
		MEAN_VALUE("curvature_mean"),
		STD_DEV("curvature_std_dev"),
		VARIANCE("curvature_var"),
		ENTROPY("curvature_entropy"),
		BENDING_ENERGY("curvature_bending_energy");

		private double value = 0;
		private String header;

		CurvatureFeaturesStrategy(String header) {
			this.header = header;
		}

		public double getValue() {
			return value;
		}

		public void setValue(double value) {
			this.value = value;
		}

		public String getHeader() {
			return header;
		}
		
		public String toString() {
			return getHeader();
		}
	}

	public CurvatureFeatures(FImage image, double[] sigma_array, CurvatureFeaturesStrategy[] strategies, double scale) throws Exception {
		double[] sigmas = sigma_array;
		this.curvegram = new Curvegram(image, sigmas);
		this.done = false;

		this.process();

		this.strategies = new CurvatureFeaturesStrategy[strategies.length];
		
		double std_dev = this.getStdDev();

		for(int j = 0; j < strategies.length; j++) {
			
			this.strategies[j] = strategies[j];
			
			switch(strategies[j]) {
			case MIN_STD_DEV_VALUE:
				this.strategies[j].setValue(this.getMinK());
				break;
			case MAX_STD_DEV_VALUE:
				this.strategies[j].setValue(this.getMaxK());
				break;
			case MEAN_VALUE:
				this.strategies[j].setValue(this.getMeanK());
				break;
			case STD_DEV:
				this.strategies[j].setValue(std_dev);
				break;
			case VARIANCE:
				this.strategies[j].setValue(this.getVariance());
				break;
			case ENTROPY:
				this.strategies[j].setValue(this.getEntropy());
				break;
			case BENDING_ENERGY:
				this.strategies[j].setValue(this.getBendingEnergy());
				break;
			default:
				break;
			}			
		}
	}
	
	public CurvatureFeatures(ComplexSignalContour2 contour, double[] sigma_array, CurvatureFeaturesStrategy[] strategies, double scale) throws Exception {
		double[] sigmas = sigma_array;
		this.curvegram = new Curvegram(contour, sigmas);
		this.done = false;

		this.process();

		this.strategies = new CurvatureFeaturesStrategy[strategies.length];

		for(int j = 0; j < strategies.length; j++) {
			
			this.strategies[j] = strategies[j];
			
			switch(strategies[j]) {
			case MIN_STD_DEV_VALUE:
				this.strategies[j].setValue(this.getMinK());
				break;
			case MAX_STD_DEV_VALUE:
				this.strategies[j].setValue(this.getMaxK());
				break;
			case MEAN_VALUE:
				this.strategies[j].setValue(this.getMeanK());
				break;
			case STD_DEV:
				this.strategies[j].setValue(this.getStdDev());
				break;
			case VARIANCE:
				this.strategies[j].setValue(this.getVariance());
				break;
			case ENTROPY:
				this.strategies[j].setValue(this.getEntropy());
				break;
			case BENDING_ENERGY:
				this.strategies[j].setValue(this.getBendingEnergy());
				break;
			}			
		}
	}

	public double[] getFeaturesValues() {
		double[] values = new double[this.strategies.length];

		for(int i = 0; i < strategies.length; i++) {
			values[i] = this.strategies[i].getValue();
		}

		return values;
	}

	public String[] getFeaturesHeaders() {
		String[] values = new String[this.strategies.length];

		for(int i = 0; i < strategies.length; i++) {
			values[i] = this.strategies[i].getHeader();
		}

		return values;
	}

	private void calcCurvatureStats(){

		minK_std_dev = Double.MAX_VALUE;
		maxK_std_dev = Double.MIN_VALUE;
		mean = 0;
		
		double squaredSum = 0.0;
		double counter = 0;

		for(int i = 0; i < curvegram.getCurvegram().length; i++) {
			for(double k : curvegram.getCurvegram()[i]) {
				counter+=1.0;
				mean += k;
				maxK_std_dev = Double.max(k, maxK_std_dev);
				minK_std_dev = Double.min(k,  minK_std_dev);
			}

		}		
		
		for(double k : curvegram.getCurvegram()[0]) {
			squaredSum += (k*k);
		}
		
		if(counter == 0) return;

		mean /= counter;
		
		bendingEnergy = Math.log(squaredSum /  (double)(counter));

		double[] keys = {-0.5, 0.0, 0.5, 1.0};
		double[] histogram = new double[keys.length];
		
		squaredSum = 0.0;
		for(int i = 0; i < curvegram.getCurvegram().length; i++) {
			for(double k : curvegram.getCurvegram()[i]) {
				squaredSum += ((k - mean)*(k - mean));
				
				double normalized_k = (2.0 * (k - minK_std_dev)) / (maxK_std_dev - minK_std_dev) - 1.0;
				for(int j = 0; j < keys.length; j++) {					
					if(normalized_k <= keys[j]) {
						histogram[j] += 1.0;
						break;
					}
				}
			}			
		}	
		
		entropy = 0;
		for(int i = 0; i < histogram.length; i++) {
			entropy = entropy + (histogram[i]/counter) * Math.log(histogram[i]/counter); 
		}
		entropy *= -1.0;
		
		if(Double.isNaN(entropy)) entropy = 0.0;
		
		variance = squaredSum / (counter -1);

		stdDev = Math.sqrt(variance);
		
		minK_std_dev = Double.MAX_VALUE;
		maxK_std_dev = Double.MIN_VALUE;
		for(int i = 0; i < curvegram.getCurvegram().length; i++) {
			for(double k : curvegram.getCurvegram()[i]) {
				maxK_std_dev = Double.max((k-mean)/stdDev, maxK_std_dev);
				minK_std_dev = Double.min((k-mean)/stdDev,  minK_std_dev);
				
			}
		}
		
	}

	private void process() {

		if(!done) {
			this.calcCurvatureStats();
		}

		done = true;
	}

	public static void main(String[] args) throws Exception {
		String path = "/home/dmatos/workspace2/JMIRe/mask.jpg";

		FImage img = ImageUtilities.readF(new File(path));

		double[] sigmas = {100, 10};

		//TODO remover null para testes
		CurvatureFeatures cf = new CurvatureFeatures(img, sigmas, null, 0);

		cf.process();

		System.out.println("Bending energy: "+cf.getBendingEnergy());
		System.out.println("Entropy: "+cf.getEntropy());
	}
}
