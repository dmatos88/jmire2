package br.mire.features.extraction;

import java.io.File;

import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.processing.transform.FProjectionProcessor;
import org.openimaj.math.geometry.point.Point2d;
import org.openimaj.math.geometry.transforms.TransformUtilities;

import Jama.Matrix;
import br.mire.segmentation.contour.ComplexSignalContour2;
import br.mire.utils.math.Coord2D;

public class GeometricFeatures implements JMIReFeatures{

	private GeometricFeaturesStrategy[] strategies;

	public static double perimeter(ComplexSignalContour2 contourSignal) throws Exception{

		double perimeter = 0.00;

		for(int code : contourSignal.getChaincode()) {
			if(code % 2 == 0) {
				perimeter += 1.0;
			} else {
				perimeter += Math.sqrt(2);
			}
		}

		return perimeter;
	}

	public static double area(ComplexSignalContour2 contourSignal) throws Exception{

		double area = 0.0;

		double[] xsignal = contourSignal.getXSignal();
		double[] ysignal = contourSignal.getYSignal();

		for(int i = 0; i < xsignal.length-1; i++) {
			area = area + ( xsignal[i]  * (ysignal[i+1] - ysignal[i]) - ysignal[i] * (xsignal[i+1] - xsignal[i]));
		}

		return area / 2.0;
	}

	public enum GeometricFeaturesStrategy{		

		MINOR_AXIS("minor_axis"), //41
		MAJOR_AXIS("major_axis"), //42
		AREA("area"), //43
		PARIMETER("perimeter"),//44
		CIRCULARITY("circularity"), //45
		THICKNESS_RATIO("thickness_ratio"), //46
		ASPECT_RATIO("aspect_ratio"), //47
		MAJOR_AXIS_PERIMETER_RATIO("major_axis_perimeter_ratio"), //48
		MAX_DIST_BOUNDARY_2_CENTROID("max_dist_boundary_centroid"), //49
		MIN_DIST_BOUNDARY_2_CENTROID("min_dist_boundary_centroid"), //50
		MEAN_DIST_BOUNDARY_2_CENTROID("mean_dist_boundary_centroid"), //51
		BILATERAL_SYMMETRY("bilateral_symmetry"); //52

		private String header;
		private double value;

		private GeometricFeaturesStrategy(String header) {
			this.header = header;
		}

		public double getValue() {
			return value;
		}

		public void setValue(double value) {
			this.value = value;
		}

		public String getHeader() {
			return header;
		}
	}

	/**
	 * 
	 * @param image binary image with one for object pixels and zero for background pixels 
	 * @return object centroid if the contains 1.0f value pixels, null otherwise
	 */
	public static Coord2D centroid(ComplexSignalContour2 contourSignal) throws Exception{

		double sumX = 0;
		double sumY = 0;

		//ComplexSignalContour2 contourSignal = new ComplexSignalContour2(image);	
		double[] signalX = contourSignal.getXSignal();
		double[] signalY = contourSignal.getYSignal();

		for(int i = 0; i < signalX.length; i++) {			
			sumX += signalX[i];
			sumY += signalY[i];
		}

		if(signalX.length > 0) {
			return new Coord2D(Math.ceil(sumX/signalX.length), Math.ceil(sumY/signalX.length));
		}

		return null;
	}

	public static double maxBoundary2Centroid(ComplexSignalContour2 contourSignal, Coord2D centroid) throws Exception{
		double max = 0.0;

		//ComplexSignalContour2 contourSignal = new ComplexSignalContour2(image);	

		double[] vectorX = contourSignal.getXSignal();
		double[] vectorY = contourSignal.getYSignal();

		//Coord2D centroid = centroid(contourSignal);
		Coord2D temp = null;

		for(int i = 0; i < vectorX.length; i++) {
			temp = new Coord2D(vectorX[i], vectorY[i]);
			max = Double.max(max, centroid.getEuclideanDistance(temp));
		}

		return max;
	}

	public static double minBoundary2Centroid(ComplexSignalContour2 contourSignal, Coord2D centroid) throws Exception{
		double min = Double.MAX_VALUE;

		//ComplexSignalContour2 contourSignal = new ComplexSignalContour2(image);	

		double[] vectorX = contourSignal.getXSignal();
		double[] vectorY = contourSignal.getYSignal();

		//Coord2D centroid = centroid(contourSignal);
		Coord2D temp = null;

		for(int i = 0; i < vectorX.length; i++) {
			temp = new Coord2D(vectorX[i], vectorY[i]);
			min = Double.min(min, centroid.getEuclideanDistance(temp));
		}

		return min;
	}

	public static double meanBoundary2Centroid(ComplexSignalContour2 contourSignal, Coord2D centroid) throws Exception{
		double sum = 0.0;
		double counter = 0.0;

		//ComplexSignalContour2 contourSignal = new ComplexSignalContour2(image);	

		double[] vectorX = contourSignal.getXSignal();
		double[] vectorY = contourSignal.getYSignal();

		//Coord2D centroid = centroid(contourSignal);
		Coord2D temp = null;

		for(int i = 0; i < vectorX.length; i++) {
			temp = new Coord2D(vectorX[i], vectorY[i]);
			sum += centroid.getEuclideanDistance(temp);
			counter += 1.0;
		}

		double mean = 0.0;
		if(counter > 0) {
			mean = sum / counter;
		}

		return mean;
	}

	/**
	 * 
	 * @param image
	 * @return minor axis length at [0] and major axis length at [1]
	 */
	public static double[] principalAxis(ComplexSignalContour2 contourSignal) throws Exception{

		//ComplexSignalContour2 contourSignal = new ComplexSignalContour2(image);	

		double[] vectorX = contourSignal.getXSignal();
		double[] vectorY = contourSignal.getYSignal();

		double[][] dMx = new double[vectorX.length][2];

		double maxDist = 0.0;

		for(int i = 0; i < vectorX.length; i++) {
			dMx[i][0] = vectorX[i];
			dMx[i][1] = vectorY[i];
			for(int j = i+1; j < vectorX.length; j++) {
				double a = (vectorX[i]-vectorX[j]);
				double b = (vectorY[i]-vectorY[j]);
				maxDist = Double.max(maxDist, Math.sqrt(b*b+a*a));
			}
		}

		RealMatrix rMx = MatrixUtils.createRealMatrix(dMx);

		Covariance covObj = new Covariance(rMx);

		RealMatrix covMx = covObj.getCovarianceMatrix();

		EigenDecomposition eigenD = new EigenDecomposition(covMx);

		double[] realEigenValues = eigenD.getRealEigenvalues();

		// largest eigenValue = minor axis
		// smallest eigenValue = major axis
		// 1/sqrt(eigenValue) = axis length

		double[] axes = new double[2];

		realEigenValues = eigenD.getRealEigenvalues();

		if(realEigenValues[0] > realEigenValues[1]) {
			axes[0] = realEigenValues[0]; //major
			axes[1] = realEigenValues[1]; //minor
			//System.out.println("major axis in ev1");
		} else {
			axes[0] = realEigenValues[1];
			axes[1] = realEigenValues[0];
			//System.out.println("major axis in ev2");
		}

		//Normalized axes
		//axes[0] = 1.0/Math.sqrt(axes[0]);
		//axes[1] = 1.0/Math.sqrt(axes[1]);

		//LinePlot for image purposes
		/*

		final XYSeriesCollection dataset = new XYSeriesCollection();

		double[][] evecs = eigenD.getV().getData();

		final XYSeries line1 = new XYSeries( "ev1");
		line1.add(0,0);
		line1.add(evecs[0][0]*axes[0], evecs[1][0]*axes[0]);

		final XYSeries line2 = new XYSeries( "ev2");
		line2.add(0,0);
		line2.add(evecs[0][1]*axes[1], evecs[1][1]*axes[1]);

		dataset.addSeries(line1);
		dataset.addSeries(line2);

		LinePlot.linePlotDataset("eigenaxis plot", "eigenaxis", "X", "Y", dataset);
		//*/

		double ratio = axes[1]/axes[0];		
		axes[0] = maxDist;
		axes[1] = maxDist * ratio;

		return axes;
	}

	public static double getCircularity(double perimeter, double area) {
		return perimeter*perimeter/area;
	}

	public static double getThicknessRatio(double perimeter, double area) {
		return 4.0 * Math.PI * area / (perimeter*perimeter);
	}

	public static double getAspectRatio(double minorAxisLength, double majorAxisLength) {
		return minorAxisLength / majorAxisLength;
	}

	public static double getMajorAxisPerimeterRatio(double majorAxisLength, double perimeter) {
		return majorAxisLength / perimeter;
	}

	private static FImage mirrorVerticallyAboutLine(FImage image, int line_index) {

		FImage newImage = new FImage(image.width, image.height);

		int index = 0;

		while(line_index - index >= 0 && line_index + index < image.height) {
			int index_above = line_index - index;
			int index_below = line_index + index;
			for(int x = 0; x < image.width; x++) {
				newImage.setPixel(x, index_above, image.getPixel(x, index_below));
				newImage.setPixel(x, index_below, image.getPixel(x, index_above));
			}
			index++;
		}

		return newImage;

	}

	private static FImage mirrorHorizontallyAboutCollumn(FImage image, int col_index) {

		FImage newImage = new FImage(image.width, image.height);

		int index = 0;

		while(col_index - index >= 0 && col_index + index < image.width) {
			int index_above = col_index - index;
			int index_below = col_index + index;
			for(int y = 0; y < image.height; y++) {
				newImage.setPixel(index_above, y, image.getPixel(index_below, y));
				newImage.setPixel(index_below, y, image.getPixel(index_above, y));
			}
			index++;
		}

		return newImage;

	}
	
	/**
	 * 
	 * @param areaImage binary image of the area of the object
	 * @param centroid
	 * @param principalAxisFirstComponent
	 * @return bilateral simmetry around major axis
	 * @throws Exception
	 */
	public static double getSymmetry(FImage areaImage, Coord2D centroid, ComplexSignalContour2 contourSignal) throws Exception {
		
		double[] vectorX = contourSignal.getXSignal();
		double[] vectorY = contourSignal.getYSignal();

		double[][] dMx = new double[vectorX.length][2];

		double maxDist = 0.0;

		Coord2D principalAxisFirstComponent = new Coord2D(0,0);

		for(int i = 0; i < vectorX.length; i++) {
			dMx[i][0] = vectorX[i];
			dMx[i][1] = vectorY[i];
			for(int j = i+1; j < vectorX.length; j++) {
				double a = (vectorX[i]-vectorX[j]);
				double b = (vectorY[i]-vectorY[j]);
				double newMaxDist = Double.max(maxDist, Math.sqrt(b*b+a*a));{
					if(newMaxDist > maxDist) {
						maxDist = newMaxDist;
						principalAxisFirstComponent = new Coord2D(vectorX[i], vectorY[i]);
					}
				}
			}
		}

		
		double x = centroid.getX() - principalAxisFirstComponent.getX();
		double y = centroid.getY() - principalAxisFirstComponent.getY();

		double theta = -Math.atan2(y, x);

		final Matrix rotation = TransformUtilities.rotationMatrixAboutPoint((float)theta, (float)centroid.getX(), (float)centroid.getY());
		final FProjectionProcessor pp = new FProjectionProcessor();
		pp.setMatrix(rotation);
		pp.accumulate(areaImage);
		FImage rotImage = pp.performProjection(false, 0f);

		rotImage.threshold(0.1f);

		contourSignal = new ComplexSignalContour2(rotImage, 0, 0);

		Coord2D new_centroid =  GeometricFeatures.centroid(contourSignal);

		FImage mirroredV = mirrorVerticallyAboutLine(rotImage, (int) Math.ceil(new_centroid.getY()));

		mirroredV.addInplace(rotImage);
		
		//DisplayUtilities.display(mirroredV.divide(2f)).setResizable(true);

		//Close close = new Close();
		//close.processImage(rotImage);			

		double n = 0.0;
		double n2 = 0.0;

		for(int i = 0; i < mirroredV.width; i++) {
			for(int j = 0; j < mirroredV.height; j++) {
				float pxValue = mirroredV.getPixel(i, j);
				if(pxValue > 0) {
					if(pxValue == 2)
						n2 += 1.0;
					n += 1.0;
				}
			}
		}

		if(n > 0)
			return  n2 / n;
		return 0;

	}

	public static double[] getVHSimmetry(FImage areaImage, Coord2D centroid, ComplexSignalContour2 contourSignal) throws Exception {
		
		double[] vectorX = contourSignal.getXSignal();
		double[] vectorY = contourSignal.getYSignal();

		double[][] dMx = new double[vectorX.length][2];

		double maxDist = 0.0;

		Coord2D principalAxisFirstComponent = new Coord2D(0,0);

		for(int i = 0; i < vectorX.length; i++) {
			dMx[i][0] = vectorX[i];
			dMx[i][1] = vectorY[i];
			for(int j = i+1; j < vectorX.length; j++) {
				double a = (vectorX[i]-vectorX[j]);
				double b = (vectorY[i]-vectorY[j]);
				double newMaxDist = Double.max(maxDist, Math.sqrt(b*b+a*a));{
					if(newMaxDist > maxDist) {
						maxDist = newMaxDist;
						principalAxisFirstComponent = new Coord2D(vectorX[i], vectorY[i]);
					}
				}
			}
		}
		double x = centroid.getX() - principalAxisFirstComponent.getX();
		double y = centroid.getY() - principalAxisFirstComponent.getY();

		double theta = -Math.atan2(y, x);

		//System.out.println("Theta: "+theta);

		FImage area2Display = areaImage.clone();
		/*
		area2Display.drawLine(
				(int)centroid.getX(), (int)centroid.getY(), 
				(int)principalAxisFirstComponent.getX(), 
				(int)principalAxisFirstComponent.getY(), 0f);
		//*/

		//DisplayUtilities.display(area2Display).setResizable(true);

		final Matrix rotation = TransformUtilities.rotationMatrixAboutPoint((float)theta, (float)centroid.getX(), (float)centroid.getY());
		final FProjectionProcessor pp = new FProjectionProcessor();
		pp.setMatrix(rotation);
		pp.accumulate(area2Display);
		FImage rotImage = pp.performProjection(false, 0f);

		rotImage.threshold(0.1f);

		//DisplayUtilities.display(rotImage).setResizable(true);	

		contourSignal = new ComplexSignalContour2(rotImage, 0, 0);

		Coord2D new_centroid =  GeometricFeatures.centroid(contourSignal);

		System.out.println("new centroid: "+new_centroid.toString());

		FImage mirroredV = mirrorVerticallyAboutLine(rotImage, (int) Math.ceil(new_centroid.getY()));

		//DisplayUtilities.display(mirroredV).setResizable(true);;

		mirroredV.addInplace(rotImage);

		//DisplayUtilities.display(mirroredV.divide(2f)).setResizable(true);

		//Close close = new Close();
		//close.processImage(rotImage);			

		double n = 0.0;
		double n2 = 0.0;

		double[] simmetryValues = new double[2];
		simmetryValues[0] = simmetryValues[1] = 0;

		for(int i = 0; i < mirroredV.width; i++) {
			for(int j = 0; j < mirroredV.height; j++) {
				float pxValue = mirroredV.getPixel(i, j);
				if(pxValue > 0) {
					if(pxValue == 2)
						n2 += 1.0;
					n += 1.0;
				}
			}
		}

		if(n > 0)
			simmetryValues[0] =  n2 / n;

		FImage mirroredH = mirrorHorizontallyAboutCollumn(rotImage, (int) Math.ceil(new_centroid.getX()));
		mirroredH.addInplace(rotImage);
		//DisplayUtilities.display(mirroredH.divide(2f)).setResizable(true);

		n2 = n = 0;

		for(int i = 0; i < mirroredH.width; i++) {
			for(int j = 0; j < mirroredH.height; j++) {
				float pxValue = mirroredH.getPixel(i, j);
				if(pxValue > 0) {
					if(pxValue == 2)
						n2 += 1.0;
					n += 1.0;
				}
			}
		}

		if(n > 0)
			simmetryValues[1] =  n2 / n;

		return simmetryValues;
	}

	public static void main(String[] args) throws Exception {
		String path = "/home/dmatos/nucleus2.png";

		FImage img = ImageUtilities.readF(new File(path));

		img.threshold(0.1f);

		ComplexSignalContour2 contourSignal = new ComplexSignalContour2(img, 0, 0);
		
		Coord2D centroid = GeometricFeatures.centroid(contourSignal);

		System.out.println("centroid: "+centroid.toString());
		
		double s = GeometricFeatures.getSymmetry(img, centroid, contourSignal);
		
		System.out.println("simetry: "+s);

		/*
		 * 
		double[] simmetryValues = GeometricFeatures.getVHSimmetry(img, centroid, contourSignal);

		System.out.println("Simmetry v: "+simmetryValues[0]);
		System.out.println("Simmetry H: "+simmetryValues[1]);

		/**
        DisplayUtilities.display(img);

		ComplexSignalContour2 contourSignal = new ComplexSignalContour2(img, 0, 0);

		final XYSeries contour = new XYSeries( "contour" );          
		for(int i = 0; i < contourSignal.getXSignal().length; i++ ) {
			contour.add( (double)contourSignal.getXSignal()[i] ,(double) contourSignal.getYSignal()[i] );
		}

		final XYSeriesCollection dataset = new XYSeriesCollection( );          
		dataset.addSeries( contour );
		//				LineChartPloter chart = new LineChartPloter("contorno",
		//						"paramentrico", dataset);
		//				chart.pack( );          
		//				RefineryUtilities.centerFrameOnScreen( chart );          
		//				chart.setVisible( true );
		ScatterPlot.scatterPlotDataset(dataset, "Geometric Features", "x", "y");

		double[] axes = GeometricFeatures.principalAxis(contourSignal);

		//major axis
		System.out.println(axes[0]);
		//minor axis
		System.out.println(axes[1]);

		double ocular_zoom = 10.0;
		double objective_zoom = 100.0;
		double field_number = 20.0;
		double real_field_diameter = field_number / objective_zoom;
		double pixel_side_in_mm = real_field_diameter / axes[0]; ///((axes[0] + axes[1]) / 2.0);
		double pixel_side_in_um = pixel_side_in_mm * objective_zoom * ocular_zoom;

		double mice_rbc_diameter_in_px = 81; 

		System.out.println("Pixel side: "+pixel_side_in_um+"um");


		System.out.println("Mice RBC diameter: "+mice_rbc_diameter_in_px * pixel_side_in_um);

		 */
	}

	public GeometricFeatures(FImage binaryImage, GeometricFeaturesStrategy[] strategies, ComplexSignalContour2 contourSignal, double scale) throws Exception {
		this.strategies = new GeometricFeaturesStrategy[strategies.length];		

		Coord2D centroid = centroid(contourSignal);

		double perimeter = perimeter(contourSignal) * scale;
		double area = area(contourSignal) * scale;
		double[] axis = principalAxis(contourSignal);

		axis[0] *= scale;
		axis[1] *= scale;

		for(int i = 0; i < strategies.length; i++) {
			this.strategies[i] = strategies[i];

			switch(strategies[i]) {
			case AREA:
				this.strategies[i].setValue(area);
				break;
			case ASPECT_RATIO:
				this.strategies[i].setValue(getAspectRatio(axis[1], axis[0]));
				break;
			case CIRCULARITY:
				this.strategies[i].setValue(getCircularity(perimeter, area));
				break;
			case MAJOR_AXIS:
				this.strategies[i].setValue(axis[0]);
				break;
			case MAJOR_AXIS_PERIMETER_RATIO:
				this.strategies[i].setValue(getMajorAxisPerimeterRatio(axis[0], perimeter));
				break;
			case MAX_DIST_BOUNDARY_2_CENTROID:
				this.strategies[i].setValue(maxBoundary2Centroid(contourSignal, centroid)*scale);
				break;
			case MEAN_DIST_BOUNDARY_2_CENTROID:
				this.strategies[i].setValue(meanBoundary2Centroid(contourSignal, centroid)*scale);
				break;
			case MINOR_AXIS:
				this.strategies[i].setValue(axis[1]);
				break;
			case MIN_DIST_BOUNDARY_2_CENTROID:
				this.strategies[i].setValue(minBoundary2Centroid(contourSignal, centroid)*scale);
				break;
			case PARIMETER:
				this.strategies[i].setValue(perimeter);
				break;
			case THICKNESS_RATIO:
				this.strategies[i].setValue(getThicknessRatio(perimeter, area));
				break;
			case BILATERAL_SYMMETRY:
				this.strategies[i].setValue(getSymmetry(binaryImage, centroid, contourSignal));
				break;
			}
		}		
	}

	@Override
	public String[] getFeaturesHeaders() {
		String[] header = new String[this.strategies.length];

		for(int i = 0; i < strategies.length; i++) {
			header[i] = strategies[i].getHeader();
		}

		return header;
	}

	@Override
	public double[] getFeaturesValues() {
		double[] values = new double[this.strategies.length];

		for(int i = 0; i < strategies.length; i++) {
			values[i] = this.strategies[i].getValue();
		}

		return values;
	}

}
