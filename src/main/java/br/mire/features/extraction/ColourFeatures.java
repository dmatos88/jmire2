package br.mire.features.extraction;

import org.openimaj.image.FImage;
import org.openimaj.image.MBFImage;
import org.openimaj.image.connectedcomponent.ConnectedComponentLabeler;
import org.openimaj.image.connectedcomponent.proc.ColourDescriptor;
import org.openimaj.image.pixel.ConnectedComponent;

public class ColourFeatures implements JMIReFeatures{

	public enum ColourFeaturesStrategy{
		MEAN("colour_mean"),
		MEDIAN("colour_median"),
		MODE("colour_mode"),
		RANGE("colour_range"),
		VARIANCE("colour_variance");

		private String header;
		private double value;

		ColourFeaturesStrategy(String title) {
			this.header = title;
		}

		public double getValue() {
			return value;
		}

		public void setValue(double value) {
			this.value = value;
		}

		public String getHeader() {
			return header;
		}

	}

	private ColourFeaturesStrategy[] strategies;

	public ColourFeatures(ColourFeaturesStrategy[] strategies, FImage mask, MBFImage image) {
		this.strategies = new ColourFeaturesStrategy[strategies.length];
		
		ConnectedComponentLabeler cclabeler = new ConnectedComponentLabeler(
				ConnectedComponentLabeler.Algorithm.TWO_PASS,
				0.5f,
				ConnectedComponent.ConnectMode.CONNECT_8);	
		
		cclabeler.analyseImage(mask);
		
		ColourDescriptor colourDesc = new ColourDescriptor();
		colourDesc.setImage(image);
		colourDesc.process(cclabeler.getComponents().get(0));
		
		double[] colourFeaturesVector = colourDesc.getFeatureVectorArray();

		for(int i = 0; i < strategies.length; i++) {
			this.strategies[i] = strategies[i];
			
			switch(strategies[i]) {
			case MEAN:
				this.strategies[i].setValue(colourFeaturesVector[0]);
				break;
			case MEDIAN:
				this.strategies[i].setValue(colourFeaturesVector[1]);
				break;
			case MODE:
				this.strategies[i].setValue(colourFeaturesVector[2]);
				break;
			case RANGE:
				this.strategies[i].setValue(colourFeaturesVector[3]);
				break;
			case VARIANCE:
				this.strategies[i].setValue(colourFeaturesVector[4]);
				break;
			default:
				break;
			}
		}
	}



	@Override
	public String[] getFeaturesHeaders() {
		String[] header = new String[this.strategies.length];

		for(int i = 0; i < strategies.length; i++) {
			header[i] = strategies[i].getHeader();
		}

		return header;
	}

	@Override
	public double[] getFeaturesValues() {
		double[] values = new double[this.strategies.length];

		for(int i = 0; i < strategies.length; i++) {
			values[i] = this.strategies[i].getValue();
		}

		return values;
	}
}
