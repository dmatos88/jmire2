package br.mire.features.extraction;

public interface JMIReFeatures {

	public String[] getFeaturesHeaders();
	
	public double[] getFeaturesValues();
}
