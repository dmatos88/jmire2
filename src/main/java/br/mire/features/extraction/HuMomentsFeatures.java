package br.mire.features.extraction;

import org.openimaj.image.FImage;
import org.openimaj.image.connectedcomponent.ConnectedComponentLabeler;
import org.openimaj.image.connectedcomponent.proc.HuMoments;
import org.openimaj.image.pixel.ConnectedComponent;

public class HuMomentsFeatures implements JMIReFeatures{

	public enum HuMomentsStrategy{
		MOMENT_1ST("hu_1st_moment"),
		MOMENT_2ND("hu_2nd_moment"),
		MOMENT_3RD("hu_3rd_moment"),
		MOMENT_4TH("hu_4th_moment"),
		MOMENT_5TH("hu_5th_moment"),
		MOMENT_6TH("hu_6th_moment"),
		MOMENT_7TH("hu_7th_moment");

		private String header;
		private double value;

		HuMomentsStrategy(String title) {
			this.header = title;
		}

		public double getValue() {
			return value;
		}

		public void setValue(double value) {
			this.value = value;
		}

		public String getHeader() {
			return header;
		}

	}

	private HuMomentsStrategy[] strategies;

	public HuMomentsFeatures(HuMomentsStrategy[] strategies, FImage mask) {
		this.strategies = new HuMomentsStrategy[strategies.length];
		
		ConnectedComponentLabeler cclabeler = new ConnectedComponentLabeler(
				ConnectedComponentLabeler.Algorithm.TWO_PASS,
				0.5f,
				ConnectedComponent.ConnectMode.CONNECT_8);	
		
		cclabeler.analyseImage(mask);
		
		HuMoments huMoments = new HuMoments();
		huMoments.process(cclabeler.getComponents().get(0));
		
		double[] huMomentsArray = huMoments.getFeatureVectorArray();

		for(int i = 0; i < strategies.length; i++) {
			this.strategies[i] = strategies[i];
			
			this.strategies[i].setValue(0);
			
			switch(strategies[i]) {			
			
			case MOMENT_1ST:
				if(huMomentsArray[0] != 0.00000)
					this.strategies[i].setValue(-Math.signum(huMomentsArray[0])*Math.log(Math.abs(huMomentsArray[0])));
				break;
			case MOMENT_2ND:
				if(huMomentsArray[1] != 0.00000)
					this.strategies[i].setValue(-Math.signum(huMomentsArray[1])*Math.log(Math.abs(huMomentsArray[1])));
				break;
			case MOMENT_3RD:
				if(huMomentsArray[2] != 0.00000)					
					this.strategies[i].setValue(-Math.signum(huMomentsArray[2])*Math.log(Math.abs(huMomentsArray[2])));
				break;
			case MOMENT_4TH:
				if(huMomentsArray[3] != 0.00000)					
					this.strategies[i].setValue(-Math.signum(huMomentsArray[3])*Math.log(Math.abs(huMomentsArray[3])));
				break;
			case MOMENT_5TH:
				if(huMomentsArray[4] != 0.00000)
					this.strategies[i].setValue(-Math.signum(huMomentsArray[4])*Math.log(Math.abs(huMomentsArray[4])));
				break;
			case MOMENT_6TH:
				if(huMomentsArray[5] != 0.00000)
					this.strategies[i].setValue(-Math.signum(huMomentsArray[5])*Math.log(Math.abs(huMomentsArray[5])));
				break;
			case MOMENT_7TH:
				if(huMomentsArray[6] != 0.00000)
					this.strategies[i].setValue(-Math.signum(huMomentsArray[6])*Math.log(Math.abs(huMomentsArray[6])));
				break;
			default:
				break;
			
			}
		}
	}



	@Override
	public String[] getFeaturesHeaders() {
		String[] header = new String[this.strategies.length];

		for(int i = 0; i < strategies.length; i++) {
			header[i] = strategies[i].getHeader();
		}

		return header;
	}

	@Override
	public double[] getFeaturesValues() {
		double[] values = new double[this.strategies.length];

		for(int i = 0; i < strategies.length; i++) {
			values[i] = this.strategies[i].getValue();
		}

		return values;
	}

}
