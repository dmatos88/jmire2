package br.mire.features.extractor.utils;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.frank.math.Complex;
import com.frank.math.FFT1D;

import br.mire.utils.math.Fftshift;
import br.mire.utils.math.FourierTransform;
import br.mire.utils.ploters.LinePlot;
import weka.core.Utils;

public class FourierBasedCurvatureEstimation {
	
	private int N;
	private double T;
	private double a;
	private double[] u;
	private double[] dU;
	private double[] ddU;
	private double[] gaussian;
	private double[] f;
	private double[] ru;
			
	public FourierBasedCurvatureEstimation(final double[] u, final double a ) {
		this.N = u.length;
		this.a = a;
		
		this.f = new double[N];
		this.dU = new double[N];
		this.ddU = new double[N];
		this.gaussian = new double[N];
		this.u = new double[N];
		this.ru = new double[N];
		
		this.T = 2*Math.PI/(N-1);
		
		for(int i = 0; i < u.length; i++) {
			this.u[i] = u[i] ;
		}
		double tau = 1.0/a;
		int i = 0;
		for(double s = -Math.floor(N/2); s <= N - Math.floor(N/2)-1; s+=1, i++) {	
			this.f[i] = s/(double)(N*T);
			this.gaussian[i] = Math.exp(-(4*Math.PI*Math.PI)*f[i]*f[i] / (2*tau*tau) );
		}
		
		this.values_2_proportion(gaussian);
		this.calculateDiffs2();
	}
	
	public void values_2_proportion(double[] values) {
		double sum = Utils.sum(values);
		
		for(int i = 0; i < values.length; i++) {
			values[i] /= sum;
		}
	}
	
	public final double[] getRu() {
		return this.ru;
	}
	
	public final double getT() {
		return T;
	}
	
	public final double[] getdU(){
		return this.dU;
	}
	
	public final double[] getddU() {
		return this.ddU;
	}
	
	public final double[] getGaussian() {
		return this.gaussian;
	}
	
	public final double[] getF() {
		return this.f;
	}
	
	public final double get_a() {
		return this.a;
	}
	
	public final double[] get_u() {
		return this.u;
	}
	
	public final int getN() {
		return this.N;
	}
	
	
	public double perimeter(double[] absolute_u, double[] absolute_ru) {
		
		double L = 0.0;
		double La = 0.0;
		for(int i = 0; i < absolute_u.length; i++) {
			L += absolute_u[i];
			La += absolute_ru[i];
		}
		
		L = 2.0*Math.PI/(double)absolute_u.length*L;
		La = 2.0*Math.PI/(double)absolute_ru.length*La;
	
		return L/La;		
	}

	/**
	 * @deprecated
	 */
	private void calculateDiffs(){
		double[] Gs = FourierTransform.dft1D(this.get_u());

		double[] Gf2 = Fftshift.fftshift(Gs);

		double[] du = new double[this.getN()*2];
		double[] ddu = new double[this.getN()*2];
		double[] uL = new double[this.getN()*2];
		double[] rTemp = new double[this.getN()*2];
		
		for(int i = 0; i < this.getN(); i++) {	
			
			double[] complexGf = {Gf2[i*2], Gf2[i*2+1]};
			
			//du
			double[] complexJ2PIF = {0,2*Math.PI*this.getF()[i]};
			double[] complexMult = FourierTransform.multiplyComplex(complexGf, complexJ2PIF);
			du[i*2] = complexMult[0] * this.getGaussian()[i];
			du[i*2+1] = complexMult[1] * this.getGaussian()[i];
			
			//uL (for perimeter estimation)
			uL[i*2] = complexMult[0];
			uL[i*2+1] = complexMult[1];
			
			//ddu
			double[] complexMinus2PIFSquared = {-1.0*Math.pow(2*Math.PI*this.getF()[i], 2), 0};
			double[] complexMult2 = FourierTransform.multiplyComplex(complexGf, complexMinus2PIFSquared);
			ddu[i*2] = complexMult2[0]  * this.getGaussian()[i];
			ddu[i*2+1] = complexMult2[1] * this.getGaussian()[i];
			
			rTemp[i*2] = complexGf[0] * this.getGaussian()[i];
			rTemp[i*2+1] = complexGf[1] * this.getGaussian()[i];
		}

		double[] unshiftedDu = Fftshift.fftshift(du);
		double[] unshiftedDdu = Fftshift.fftshift(ddu);
		double[] unshiftedRtemp = Fftshift.fftshift(rTemp);
		
		double[] duFinal = FourierTransform.inverseDft1d(unshiftedDu);
		double[] dduFinal = FourierTransform.inverseDft1d(unshiftedDdu);
		double[] rTempFinal = FourierTransform.inverseDft1d(unshiftedRtemp);
		
		for(int j = 0; j < N; j++) {
			this.dU[j] = duFinal[j*2];
			this.ddU[j] = dduFinal[j*2];
			this.ru[j] = rTempFinal[j*2];
		}
		
	}
	
	private Complex[] double2complex(double[] v) {
		Complex[] complex = new Complex[v.length];
		
		for(int i = 0; i < v.length; i++) {
			complex[i] = new Complex(v[i], 0);
		}
		
		return complex;
	}
	
	private void calculateDiffs2(){		
		
		Complex[] Gs = FFT1D.fft(this.double2complex(this.get_u()));
		
		//double[] Gs = FourierTransform.dft1D(this.get_u());

		Complex[] Gf2 = Fftshift.fftshift(Gs);

		Complex[] du = new Complex[this.getN()];
		Complex[] ddu = new Complex[this.getN()];
		Complex[] uL = new Complex[this.getN()];
		Complex[] rTemp = new Complex[this.getN()];
		
		for(int i = 0; i < this.getN(); i++) {	
			
			Complex complexGf = Gf2[i];
			
			//du
			Complex complexJ2PIF = new Complex(0,2*Math.PI*this.getF()[i]);
			Complex complexMult = complexGf.multiply(complexJ2PIF);
			du[i] = complexMult.multiply(this.getGaussian()[i]);
			
			//uL (for perimeter estimation)
			uL[i] = complexMult;
			
			//ddu
			Complex complexMinus2PIFSquared = new Complex(-1.0*Math.pow(2*Math.PI*this.getF()[i], 2), 0);
			Complex complexMult2 = complexGf.multiply(complexMinus2PIFSquared);
			ddu[i] = complexMult2.multiply(this.getGaussian()[i]);
			
			rTemp[i] = complexGf.multiply(this.getGaussian()[i]);
		}

		Complex[] unshiftedDu = Fftshift.fftshift(du);
		Complex[] unshiftedDdu = Fftshift.fftshift(ddu);
		Complex[] unshiftedRtemp = Fftshift.fftshift(rTemp);
		
		Complex[] duFinal = FFT1D.ifft(unshiftedDu);
		Complex[] dduFinal = FFT1D.ifft(unshiftedDdu);
		Complex[] rTempFinal = FFT1D.ifft(unshiftedRtemp);
		
		for(int j = 0; j < N; j++) {
			this.dU[j] = duFinal[j].real;
			this.ddU[j] = dduFinal[j].real;
			this.ru[j] = rTempFinal[j].real;
		}
		
	}
		
	public static void main(String[] args) {
		final XYSeries signal = new XYSeries( "gt''" );
		
		int N = 256;
		
		double[] gt2linha =  new double[N];
		
		double T = 2*Math.PI/(N-1); 
		
		double t = 0.0;
		for(int i = 0; i < N; i++, t+=T) {
			gt2linha[i] = Math.cos(2*t)+2*t*Math.sin(t*t);
		}
		
		double a = 100.0;
		FourierBasedCurvatureEstimation f_estimation = new FourierBasedCurvatureEstimation(gt2linha, 1.0/a);
		
		t = 0.0;
		for(int i = 0; i < N; i++, t+=T) {
			signal.add(t, f_estimation.getdU()[i]);
		}
		
		final XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries( signal );
		
		LinePlot.linePlotDataset("Ploter", "Fourier Based Curvature Estimation", "t", "dg/dt", dataset);
		
	}
}
