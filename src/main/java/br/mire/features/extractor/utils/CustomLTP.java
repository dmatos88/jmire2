package br.mire.features.extractor.utils;

import org.openimaj.image.feature.dense.binarypattern.LocalTernaryPattern;

public class CustomLTP extends LocalTernaryPattern {

	public CustomLTP(float radius, int samples, float threshold) {
		super(radius, samples, threshold);
	}
	
	public float[][] getFloatNegativePattern(){
		int height = super.getNegativePattern().length;
		int width = super.getNegativePattern()[0].length;
		float[][] floatNegativePattern = new float[height][width];
		for(int i = 0; i < height; i++) {
			for(int j = 0; j < width; j++) {
				floatNegativePattern[i][j] = (float) super.getNegativePattern()[i][j];
			}
		}
		return floatNegativePattern;
	}
	
	public float[][] getFloatPositivePattern(){
		int height = super.getPositivePattern().length;
		int width = super.getPositivePattern()[0].length;
		float[][] floatPositivePattern = new float[height][width];
		for(int i = 0; i < height; i++) {
			for(int j = 0; j < width; j++) {
				floatPositivePattern[i][j] = (float) super.getPositivePattern()[i][j];
			}
		}
		return floatPositivePattern;
	}

}
