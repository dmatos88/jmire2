package br.mire.features.extractor.utils.geodesic;

import java.util.LinkedList;
import java.util.List;

import org.openimaj.image.FImage;
import org.openimaj.image.processing.threshold.OtsuThreshold;

import br.mire.features.extractor.utils.geodesic.Dijkstra.Node;
import br.mire.segmentation.contour.ComplexSignalContour2;
import br.mire.utils.math.Coord2D;

public class GeodesicDistance {

	public static Coord2D getNearestPointFromCentroid(ComplexSignalContour2 contourSignal, Coord2D centroid) throws Exception {
		
		Coord2D nearest = null;
		
		double min_dist = Double.MAX_VALUE;
		double dist = 0.0;
		for(int i = 0; i < contourSignal.getXSignal().length; i++) {			
			dist = centroid.getEuclideanDistance(new Coord2D(contourSignal.getXSignal()[i], contourSignal.getYSignal()[i]));
			if(dist < min_dist) {
				min_dist = dist;
				nearest = new Coord2D(contourSignal.getXSignal()[i], contourSignal.getYSignal()[i]);
				//System.out.println("Nearest: "+nearest.toString());
			}
		}
		return nearest;
	}

	/**
	 * 
	 * @param b_star_image normalized and smoothered
	 * @param maskA
	 * @param maskB
	 * @param pointA
	 * @param pointB
	 * @param sigma
	 * @return
	 * @throws Exception
	 */
	public static List<Node> getGeodesicPath(
			FImage b_star_image, 
			ComplexSignalContour2 contourA, 
			ComplexSignalContour2 contourB,
			final Coord2D pointA,
			final Coord2D pointB,
			float sigma) throws Exception {
		
		System.out.println("Executing Dijkstra for points:"+pointA.toString()+" and "+pointB.toString());


		//Coord2D pointA = LocalThresholdingController.getInstance().getMaskCentroidFromClick(i-1);
		//Coord2D pointB = LocalThresholdingController.getInstance().getMaskCentroidFromClick(i);
		
		//JMIRe.getInstance().displayMessage("Center A: "+pointA.toString());
		//JMIRe.getInstance().displayMessage("Center B: "+pointB.toString());
		
		int center_x = (int) (Math.min(pointA.getX(), pointB.getX()) + Math.abs( pointA.getX() - pointB.getX()));
		int center_y = (int) (Math.min(pointA.getY(), pointB.getY()) + Math.abs( pointA.getY() - pointB.getY()));

		int dist = (int) Math.floor(pointA.getEuclideanDistance(pointB) * 4);
		
		if(dist == 0) return new LinkedList<Node>();

		Coord2D leftUpperCorner = new Coord2D(center_x - (dist/2), center_y - (dist/2));
		
		FImage cropped = b_star_image.extractCenter(center_x, center_y, dist, dist);
		
		float otsu_thresh = OtsuThreshold.calculateThreshold(cropped, 100);
		//JMIRe.getInstance().displayMessage("Otus's value: "+otsu_thresh);
		FImage otsu_mask = cropped.clone();
		otsu_mask.threshold(otsu_thresh*1.05f);
		
		//DisplayUtilities.display(otsu_mask).setTitle("Otsu's on cropped");
		
		for(int x = 0; x < otsu_mask.width; x++) {
			for(int y = 0; y < otsu_mask.height; y++) {
				if(otsu_mask.getPixel(x,y)>0) cropped.setPixel(x,y, 1.0f);
			}
		}
		
		//DisplayUtilities.display(cropped).setTitle("Cropped from b_star after Otsu");
		Coord2D pA = new Coord2D(pointA.getX() - leftUpperCorner.getX(), pointA.getY() - leftUpperCorner.getY());
		Coord2D pB = new Coord2D(pointB.getX() - leftUpperCorner.getX(), pointB.getY() - leftUpperCorner.getY());
		
		//System.out.println("Executing Dijkstra for shifted points:"+pA.toString()+" and "+pB.toString());

		if(otsu_mask.getPixel((int)pA.getX(),(int)pA.getY()) != 0) {
			pA = getNearestPointFromCentroid(contourA, pointA);
			//System.out.println("Nearest point from centroid pA:"+pA.toString());
			pA.setX(pA.getX() - leftUpperCorner.getX());
			pA.setY(pA.getY() - leftUpperCorner.getY());
		}
		if(otsu_mask.getPixel((int)pB.getX(),(int)pB.getY()) != 0) {
			pB = getNearestPointFromCentroid(contourB, pointB);
			//System.out.println("Nearest point from centroid pB:"+pB.toString());
			pB.setX(pB.getX() - leftUpperCorner.getX());
			pB.setY(pB.getY() - leftUpperCorner.getY());
		}
		//System.out.println("Executing Dijkstra for shifted points:"+pA.toString()+" and "+pB.toString());
		
		Dijkstra dijkstra = new Dijkstra();
		List<Node> minPath = dijkstra.execute(pA, pB, cropped, 1.0f);
		
		List<Node> realPath = new LinkedList<Node>();
		
		for(Node px : minPath) {
			Node realPx = px;
			int x = (int) (leftUpperCorner.getX() + px.coord.getX());
			int y = (int) (leftUpperCorner.getY() + px.coord.getY());
			realPx.coord.setX(x);
			realPx.coord.setY(y);
			realPath.add(realPx);
		}
		
		return realPath;
	}
	
}
