package br.mire.features.extractor.utils.geodesic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import br.mire.segmentation.controllers.LocalThresholdingController;

public class GeodesicDistancesHistogram {

	private double max;
	private double min;
	private double interval;
	private double[] histogram;
	private double[] distances;
	private int nbins;
	
	public GeodesicDistancesHistogram(int nbins) {
		this.nbins = nbins;
		this.histogram = new double[nbins+1];
	}
	
	public double getProbability(double value) {
		int index = (int)(Math.floor((value-min)/(interval)));
		if(index < 0 || index >= histogram.length) return 0.0;
		return histogram[index] / distances.length;
	}
	
	public void makeHistogram(String filepath) throws IOException {
		
		this.readDistances(filepath);
		
		max = Double.MIN_VALUE;
		min = Double.MAX_VALUE;
		
		for(double d : distances) {
			max = Double.max(max, d);
			min = Double.min(min, d);
			//System.out.println(d);
		}

		//System.out.println("max: "+max);
		//System.out.println("min: "+min);
		
		interval = (max - min) / nbins;
		
		//System.out.println("interval:" +interval);
		
		for(double d : distances) {
			//System.out.println("d: "+d);
			//System.out.println("bin: "+(int)(Math.floor((d-min)/(interval))));
			histogram[(int)(Math.floor((d-min)/(interval)))]+=1.0;
		}
	
		/*
		for(int i = 0; i < histogram.length; i++) {
			System.out.println(((i+1)*interval+min)+": "+histogram[i]);
		}
		*/
	}
	
	public boolean readDistances(String filepath) throws IOException {

		int objectCounter = -1;
		BufferedReader br = new BufferedReader(new FileReader(filepath));
		String line;
		while ((line = br.readLine()) != null) {
			objectCounter++;
		}
		br.close();

		if (objectCounter > 0) {
			distances = new double[objectCounter];
			objectCounter = 0;
			br = new BufferedReader(new FileReader(filepath));
			br.readLine(); //consuming header
			while ((line = br.readLine()) != null) {
				String[] distancesStr = line.split(LocalThresholdingController.csv_separator);
				//i == 0 => class identifier
				for (int i = 1; i < distancesStr.length; i++) {
					distances[objectCounter] = Double.valueOf(distancesStr[i]);
				}
				objectCounter++;
			}
			br.close();

			return true;
		}

		return false;
	}
	
	public static void main(String[] args) throws IOException {
		GeodesicDistancesHistogram gd = new GeodesicDistancesHistogram(10);
		gd.makeHistogram("/home/dmatos/dest2/geodesic_distances.csv");
		
		double distance = 4.89;
		
		System.out.println("Probabilitie of "+distance+": "+gd.getProbability(distance));
		
	}
}
