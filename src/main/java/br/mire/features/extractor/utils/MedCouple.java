package br.mire.features.extractor.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.ArrayUtils;

/**
 * Based on C++ code for medcouple of Jordi Gutiérrez Hermoso <jordigh@octave.org>
 * found at http://inversethought.com/hg/medcouple/file/default/jmedcouple.c%2B%2B
 * @author dmatos
 *
 */

public class MedCouple{

	private static long sum(Long[] vector) {
		long sum = 0;
		for(int i = 0; i < vector.length; i++) {
			sum += vector[i];
		}
		return sum;
	}
		

	/*
	  This computes the weighted median of array A with corresponding
	  weights W.
	*/
	private static double wmedian(List<Double> A,
	               Long[] W){
		
	  Map<Double, Long> AW = new TreeMap<Double, Long>();

	  long n = A.size();

	  for (int i = 0; i < n; i ++)
	    AW.put(A.get(i), W[i]);

	  long wtot = sum(W);

	  long beg = 0;
	  long end = n;
	  
	  Long lastMid = -1L;
	  
	  while (true) {
				  
	    Long mid = (beg + end)/2;
	    
	    if(mid >= AW.size())
	    	mid = new Long(AW.size()-1);
	    	    
	    Double trial = (Double) AW.keySet().toArray()[mid.intValue()];
	   	
	    //end looping if it's stuck in a position
	    if(mid == lastMid)
	    	return trial;
	    lastMid = mid;
	    
//	    System.out.println("flag up trial "+AW.keySet());
//	    System.out.println("flag up midvalue "+mid);
	    
	    long wleft = 0, wright = 0;

	    for (Double aw : AW.keySet()) {
	      double a = aw;
	      long w = AW.get(aw);
	      //tie(a, w) = move(aw);
	      if (a > trial)
	        wleft += w;
	      else
	        // This also includes a == trial, i.e. the "middle" weight.
	        wright += w;
	    }

	    if (2*wleft > wtot)
	      end = mid;
	    else if (2*wright < wtot)
	      beg = mid;
	    else
	      return trial;
	    
	    
	    
	  }
	}
	
	/*
    Kernel function h for the medcouple, closing over the values of
    Zplus and Zminus just defined above.

    In case a and be are within epsilon of the median, the kernel
    is the signum of their position.
  */
	private static double h_kern(
			Object[] Zplus,
			Object[] Zminus,
			Long i, Long j,
			Double eps2,
			int n_plus) {
	    double a = (Double) Zplus[i.intValue()];
	    double b = (Double) Zminus[j.intValue()];
	    
//	    System.out.println("flag up a: "+a);
//	    System.out.println("flag up b: "+b);

	    double h;
	    if (Math.abs(a - b) <= 2*eps2)
	      h = Math.signum(n_plus - 1 - i - j);
	    else
	      h = (a + b)/(a - b);
//	    System.out.println("flag up h: "+h);
	    return h;
	  };

	public static double medcouple(double[] X)
	{
	  	
	  Double eps1 = Math.ulp(1.0);
	  Double eps2 = Double.MIN_VALUE;
		
	  Long n = new Long(X.length);
	  Long n2 = (n - 1)/2;

	  if (n < 3)
	    return 0;

	  Double[] Z = ArrayUtils.toObject(X);
	  java.util.Arrays.sort(Z,Collections.reverseOrder()); 

	  double Zmed;
	  if (n % 2 == 1)
	    Zmed = Z[n2.intValue()];
	  else
	    Zmed = (Z[n2.intValue()] + Z[n2.intValue()+ 1])/2.0;

	  // Check if the median is at the edges up to relative epsilon
	  if (Math.abs(Z[0] - Zmed) < eps1*(eps1 + Math.abs(Zmed)))
	    return -1.0;
	  if (Math.abs(Z[n.intValue() - 1] - Zmed) < eps1*(eps1 + Math.abs(Zmed)))
	    return 1.0;

	  // Centre Z wrt median, so that median(Z) = 0.
	  for(int i = 0; i < Z.length; i++) {
		  Z[i] -= Zmed;
	  }

	  // Scale inside [-0.5, 0.5], for greater numerical stability.
	  double Zden = 2*Math.max(Z[0], -Z[n.intValue() - 1]);
	  for(int i = 0; i < Z.length; i++) {
		  Z[i] /= Zden;
	  }
	  Zmed /= Zden;

	  double Zeps = eps1*(eps1 + Math.abs(Zmed));

	  // These overlap on the entries that are tied with the median
	  List<Double> Zplus = new LinkedList<Double>();
	  List<Double> Zminus = new LinkedList<Double>();
	  
	  for(int i = 0; i < Z.length; i++) {
		  if(Z[i] >= -Zeps) {
			  Zplus.add(Z[i]);
		  }
		  if(Zeps >= Z[i]) {
			  Zminus.add(Z[i]);
		  }
	  }

	  Long n_plus = new Long(Zplus.size());
	  Long n_minus = new Long(Zminus.size());

	  // Init left and right borders
	  List<Long> L = new ArrayList<Long>(n_plus.intValue());
	  for(int i = 0; i < n_plus.intValue(); i++) {
		  L.add(0L);
	  }	  
	  List<Long> R = new ArrayList<Long>(n_plus.intValue());
	  for(int i = 0; i < n_plus.intValue(); i++) {
		  R.add(n_minus - 1L);
	  }
	  
	  Long Ltot = 0L;
	  Long Rtot = n_minus*n_plus;
	  Long medc_idx = Rtot/2;
	  
	  // kth pair algorithm (Johnson & Mizoguchi)
	  while (Rtot - Ltot > n_plus) {

	    // First, compute the median inside the given bounds
	    List<Double> A = new LinkedList<Double>();
	    List<Long> W = new LinkedList<Long>();
	    for (int i = 0; i < n_plus; i++) {
	      if (L.get(i) <= R.get(i)) {
	        A.add(
	        		h_kern(
	        				Zplus.toArray(),
	        				Zminus.toArray(),
			        		new Long(i), 
			        		new Long((L.get(i) + R.get(i))/2),
			        		eps2,
			        		n_plus.intValue()
			        	)
	        		);
	        
	        W.add(R.get(i) - L.get(i) + 1);
	      }
	    }
	    
	    Long[] arrayW = new Long[W.size()];
	    for(int i = 0; i < W.size(); i++) arrayW[i] = W.get(i);
	    
	    double Am = wmedian(A, arrayW);
	    
//	    System.out.println("flag up "+Am);
	    
	    double Am_eps = eps1*(eps1 + Math.abs(Am));

	    // Compute new left and right boundaries, based on the weighted
	    // median
	    Long[] P = new Long[n_plus.intValue()];
	    Long[] Q = new Long[n_plus.intValue()];
	    
	    
	    {
	      long j = 0;
	      for (int i = n_plus.intValue() - 1; i >= 0; i--) {
	        while (j < n_minus && 
	        		h_kern(
	        				Zplus.toArray(),
	        				Zminus.toArray(),
			        		new Long(i), 
			        		new Long(j),
			        		eps2,
			        		n_plus.intValue()
	        				
	        			) - Am > Am_eps)
	          j++;
	        P[i] =  j - 1;
	      }
	    }

	    {
	      long j = n_minus - 1;
	      for (int i = 0; i < n_plus; i++) {
	        while (j >= 0 && h_kern(
    				Zplus.toArray(),
    				Zminus.toArray(),
	        		new Long(i), 
	        		new Long(j),
	        		eps2,
	        		n_plus.intValue()
    				
    			) - Am < -Am_eps)
	          j--;
	        Q[i] = j + 1;
	      }
	    }

	    long 
	      sumP = sum(P) + n_plus,
	      sumQ = sum(Q);

	    if (medc_idx <= sumP - 1) {
	      R = new ArrayList<Long>(P.length);
	      for(int i = 0; i < P.length; i++) R.add(P[i]);
	      Rtot = sumP;
	    }
	    else {
	      if (medc_idx > sumQ - 1) {
	        L = new ArrayList<Long>(Q.length);
	        for(int i = 0; i < Q.length; i++) L.add(Q[i]);
	        Ltot = sumQ;
	      }
	      else
	        return Am;
	    }
	  }

	  // Didn't find the median, but now we have a very small search space
	  // to find it in, just between the left and right boundaries. This
	  // space is of size Rtot - Ltot which is <= n_plus
	  Double[] A = new Double[n_plus.intValue()];	  
	  int A_Index = 0;
	  for (int i = 0; i < n_plus; i++) {		
	    for (long j = L.get(i); j <= R.get(i); j++)
	      A[A_Index++] = (h_kern(
  				Zplus.toArray(),
  				Zminus.toArray(),
	        		new Long(i), 
	        		new Long(j),
	        		eps2,
	        		n_plus.intValue()
  				
  			));
	  }	   
	  for(int i = 0; i < A.length; i++) if(A[i] == null) A[i] = 0.0;
//	  System.out.println("flag up length of A "+A.length);
//	  System.out.println("a(0) "+A[0]);
//	  System.out.println("a(1) "+A[1]);
	  java.util.Arrays.sort(A,Collections.reverseOrder());
	  double Am = 0.0;
	  //even case
	  if(A.length%2 == 0) Am = A[medc_idx.intValue() - Ltot.intValue() + 1];
	  //odd case
	  else Am = A[medc_idx.intValue() - Ltot.intValue()];
	  if(Am > 0.0) return Am;
	  return -Am;
	}
	
	public static void main(String[] args) {
		double[] t1 = {50.0, 2.1, 1.0, 0};
		double[] t2 = {10.0, 5.3, 6.0, 0, 3.3, 4.4};
		double[] t3 = {5.0, 30.0, 3.0, 4.0, 32.0};
		double[] t4 = {32, 4, 3, 5, 0};
		
		double[] tRand = new double[1000];
		tRand[0] = Math.random();
		System.out.print("tRand = ["+tRand[0]);
		for(int i = 1; i < 1000; i++) {
			tRand[i] = Math.random();
			System.out.print(","+tRand[i]);
		}
		System.out.println("]");
		
		//Tested against MATLAB LIBRA mc implementation
		System.out.println("t1: "+MedCouple.medcouple(t1)); //0.4690
		System.out.println("t2: "+MedCouple.medcouple(t2)); //1.0324e-15
		System.out.println("t3: "+MedCouple.medcouple(t3)); //0.8621
		System.out.println("t4: "+MedCouple.medcouple(t4)); //0
		System.out.println("tRand: "+MedCouple.medcouple(tRand)); //OK
	}
	
}
