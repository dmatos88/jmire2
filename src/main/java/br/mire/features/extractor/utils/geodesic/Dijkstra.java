package br.mire.features.extractor.utils.geodesic;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

import javax.swing.JFrame;

import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.processing.convolution.FGaussianConvolve;
import org.openimaj.image.processing.threshold.OtsuThreshold;

import br.mire.segmentation.application.JMIRe;
import br.mire.utils.image.RGB2Lab;
import br.mire.utils.math.Coord2D;

public class Dijkstra {

	public class Node implements Comparable<Node>{
		boolean closed = false;
		public Coord2D coord = null;
		Coord2D label = null;
		double dist = Double.MAX_VALUE; 
		//it might be 1 or sqrt(2)
		public double dist_from_label = 0;
		
		public Node(Coord2D coord) {
			this.coord = coord;
		}

		@Override
		public int compareTo(Node o) {
			if(this.coord.getX() == o.coord.getX() && this.coord.getY() == o.coord.getY()) return 0;
			if(this.dist == o.dist) return -1;
			return (int) (100 * (this.dist - o.dist));
		}
	}

	/**
	 * 
	 * @param pointA a point within object area with value lesser than 1.0
	 * @param pointB a point within object area with value lesser than 1.0
	 * @param image
	 * @return
	 */
	public List<Node> execute(Coord2D pointA, Coord2D pointB, FImage image, float threshold){

		//DisplayUtilities.display(image).setTitle("To Djikstra");
		
		int width = image.width;
		int height = image.height;

		Node[][] graph = new Node[width][height];
		
		Queue<Node> q = new PriorityQueue<>();
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				graph[x][y] = new Node(new Coord2D(x, y));
				q.add(graph[x][y]);
			}
		}

		graph[(int)pointA.getX()][(int)pointA.getY()].label = null;
		graph[(int)pointA.getX()][(int)pointA.getY()].dist = 0;
		graph[(int)pointA.getX()][(int)pointA.getY()].coord = pointA;
		q.add(graph[(int)pointA.getX()][(int)pointA.getY()]);
		
		while(!q.isEmpty()) {
			
			Node r = q.poll();
			int ry = (int) r.coord.getY();
			int rx = (int) r.coord.getX();

			r = graph[rx][ry];
			
			if(r.closed) {
				continue;
			}
			
			//System.out.println("Closing ["+rx+","+ry+"] which pixel value is "+image.getPixel(rx,ry));
			graph[rx][ry].closed = true;
			
			for(int y = -1; y < 2; y++) {
				for(int x = -1; x < 2; x++) {
					int ny = (int) ry + y;
					int nx = (int) rx + x;
					if(ny >= 0 && ny < height && nx >= 0 && nx < width) {
						if(!graph[nx][ny].closed) {	
							double neighbor_value = image.getPixel(nx,ny);//image.getPixel(nx,ny)<threshold?image.getPixel(nx,ny)*image.getPixel(nx,ny):Math.pow(image.getPixel(nx,ny), 10);
							double r2n_dist = neighbor_value + Math.log(pointB.getEuclideanDistance(graph[nx][ny].coord));   //difference of pixel intensity 
							if(r2n_dist+r.dist < graph[nx][ny].dist && image.getPixel(nx,ny)<threshold) {
								//System.out.println("Updating ["+nx+","+ny+"] which pixel value is "+image.getPixel(nx,ny));
								graph[nx][ny].label = r.coord;
								graph[nx][ny].dist = r2n_dist + r.dist;
								if(Math.abs(x + y) == 1) graph[nx][ny].dist_from_label = 1;
								else graph[nx][ny].dist_from_label = Math.sqrt(2.0);
								q.add(graph[nx][ny]);
							}
						}
					}
				}			
			}
		}
		
		
		int goal_y = (int) pointB.getY();
		int goal_x = (int) pointB.getX();
		Node v = graph[goal_x][goal_y];

		List<Node> minimum_path = new LinkedList<>();
		
		//System.out.println("Starting at "+v.coord.getX()+","+v.coord.getY());
		//if(v.closed) System.out.println("v is closed");
		//if(v.rot == null) System.out.println("v.rot is null");
		
		//FImage path = new FImage(width, height);
		while(v.label != null) {
			//System.out.println("Adding "+v.rot.getX()+","+v.rot.getY());
			minimum_path.add(v);
			//path.setPixel((int)v.label.getX(), (int)v.label.getY(), 1.0f);
			v = graph[(int)v.label.getX()][(int)v.label.getY()];			
		}
		//DisplayUtilities.display(path).setTitle("PATH");;
		return minimum_path;
	}

	public static void main(String[] args) throws IOException {
		
		
		String filepath = "/home/dmatos/samples_fields/field[028]count[1].jpg";
		MBFImage image = ImageUtilities.readMBF(new File(filepath));

		int width = image.getWidth();
		int height = image.getHeight();

		float[][][] rgb = new float[3][width][height];

		for(int band = 0; band < 3; band++) {
			for(int x = 0; x < width; x++) {
				for(int y = 0; y < height; y++) {
					rgb[band][x][y] = image.getBand(band).getPixel(x,y);
				}
			}
		}

		float sigma = 1.5f;
		FGaussianConvolve gauss = new FGaussianConvolve(sigma);
		
		float[][][] matrixLab = RGB2Lab.convert(rgb);

		FImage b_star_image = new FImage(matrixLab[2]);
		b_star_image.normalise();

		gauss.processImage(b_star_image);
		
		
		Coord2D pointA = new Coord2D(1793,1252);
		Coord2D pointB = new Coord2D(1795,1305);

		Dijkstra d = new Dijkstra();
		int center_x = (int) (Math.min(pointA.getX(), pointB.getX()) + Math.abs( pointA.getX() - pointB.getX()));
		int center_y = (int) (Math.min(pointA.getY(), pointB.getY()) + Math.abs( pointA.getY() - pointB.getY()));
		
		int dist = (int) Math.floor(pointA.getEuclideanDistance(pointB) * 4);

		Coord2D leftUpperCorner = new Coord2D(center_x - (dist/2), center_y - (dist/2));
		
		FImage cropped = b_star_image.extractCenter(center_x, center_y, dist, dist);
		
		float otsu_thresh = OtsuThreshold.calculateThreshold(cropped, 100);
		JMIRe.getInstance().displayMessage("Otus's value: "+otsu_thresh);
		FImage mask = cropped.clone();
		mask.threshold(otsu_thresh*1.05f);
		
		DisplayUtilities.display(mask).setTitle("Otsu's on cropped");
		
		for(int x = 0; x < mask.width; x++) {
			for(int y = 0; y < mask.height; y++) {
				if(mask.getPixel(x,y)>0) cropped.setPixel(x,y, 1.0f);
			}
		}	
		

		DisplayUtilities.display(cropped);
		
		pointA.setX(pointA.getX() - leftUpperCorner.getX());
		pointA.setY(pointA.getY() - leftUpperCorner.getY());

		pointB.setX(pointB.getX() - leftUpperCorner.getX());
		pointB.setY(pointB.getY() - leftUpperCorner.getY());	
		
		List<Node> path = d.execute(pointA, pointB, cropped, 1.0f);
		
		for(Node v : path) {
			int x = (int) (leftUpperCorner.getX() + v.coord.getX());
			int y = (int) (leftUpperCorner.getY() + v.coord.getY());
			b_star_image.setPixel(x, y, 0f);
			System.out.println("["+v.coord.getX()+","+v.coord.getY()+"]");
		}
		
		DisplayUtilities.display(b_star_image).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		System.out.println("center: ["+center_x+","+center_y+"]");
		System.out.println("dist: "+dist);
	}
}
