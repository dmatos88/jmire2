package br.mire.features.extractor;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

import javax.imageio.ImageIO;

import de.lmu.ifi.dbs.jfeaturelib.features.Haralick;
import de.lmu.ifi.dbs.utilities.Arrays2;
import ij.process.ColorProcessor;

public class HaralickFeatures {
	
	public static Double[] extractFeatures(String filePath) throws IOException, URISyntaxException{
		 // load the image
        //InputStream stream = HaralickFeatures.class.getClassLoader().getResourceAsStream(filePath);
        
		// Read from an input stream
	    InputStream stream = new BufferedInputStream(new FileInputStream(filePath));
		ColorProcessor image = new ColorProcessor(ImageIO.read(stream));

        // initialize the descriptor
        Haralick descriptor = new Haralick();

        // run the descriptor and extract the features
        descriptor.run(image);

        // obtain the features
        List<double[]> features = descriptor.getFeatures();
        Double[] featuresExtracted = new Double[features.get(0).length];
        
        // print the features to system out
        for (double[] feature : features) {
            for(int i = 0; i < feature.length; i++) {
            	featuresExtracted[i] = feature[i];
            }
        }
        
        return featuresExtracted;
	}
	
	public static void main(String[] args) {
		try {
			System.out.println(Paths.get(".").toAbsolutePath().normalize().toString());
			Double[] features = HaralickFeatures.extractFeatures("samples/s001.jpeg");
			for(Double f : features) {
				System.out.print(f+" ,  ");
			}
			System.out.println("# of features: "+features.length);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
