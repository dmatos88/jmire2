package br.mire.features.extractor;

public class HistogramFeatures {
	
	public static int numberOfFeatures = 9;

	/**
	 * 
	 * @param histogram normalized with distance 1 between bins
	 * @return
	 */
	public static double getMean(double[] histogram) {
		double summedValue = 0.0;
		for(int i = 0; i < histogram.length; i++) {
			summedValue += histogram[i] * i;			
		}
		return summedValue/histogram.length;
	}
	
	/**
	 * 
	 * @param histogram normalized
	 * @return median value of a normalized histogram, -1 case the histogram has length 0
	 */
	public static double getMedian(double[] histogram) {
		
		double summedValue = 0.0;
		for(int i = 0; i < histogram.length; i++) {
			summedValue += histogram[i];
			if(summedValue >= 50.0) {
				return i;
			}
		}
		return -1;
	}
	
	public static double getVariance(double[] histogram, double mean) {

		double summedValue = 0.0;
		double summatory = 0.0;
		
		for(int i = 0; i < histogram.length; i++) {
			summedValue += histogram[i]  *  Math.pow((i - mean), 2);
			summatory += i;
		}
		
		return summedValue / summatory;
	}
	
	
	public static double getVariance(double[] histogram) {				
		return getVariance(histogram, getMean(histogram));
	}
	
	public static double getStdDeviation(double[] histogram, double mean) {
		return Math.sqrt(getVariance(histogram, mean));
	}
	
	public static double getStdDeviation(double[] histogram) {
		return Math.sqrt(getVariance(histogram));
	}
	
	public static double getSkewness(double[] histogram) {
		return br.mire.features.extractor.utils.MedCouple.medcouple(histogram);
	}
	
	public static double getKurtosis(double[] histogram) {
		return  new org.apache.commons.math3.stat.descriptive.moment.Kurtosis().evaluate(histogram);
	}
	
	public static double getEntropy(double[] histogram) {		
		return JavaMI.Entropy.calculateEntropy(histogram);
		
	}
	
	public static double getMax(double[] histogram) {
		double max = Double.MIN_VALUE;
		int maxIndex = -1;
		for(int i = 0; i < histogram.length; i++) {
			if(histogram[i]> max) {
				maxIndex = i;
				max = histogram[i];
			}
		}
		return maxIndex;
	}
	
	public static double getMin(double[] histogram) {
		double min = Double.MAX_VALUE;
		int minIndex = -1;
		for(int i = 0; i < histogram.length; i++) {
			if(histogram[i] < min) {
				minIndex = i;
				min = histogram[i];
			}
		}
		return minIndex;
	}
	
	public static Double[] extractFeatures(double[] histogram) {
		Double[] features = new Double[numberOfFeatures];
		int index = 0;
		features[index++] = getMean(histogram);
		double mean = features[0];
		features[index++] = getMedian(histogram);
		features[index++] = getVariance(histogram, mean);
		features[index++] = getEntropy(histogram);
		features[index++] = getKurtosis(histogram);
		features[index++] = getMax(histogram);
		features[index++] = getMin(histogram);
		features[index++] = getSkewness(histogram);
		features[index++] = getStdDeviation(histogram, mean);
		return features;
	}
	
	public static double histogramDistance(double[] histogram1, double[] histogram2) {
		//using nearest neighbor classification in the χ 2 histogram distance
		//for comparison
		double sum = 0.0;
		for(int i = 0; i < histogram1.length; i++) {
			double numerator = Math.pow(histogram1[i] - histogram2[i], 2);
			double denominator = histogram1[i] + histogram2[i];
			if(denominator > 0)
				sum += numerator / denominator;
		}
		return sum;
	}
	
}
