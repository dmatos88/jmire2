package br.mire.features.extractor;

import java.io.File;

import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;

import br.mire.features.extractor.utils.FourierBasedCurvatureEstimation;
import br.mire.segmentation.contour.ComplexSignalContour2;

public class Curvegram {

	//private String path;
	private FImage image;
	private ComplexSignalContour2 contourSignal;
	private int numOfPoints;
	private int numOfContours;
	private double[][] curvegramSurface;
	private double[] sigmas;

	public Curvegram(FImage img, double[] sigmas) throws Exception {
		//this.path = imgPath;
		this.image = img.clone();
		this.numOfContours = sigmas.length;
		this.sigmas = new double[this.numOfContours];
		for(int i = 0; i < numOfContours; i++) this.sigmas[i] = sigmas[i];
		this.initialize();
		this.calculateCurvegram();
	}
	
	public Curvegram(ComplexSignalContour2 contour, double[] sigmas) throws Exception {
		//this.path = imgPath;
		this.contourSignal = contour;
		this.numOfPoints = contourSignal.getXSignal().length;
		this.numOfContours = sigmas.length;
		this.sigmas = new double[this.numOfContours];
		for(int i = 0; i < numOfContours; i++) this.sigmas[i] = sigmas[i];
		this.calculateCurvegram();
	}
	
	public final double[][] getCurvegram(){
		return this.curvegramSurface;
	}
	
	public double[] get_x_coords() {
		return this.contourSignal.getXSignal();
	}
	
	public double[] get_y_coords() {
		return this.contourSignal.getYSignal();
	}

	private void initialize() throws Exception {
		this.contourSignal = new ComplexSignalContour2(image, 0, 0);
		this.numOfPoints = contourSignal.getXSignal().length;
	}

	private double perimeter(double[] contour) {
		double perimeter = 0.0;

		for(int i = 1; i < contour.length; i++) {
			perimeter += Math.abs(contour[i] - contour[i-1]);
		}

		return perimeter;
	}

	private void calculateCurvegram() {

		this.curvegramSurface = new double[numOfContours][numOfPoints];

		//double a = 1.0;

		double perimeterX = this.perimeter(this.contourSignal.getXSignal());
		double perimeterY = this.perimeter(this.contourSignal.getYSignal());

		for(int i = 0; i < this.numOfContours; i++ ) { //, a+=1.0) {
			FourierBasedCurvatureEstimation estimate_y = new FourierBasedCurvatureEstimation(contourSignal.getXSignal(), sigmas[i]);
			FourierBasedCurvatureEstimation estimate_x = new FourierBasedCurvatureEstimation(contourSignal.getYSignal(), sigmas[i]);		

			double rXperimeter = this.perimeter(estimate_x.getRu());
			double rYperimeter = this.perimeter(estimate_y.getRu());

			double Cx = perimeterX / rXperimeter;
			double Cy = perimeterY / rYperimeter;

			//System.out.println(Cx+" -- "+Cy);

			for(int j = 0; j < this.numOfPoints; j++) {

				double dx = estimate_x.getdU()[j]  * Cx;
				double ddx = estimate_x.getddU()[j]  * Cx;
				double dy = estimate_y.getdU()[j]  * Cy;
				double ddy = estimate_y.getddU()[j] * Cy;

				//numerator = -imag(dx*dy+ddx*ddy-j(dx*ddy-ddx*dy))
				double numerator = -1 * (0 - (dx*ddy-ddx*dy));				
				//denominator = (dx^2 + dy^2)^1.5
				double denominator = Math.pow(dx*dx+dy*dy, 1.5);

				this.curvegramSurface[i][j] = (numerator/denominator);
			}

		}
	}

	public static void main(String[] args) throws Exception {
		//test with an image of a circle
		String filepath = "/home/dmatos/workspace2/VisionUSP/silhueta_borboleta_640x640.jpg";
		
		FImage img = ImageUtilities.readF(new File(filepath));
		
		double[] sigmas = {100, 10};
		
		Curvegram curvegram = new Curvegram(img, sigmas);

		System.out.println("k values");
		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;
		for(int i = 0; i < curvegram.getCurvegram()[0].length; i++) {
			System.out.println(curvegram.getCurvegram()[0][i]);
			if(curvegram.getCurvegram()[0][i] > max) max = curvegram.getCurvegram()[0][i];
			if(curvegram.getCurvegram()[0][i] < min) min = curvegram.getCurvegram()[0][i];
		}
		System.out.println("max "+max);
		System.out.println("min "+min);

	}

}
