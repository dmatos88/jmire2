package br.mire.features.extractor;

import java.util.List;

import org.apache.commons.vfs2.FileSystemException;
import org.openimaj.data.dataset.VFSListDataset;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.connectedcomponent.ConnectedComponentLabeler;
import org.openimaj.image.pixel.ConnectedComponent;
import org.openimaj.image.pixel.ConnectedComponent.ConnectMode;
import org.openimaj.image.pixel.Pixel;
import org.openimaj.image.processing.algorithm.EqualisationProcessor;
import org.openimaj.image.processing.face.feature.ltp.GaussianWeighting;
import org.openimaj.image.processing.face.feature.ltp.LTPWeighting;
import org.openimaj.image.processing.face.feature.ltp.LtpDtFeature;
import org.openimaj.image.processing.threshold.OtsuThreshold;

public class LTPFeatures {


	protected static double[] getRelativeHistogram(List<List<Pixel>> ltpPixels, double sampleArea) 
			throws NullPointerException{

		double[] ltpHistogram = new double[ltpPixels.size()];
		int index = 0;
		double summatory = 0.0;

		for(List<Pixel> pxList : ltpPixels) {
			if(pxList == null) continue;
			ltpHistogram[index] = (double)pxList.size();///(2*sampleArea);
			summatory += ltpHistogram[index];
			index++;
		}
		
		//normalize data
		for(int i = 0; i < index; i++) {
			ltpHistogram[i] /= summatory;
		}

		return ltpHistogram;
	}

	protected static double[] extractHistogram(FImage sample, FImage mask, int objectArea) throws FileSystemException{

		LTPWeighting weighting = new GaussianWeighting(1f);

		LtpDtFeature ltp = new LtpDtFeature(sample, mask, weighting);

//		System.out.println("# of codes: "+ltp.ltpPixels.size());

		double[] histogram = getRelativeHistogram(ltp.ltpPixels,objectArea);

		return histogram;
	}
	
	public static Double[] extractFeatures(FImage sample, FImage mask, int objectArea) throws FileSystemException {
		double[] histogram = extractHistogram(sample, mask, objectArea);
		return HistogramFeatures.extractFeatures(histogram);
	}

	public void test03() throws FileSystemException {
		String srcdir = "/home/dmatos/samples/";

		VFSListDataset<FImage> images = 
				new VFSListDataset<FImage>(srcdir, ImageUtilities.FIMAGE_READER);

		//		System.out.println("# of Samples: "+images.size());

		FImage sample = images.get(1);
		FImage sample2 = images.get(0);

		EqualisationProcessor histEq = new EqualisationProcessor();
		histEq.processImage(sample);
		histEq.processImage(sample2);

		DisplayUtilities.display(sample);

		int numBins = 59; 

		float threshold1 = OtsuThreshold.calculateThreshold(sample, numBins);
		float adjustment_factor = 1f;

		ConnectedComponentLabeler cclabeler1 = new ConnectedComponentLabeler(
				ConnectedComponentLabeler.Algorithm.TWO_PASS,
				threshold1*adjustment_factor,
				ConnectMode.CONNECT_8);
		List<ConnectedComponent> ccList1 = cclabeler1.findComponents(sample);

		FImage mask1 = new FImage(sample.width, sample.height);
		int max = 0;
		ConnectedComponent biggerCc = null;
		for(ConnectedComponent cc : ccList1) {
			if(cc.getPixels().size() > max) {
				max = cc.getPixels().size();
				biggerCc = cc;
			}
		}

		mask1.fill(0f);
		for(Pixel px : biggerCc.getPixels()) {
			mask1.setPixel(px.x, px.y, 1f);
		}

		DisplayUtilities.display(mask1);

		double[] histogram1 = extractHistogram(sample, mask1, biggerCc.pixels.size());//sample.height*sample.width);


		float threshold = OtsuThreshold.calculateThreshold(sample2, numBins);
		System.out.println("targe threshold: "+threshold);
		adjustment_factor = 1f;
		ConnectedComponentLabeler cclabeler = new ConnectedComponentLabeler(
				ConnectedComponentLabeler.Algorithm.TWO_PASS,
				threshold*adjustment_factor,
				ConnectMode.CONNECT_8);
		cclabeler.findComponents(sample2);			

		List<ConnectedComponent> ccList = cclabeler.findComponents(sample2);

		FImage mask = new FImage(sample2.width, sample2.height);

		System.out.println("# of cc found: "+ccList.size());

		for(ConnectedComponent cc : ccList) {

			mask.fill(0f);

			System.out.println("# of pxs "+cc.getPixels().size());			

			for(Pixel px : cc.getPixels()) {
				mask.setPixel(px.x, px.y, 1.0f);
			}

			try {
				double[] histogram2 = extractHistogram(sample2, mask, cc.getPixels().size());//sample2.height*sample2.width);

				double distance = HistogramFeatures.histogramDistance(histogram1, histogram2);

				System.out.println("x2 distance: "+distance);

				//				DisplayUtilities.display(mask);

				if(distance < 0.2) {
					System.out.println("x2 distance: "+distance);
					System.out.println("histogram size: "+histogram1.length);
					DisplayUtilities.display(mask);
				}
			} catch(NullPointerException e) {
				e.printStackTrace();
			}
		}

		System.out.println("fim de teste");

	}

	public void test04SamplesSize() throws FileSystemException{
		String srcdir = "/home/dmatos/workspace2/JMIRe/samples/";

		VFSListDataset<FImage> images = 
				new VFSListDataset<FImage>(srcdir, ImageUtilities.FIMAGE_READER);

		int maxH = 0;
		int maxW = 0;
		FImage maxHname = null;
		FImage maxWname = null;
		
		for(FImage image : images) {
			if(image.width > maxW) {
				maxW = image.width;
				maxWname = image.clone();
			}
				
			if(image.height > maxH) {
				maxH = image.height;
				maxHname = image.clone();
			}
		}
		
		System.out.println("Max width: "+maxW);
		DisplayUtilities.display(maxWname);
		System.out.println("max height: "+ maxH);
		DisplayUtilities.display(maxHname);
		
	}

	public static void main(String[] args) throws FileSystemException {
		LTPFeatures l = new LTPFeatures();

		l.test03();
	}
}
