package br.mire.segmentation.controllers;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.DisplayUtilities.ImageComponent;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.processing.convolution.FGaussianConvolve;

import br.mire.features.extraction.GeometricFeatures;
import br.mire.features.extraction.CCMFeatures.CCMFeaturesStrategy;
import br.mire.features.extraction.CurvatureFeatures.CurvatureFeaturesStrategy;
import br.mire.features.extraction.GeometricFeatures.GeometricFeaturesStrategy;
import br.mire.ml.classifier.generic.JMIReClassifier;
import br.mire.ml.classifier.tcruzi.TCruzi_Tripomastigote_Giemsa;
import br.mire.segmentation.application.JMIRe;
import br.mire.segmentation.application.JMIRe.Applications;
import br.mire.segmentation.contour.ComplexSignalContour2;
import br.mire.segmentation.edges.LoGEdgeDetection;
import br.mire.segmentation.slic.SLIC;
import br.mire.segmentation.view.apps.JMIReApp;
import br.mire.utils.math.Coord2D;

public class SLICController extends JMIReController
implements MouseListener{

	private static final long serialVersionUID = 1L;
	private static SLICController appController = null;
	private String destDir;
	private String sourceDir;
	private int S;
	private double m;
	private double epson;
	private int maxIter;
	private MBFImage sample;
	private FImage mask;
	private SLIC slic;
	private int numOfCurvatures;
	private int numOfFeatures;
	private final String csv_separator = ",";
	private final String configFilePath = "jmire_slic.properties";
	private String mostRecentFile;
	private boolean initialized = false;

	private static JFrame imageDisplayer = null;

	private JMIReClassifier featuresExtractor;

	private SLICController() {
		appController = this;
		this.slic = new SLIC();
		this.epson = 0.00000001;
		this.m = 15.0;
		this.maxIter = 10;
		GeometricFeaturesStrategy[] geomStrategies = {
				GeometricFeaturesStrategy.MINOR_AXIS,
				GeometricFeaturesStrategy.MAJOR_AXIS,
				GeometricFeaturesStrategy.AREA,
				GeometricFeaturesStrategy.PARIMETER,
				GeometricFeaturesStrategy.CIRCULARITY,
				GeometricFeaturesStrategy.THICKNESS_RATIO,
				GeometricFeaturesStrategy.ASPECT_RATIO,
				GeometricFeaturesStrategy.MAJOR_AXIS_PERIMETER_RATIO,
				GeometricFeaturesStrategy.MAX_DIST_BOUNDARY_2_CENTROID,
				GeometricFeaturesStrategy.MIN_DIST_BOUNDARY_2_CENTROID,
				GeometricFeaturesStrategy.MEAN_DIST_BOUNDARY_2_CENTROID
		};
		
		CCMFeaturesStrategy[] ccmStrategies = {
				CCMFeaturesStrategy.CCM_ENTROPY_LL,
				CCMFeaturesStrategy.CCM_ENTROPY_AA,
				CCMFeaturesStrategy.CCM_ENTROPY_BB,
				CCMFeaturesStrategy.CCM_ENTROPY_LA,
				CCMFeaturesStrategy.CCM_ENTROPY_LB,
				CCMFeaturesStrategy.CCM_ENTROPY_AB,
				CCMFeaturesStrategy.CCM_ASM_LL,
				CCMFeaturesStrategy.CCM_ASM_AA,
				CCMFeaturesStrategy.CCM_ASM_BB,
				CCMFeaturesStrategy.CCM_ASM_LA,
				CCMFeaturesStrategy.CCM_ASM_LB,
				CCMFeaturesStrategy.CCM_ASM_AB,
				//CCMFeaturesStrategy.CCM_CONTRAST_LL,
				//CCMFeaturesStrategy.CCM_CONTRAST_AA,
				//CCMFeaturesStrategy.CCM_CONTRAST_BB,
				CCMFeaturesStrategy.CCM_CONTRAST_LA,
				CCMFeaturesStrategy.CCM_CONTRAST_LB,
				CCMFeaturesStrategy.CCM_CONTRAST_AB,
				CCMFeaturesStrategy.CCM_IDM_LL,
				CCMFeaturesStrategy.CCM_IDM_AA,
				CCMFeaturesStrategy.CCM_IDM_BB,
				CCMFeaturesStrategy.CCM_IDM_LA,
				CCMFeaturesStrategy.CCM_IDM_LB,
				CCMFeaturesStrategy.CCM_IDM_AB,
				CCMFeaturesStrategy.CCM_CORRELATION_LL,
				CCMFeaturesStrategy.CCM_CORRELATION_AA,
				CCMFeaturesStrategy.CCM_CORRELATION_BB,
				CCMFeaturesStrategy.CCM_CORRELATION_LA,
				CCMFeaturesStrategy.CCM_CORRELATION_LB,
				CCMFeaturesStrategy.CCM_CORRELATION_AB	
		};

		CurvatureFeaturesStrategy[] curvStrategies_sigma0 = {
				//CurvatureFeaturesStrategy.MIN_VALUE,
				//CurvatureFeaturesStrategy.MAX_VALUE,
				//CurvatureFeaturesStrategy.MEAN,
				CurvatureFeaturesStrategy.STD_DEV,
				CurvatureFeaturesStrategy.VARIANCE,
				CurvatureFeaturesStrategy.ENTROPY,
				CurvatureFeaturesStrategy.BENDING_ENERGY
		};
		
		this.featuresExtractor = new TCruzi_Tripomastigote_Giemsa();
		/*
				curvFeatList, LocalThresholdingController.sigmas,
				ccmStrategies);
		*/
		slic.setM(m);
		this.sample = null;	
        //appController.readAppConfigFile();
		initialized = true;
	}

	public static synchronized SLICController getInstance() {
		if (appController == null) {
			appController = new SLICController();
		}
		return appController;
	}


	public static Properties getProp(String filepath) throws IOException{
		Properties props = new Properties();
		FileInputStream file = new FileInputStream(filepath);
		props.load(file);
		return props;
	}

	public String getCurrentFile(){
		return mostRecentFile;
	}
	

	@Override
	public void readAppConfigFile(){
		Properties props;
		try {
			System.out.println("Reading "+appController.configFilePath);
			props = getProp(appController.configFilePath);
			appController.numOfCurvatures = Integer.valueOf((String)props.get("CURVATURES"));
			appController.numOfFeatures = 30 + 11 + 8*appController.numOfCurvatures;
			int numberOfClusters = Integer.valueOf((String)props.get("CLUSTERS"));
			String[] clusters = new String[numberOfClusters];
			for (int i = 0; i < numberOfClusters; i++){
				clusters[i] = new String((String)props.get("CLUSTER_"+i));
			}
			
			//TODO updateClusterList must be implemented here and in its panel
			//JMIRe.getInstance().updateClusterList(clusters);

			String destdir = (String)props.get("DEST_DIR");
			String srcdir = (String)props.get("SRC_DIR");

			if(destdir != null){
				appController.destDir = destdir;
			} else {
				//file chooser
				appController.sourceDir = appController.chooseFile("SOURCE");
			}

			if(srcdir != null){
				appController.sourceDir = srcdir;
			} else {
				appController.destDir = appController.chooseFile("DESTINATION");
			}

		} catch (IOException ex) {
			JMIRe.getInstance().displayMessage("Fatal error: "
					+ appController.configFilePath +" not found");
		}        
	}

	
	private String chooseFile(String name) {
		//Create a file chooser
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Select a "+name+" folder");
		String dir = "";                  

		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int returnVal = fc.showOpenDialog(JMIRe.getInstance());

		switch (returnVal) {
		case JFileChooser.APPROVE_OPTION:
			File file = fc.getSelectedFile();
			dir = file.getAbsolutePath(); 
			break;
		case JFileChooser.CANCEL_OPTION:
			if(!initialized) System.exit(0);
			break;
		default:
			dir = null;
			break;
		}

		return dir;
	}

	public String getSrcDir() {
		return appController.sourceDir;
	}

	public String getDestDir() {
		return appController.destDir;
	}

	public String changeSrcDir() {
		appController.sourceDir = appController.chooseFile("source");
		return appController.sourceDir;
	}

	public String changeDestDir() {
		appController.destDir = appController.chooseFile("destination");
		return appController.destDir;
	}

	public double getMValue() {
		return m;
	}

	public int getSValue() {
		return S;
	}
	
	@Override
	public JMIReApp getApplicationPanel() {
		return Applications.SLIC.getApp();
	}

	@Override
	public List<String> getFileList() {
		
		List<String> filenames = new LinkedList<>();
		File dir = new File(getInstance().sourceDir);
		
		if (dir.isDirectory()) {
			JMIRe.getInstance().displayMessage("Retrieving files for " + getInstance().sourceDir);
			for (File f : dir.listFiles()) {
				JMIRe.getInstance().displayMessage("Reading file " + f.getName());
				if (!f.isDirectory()) {
					filenames.add(f.getName());
				}
			}
		}
		JMIRe.getInstance().displayMessage(filenames.size() + " files found");
		Collections.sort(filenames);
		return filenames;
	}

	private void displaySuperpixels(int[][] edges, boolean showEdges) throws Exception {

		slic.compute(epson, maxIter);

		MBFImage mask = slic.getColoredSuperpixels();

		MBFImage img2Display = mask.clone();

		if(showEdges){
			MBFImage blend = mask.clone();

			FImage edgesImg = new FImage(edges.length, edges[0].length);
			for(int i = 0; i < edges.length; i++) {
				for(int j = 0; j < edges[0].length; j++) {
					edgesImg.setPixel(i, j, (float) edges[i][j]);
					if(edges[i][j] == 1) {
						blend.getBand(0).setPixel(i, j, 1.0f);
						blend.getBand(1).setPixel(i, j, 1.0f);
						blend.getBand(2).setPixel(i, j, 1.0f);
					}
				}
			}
			img2Display = blend;
		}

		imageDisplayer = DisplayUtilities.display(img2Display);

		for (Component component : imageDisplayer.getContentPane().getComponents()) {
			if (component instanceof ImageComponent) {
				component.addMouseListener(this);
				break;
			}
		}

		//imageDisplayer.setResizable(true);
	}

	private void changeParamsDisplaySuperpixels(int S, double m, float smooth, boolean showEdges) throws Exception {
		appController.S = S;
		appController.m = m;

		slic = new SLIC();

		slic.setM(m);

		double sigma = 2;
		int[][] edges = LoGEdgeDetection.getEdges(sample, sigma);

		if (smooth > 0.0f) {
			FGaussianConvolve gauss = new FGaussianConvolve(smooth);
			gauss.processImage(appController.sample.getBand(0));
			gauss.processImage(appController.sample.getBand(1));
			gauss.processImage(appController.sample.getBand(2));
		}

		slic.initializedSeeds(S, sample, edges);

		this.displaySuperpixels(edges, showEdges);
	}

	public void selectImage(String filename, int S, double m, float smooth, boolean showEdges) throws Exception {

		mostRecentFile = filename;

		if (imageDisplayer != null) {
			imageDisplayer.dispose();
		}

		String absolutePath = appController.sourceDir + File.separator + filename;

		//SLICSegmentationApp3.getInstance().displayMessage("Processing image " + filename + " ...");

		File imageFile = new File(absolutePath);
		if (imageFile.exists()) {
			MBFImage image = ImageUtilities.readMBF(imageFile);
			appController.mask = null;
			appController.sample = image.clone();
			appController.changeParamsDisplaySuperpixels(S, m, smooth, showEdges);
			//SLICSegmentationApp3.getInstance().displayMessage("Done.");

		} else {
			JMIRe.getInstance().displayMessage("Image not found at " + absolutePath);
		}

	}

	public void saveImage(String destFolder, String filename, FImage image) throws IOException {
		ImageUtilities.write(image, new File(destFolder+File.separator+filename));
	}

	public Coord2D getMaskCentroid() throws Exception{
		ComplexSignalContour2 contourSignal = new ComplexSignalContour2(appController.mask,0,0);
        Coord2D coord = GeometricFeatures.centroid(contourSignal);
		return coord;
	}


	public double[] getFeaturesVector(double scale) throws Exception {
		ComplexSignalContour2 contourSignal = new ComplexSignalContour2(appController.mask, 0, 0);
		return this.featuresExtractor.getFeaturesVector(sample, mask, contourSignal, scale);
	}	

	public List<String> getFeaturesCSVHeader() {
		return this.featuresExtractor.getFeaturesHeader();
	}

	public String[] getCentroidHeader(){
		String[] header = {"centroid_x", "centroid_y"};
		return header;
	}

        @Override
	public double[][] readFeaturesCSV(String src_dir, String cluster) throws Exception{

		String filename = src_dir+File.separator+cluster+"_features.csv";

		int objectCounter = -1;
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line;
		while ((line = br.readLine()) != null) {
			objectCounter++;
		}
		br.close();

		if(objectCounter > 0) {
			double[][] features = new double[objectCounter][numOfFeatures];
			objectCounter = 0;
			br = new BufferedReader(new FileReader(filename));
			br.readLine(); //consuming header
			while ((line = br.readLine()) != null) {
				String[] featuresStr = line.split(csv_separator);
				//i == 0 => class identifier
				for(int i = 0; i < featuresStr.length-1; i++) {
					features[objectCounter][i] = Double.valueOf(featuresStr[i]);
				}
				objectCounter++;
			}
			br.close();

			return features;
		}

		return null;
	}

	public double[][] standardize(double[][] matrix){
		double[] mean = new double[matrix[0].length];
		double[] stdDev = new double[matrix[0].length];

		for(int i = 0; i < matrix[0].length; i++) {

			mean[i] = 0;
			stdDev[i] =0;

			for(int j = 0; j < matrix.length; j++) {
				mean[i] += matrix[j][i];
			}
			mean[i] /= matrix.length;

			for(int j = 0; j < matrix.length; j++) {
				stdDev[i] += (matrix[j][i]-mean[i])*(matrix[j][i]-mean[i]);
			}
			stdDev[i] = Math.sqrt(stdDev[i]/(matrix.length-1));

			for(int j = 0; j < matrix.length; j++) {
				if(stdDev[i] > 0)
					matrix[j][i] = (matrix[j][i] - mean[i])/stdDev[i];
			}
		}
		return matrix;
	}


	public double convertPixel2milimeter(double length_in_pixels, double ppi) {
		final double inch = 25.4;
		double px_in_mm = inch/ppi;
		return length_in_pixels * px_in_mm;
	}

	public double rescaleFromZoom(double value, double zoom) {
		return value / zoom;
	}

	public double convert2micrometer(double length_in_pixels, double ppi) {
		return convertPixel2milimeter(length_in_pixels, ppi) * 1000;
	}

	public double scaleAndConvert2micrometer(double length_in_pixels, double ppi, double zoom) {
		return rescaleFromZoom(convert2micrometer(length_in_pixels, ppi), zoom);
	}

	public boolean writeData(String dest_dir, String cluster, Coord2D centroid, double[] features) throws Exception{
		if(features == null || features.length == 0) return false;
		int a = writeCentroid2Cluster(dest_dir, cluster, centroid);
		int b = writeFeature2Cluster(dest_dir, cluster, features);
		return a==b;
	}

	public void writeParameters(String dest_dir, String cluster, double[] params) throws Exception{
		new File(dest_dir).mkdirs();

		String filename = dest_dir+File.separator+"slic_parameters.csv";

		File csv = new File(filename);

		if(!csv.exists()) {
			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			writer.append("cluster"+csv_separator+"S"+csv_separator+"m"+csv_separator+"smooth\n");
			writer.flush();
			writer.close();
		}   
		BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));
		if(params.length > 0) writer.append("\n");
		writer.append(cluster);
		for(int i = 0; i < params.length; i++) {
			writer.append(csv_separator+params[i]);
		}
		writer.flush();
		writer.close();
	}

	public int writeCentroid2Cluster(String dest_dir, String cluster, Coord2D centroid) throws Exception{

		new File(dest_dir).mkdirs();

		String filename = dest_dir+File.separator+cluster+"_centroid.csv";

		File csv = new File(filename);

		if(!csv.exists()) {

			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			String[] header = getCentroidHeader();
			writer.append(header[0]+csv_separator+header[1]);
			writer.flush();
			writer.close();        	
		}        


		BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));
		writer.append("\n");
		writer.append(centroid.getX()+csv_separator+centroid.getY());
		writer.flush();
		writer.close();

		int objectCounter = -1;
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			while (br.readLine() != null) {
				objectCounter++;
			}
			br.close();
		}

		return objectCounter; 
	}

	public int writeFeature2Cluster(String dest_dir, String cluster, double[] features) throws Exception{
		int objectCounter = 0;

		String filename = dest_dir+File.separator+cluster+"_features.csv";		

		File csv = new File(filename);

		if(!csv.exists()) {

			PrintWriter writer = new PrintWriter(filename, "UTF-8");			
			List<String> header = getFeaturesCSVHeader();
			writer.append(header.get(0));
			for(int i = 1; i < header.size(); i++) {
				writer.append(csv_separator+header.get(i));
			}
			writer.flush();
			writer.close();        	
		}        



		BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));

		if(features.length > 0) writer.append("\n");

		writer.append(features[0]+"");
		for(int i = 1; i < features.length; i++) {
			writer.append(csv_separator+features[i]);
		}
		writer.append(csv_separator+cluster);
		writer.flush();
		writer.close();

		objectCounter = -1;
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			while (br.readLine() != null) {
				objectCounter++;
			}
			br.close();
		}

		return objectCounter;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		int x = -1;
		int y = -1;

		if (e.getComponent() instanceof ImageComponent) {
			x = (int) ((ImageComponent) e.getComponent()).getCurrentMouseImagePosition().getX();
			y = (int) ((ImageComponent) e.getComponent()).getCurrentMouseImagePosition().getY();
		}

		JMIRe.getInstance().displayMessage("Image clicked at " + x + ", " + y);

		if (x >= 0 && x < appController.sample.getWidth()
				&& y >= 0 && y < appController.sample.getHeight()) {
			JMIRe.getInstance().displayMessage("pixel value: " + appController.sample.getBand(0).getPixel(x, y));

			appController.mask = slic.getClusterMask(x, y);

			imageDisplayer.dispose();
			imageDisplayer = DisplayUtilities.display(appController.mask);

		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		return;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		return;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		return;
	}

	@Override
	public void mouseExited(MouseEvent e) {
		return;
	}

	public static void main(String[] args) throws Exception {
		SLICController appController = SLICController.getInstance(); 
		double scale = appController.scaleAndConvert2micrometer(1, 72, 4000);
		String src_dir = "/home/dmatos/workspace2/JMIRe/";
		String cluster = "NUCELUS";
		double[][] features = appController.readFeaturesCSV(src_dir, cluster);

		/*
		for(int j = 0; j < features.length; j++) {
			int i = 0;
			for(i = features[j].length-1; i >= features[j].length - 11; i--) {
				features[j][i] *= scale;
			}
			features[j][i-6] *= scale;
			features[j][i-14] *= scale;
		}
		 */

		features = appController.standardize(features);

		String dest_path = "/home/dmatos/workspace2/JMIRe/features_normalized.csv";

		String csv_separator = ",";

		PrintWriter writer = new PrintWriter(dest_path, "UTF-8");

		List<String> header = appController.getFeaturesCSVHeader(); 
		writer.append(header.get(0));
		for(int i = 1; i < header.size(); i++) {
			writer.append(csv_separator+header.get(i));
		}

		String[] classes = {"KINETOPLAST","NUCLEUS"};

		for(int i = 0; i < features.length; i++) {
			writer.append("\n"+classes[i%2]);
			for(int j = 0; j < features[i].length; j++) {
				writer.append(csv_separator+features[i][j]);
			}
		}

		writer.close();

		appController = null;

		System.exit(0);
	}
}
