package br.mire.segmentation.controllers;

import java.util.List;

import br.mire.segmentation.application.JMIRe.Applications;
import br.mire.segmentation.view.apps.JMIReApp;

public class JMIReController{

	private static JMIReController appController = null;
	
	public static synchronized JMIReController getController(Applications app) {
		switch(app) {		
		case LOCAL_THRESH:
			appController = LocalThresholdingController.getInstance();
			LocalThresholdingController.getInstance().setGUI(true);
			
			break;
		case SLIC:
			appController = SLICController.getInstance();
			break;
		default: appController = null;
		}
		return appController;
	}
	
	/**
	 * 
	 * @return SLIC app controller by default
	*/
	public static synchronized JMIReController getController() {
		if(appController == null) {
			appController = LocalThresholdingController.getInstance(); //SLICController.getInstance();
			
		}
		return appController;
	}

	public void readAppConfigFile() {		
		getController().readAppConfigFile();
	}

	public List<String> getFileList(){
		System.out.println("loop");
		//return null;
		return getController().getFileList();
	}
	
	public String getSrcDir() {
		return getController().getSrcDir();
	}
	
	public double[][] readFeaturesCSV(String dest_folder, String cluster) throws Exception{
		return getController().readFeaturesCSV(dest_folder, cluster);
	}

	public JMIReApp getApplicationPanel() {
		return getController().getApplicationPanel();
	}
	
}