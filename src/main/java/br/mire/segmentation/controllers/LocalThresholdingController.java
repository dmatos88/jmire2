/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.mire.segmentation.controllers;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.Transforms;
import org.openimaj.math.geometry.point.Point2d;

import br.mire.features.extraction.GeometricFeatures;
import br.mire.ml.classifier.generic.JMIReClassifier;
import br.mire.ml.classifier.tcruzi.TCruzi_Tripomastigote_Giemsa;
import br.mire.segmentation.application.JMIRe;
import br.mire.segmentation.application.JMIRe.Applications;
import br.mire.segmentation.contour.ComplexSignalContour2;
import br.mire.segmentation.local_thresholding.LocalThresholding;
import br.mire.segmentation.view.apps.JMIReApp;
import br.mire.segmentation.view.apps.LocalThresholdingPanel;
import br.mire.utils.image.HistogramEq;
import br.mire.utils.image.PreprocessField;
import br.mire.utils.math.Coord2D;
import br.mire.utils.math.MetricConversions;

/**
 *
 * @author dmatos
 */
public class LocalThresholdingController extends JMIReController implements
MouseListener {

	static Logger log = Logger.getLogger(LocalThresholdingController.class.getName());

	@SuppressWarnings("unused")
	private static final long serialVersionUID = -3187203810780203621L;
	private static LocalThresholdingController appController = null;

	public MBFImage sample;
	public FImage thresh_sample;	
	public static final String csv_separator = ",";
	
	private MBFImage sampleExtraction;
	private FImage maskExtraction;
	
	private LocalThresholding localThresholding;
	private String destDir;
	private String sourceDir;
	private int radius = 3;
	private float sigma = 1.5f;
	private float threshold = 0.82f;
	private float cutoff = 0.15f;
	private double scale = 0.07;
	
	private final String configFilePath = "jmire_local_thresh.properties";
	private String mostRecentFile;
	private boolean initialized = false;

	private FImage mask;
	private Coord2D click;

	private boolean hasImageBuffered = false;
	private boolean useGUI = true;

	private double field_number = 0;
	private double objective_mag = 0;
	private double ocular_mag = 0;
	private String selected_cluster = "";
	
	private int tcruzi_count = 5741;
	private int unknown_count = 3972;

	/**
	 * 
	 * @param false for off true for on, default on
	 * @return
	 */
	public boolean setGUI(boolean flag) {
		this.useGUI = flag;
		return this.useGUI;
	}

	public void logMessage(String msg) {
		if(useGUI) {
			JMIRe.getInstance().displayMessage(msg);
		} else {
			log.debug(msg);
		}
	}

	private static JFrame imageDisplayer = null;

	private JMIReClassifier featuresExtractor;

	public LocalThresholdingController() {

		appController = this;

		log.setLevel(Level.DEBUG);
		
		log.info("Initializing LocalThresholdingController");

		this.featuresExtractor = new TCruzi_Tripomastigote_Giemsa();
		localThresholding = new LocalThresholding();
		//appController.readAppConfigFile();        
		initialized = true;
		
		log.info("LocalThresholdingController intialized");

	}

	public static synchronized LocalThresholdingController getInstance() {
		if (appController == null) {
			appController = new LocalThresholdingController();
		}                
		return appController;
	}

	@Override
	public void readAppConfigFile() {
		Properties props;
		try {
			log.debug("Reading "+appController.configFilePath);
			appController = LocalThresholdingController.getInstance();
			props = getProp(appController.configFilePath);
			int numberOfClusters = Integer.valueOf((String) props.get("CLUSTERS"));
			String[] clusters = new String[numberOfClusters];
			for (int i = 0; i < numberOfClusters; i++) {
				clusters[i] = new String((String) props.get("CLUSTER_" + i));
				log.debug("CLUSTER_" + i+": "+clusters[i]);
			}
			((LocalThresholdingPanel)appController.getApplicationPanel()).updateClusterList(clusters);

			String destdir = (String) props.get("DEST_DIR");
			String srcdir = (String) props.get("SRC_DIR");

			if (srcdir != null) {
				log.debug("Source dir: "+srcdir);
				appController.sourceDir = srcdir;
			} else {
				//file chooser
				appController.sourceDir = appController.chooseFile("SOURCE");
			}			

			if (destdir != null) {
				log.debug("Destination dir: "+destdir);
				appController.destDir = destdir;
			} else {
				appController.destDir = appController.chooseFile("DESTINATION");
			}             

			log.debug("CHANGING SELECTED CLUSTER");

			this.changeSelectedCluster();

			log.debug("FINISHED CHANGING SELECTED CLUSTER");

		} catch (IOException ex) {
			appController.logMessage("Fatal error: "
					+ appController.configFilePath + " not found");
		}
	}

	public int getRadius() {
		return radius;
	}

	public float getSigma() {
		return sigma;
	}

	public float getThreshold() {
		return threshold;
	}

	public float getCutoff() {
		return cutoff;
	}

	public void clearBuffer(){
		if(imageDisplayer != null) imageDisplayer.dispose();
		hasImageBuffered = false;
	}

	public static Properties getProp(String filepath) throws IOException {
		Properties props = new Properties();
		FileInputStream file = new FileInputStream(filepath);
		props.load(file);
		return props;
	}

	public List<String> getFileList() {

		log.debug("Source folder "+sourceDir);

		List<String> filenames = new LinkedList<>();
		File dir = new File(getInstance().sourceDir);

		if (dir.isDirectory()) {
			appController.logMessage("Retrieving files for " + getInstance().sourceDir);
			for (File f : dir.listFiles()) {
				appController.logMessage("Reading file " + f.getName());
				if (!f.isDirectory()) {
					filenames.add(f.getName());
				}
			}
		} else {
			log.debug(sourceDir+" is not a valid directory");
		}
		appController.logMessage(filenames.size() + " files found");
		Collections.sort(filenames);
		return filenames;
	}

	private String chooseFile(String name) {
		//Create a file chooser
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Select a " + name + " folder");
		String dir = "";

		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int returnVal = fc.showOpenDialog(JMIRe.getInstance());

		switch (returnVal) {
		case JFileChooser.APPROVE_OPTION:
			File file = fc.getSelectedFile();
			dir = file.getAbsolutePath();
			break;
		case JFileChooser.CANCEL_OPTION:
			if (!initialized) {
				System.exit(0);
			}
			break;
		default:
			dir = null;
			break;
		}

		return dir;
	}

	public String changeDestDir() {
		getInstance().destDir = appController.chooseFile("DESTINATION");
		return getInstance().destDir;
	}

	public void setDestDir(String destDir) {
		getInstance().destDir = destDir;
	}

	@Override
	public String getSrcDir() {
		return getInstance().sourceDir;
	}

	public String getDestDir() {
		return getInstance().destDir;
	}

	public String getCurrentFile() {
		return mostRecentFile;
	}

	private FImage floodFillMaskFromOrigin(FImage mask) {
		List<Coord2D> pxlist = new LinkedList<Coord2D>();
		pxlist.add(new Coord2D(0,0));

		FImage newMask = new FImage(mask.getWidth(), mask.getHeight());
		newMask.fill(1.0f);

		Coord2D c;
		int x, y;
		while(!pxlist.isEmpty()) {
			c = pxlist.remove(0);
			x = (int) c.getX();
			y = (int) c.getY();
			if(mask.getPixel(x,y) == 0) {
				newMask.setPixel(x, y, 0f);
				mask.setPixel(x, y, 1f); //flag as visited
				for(int i = -1; i < 2; i++) {
					for(int j = -1; j < 2; j++) {
						if(x + i >= 0 && y + j >= 0 & x + i < mask.getWidth() && j + y < mask.getHeight()) {
							if(mask.getPixel(x+i, y+j) == 0) {
								pxlist.add(new Coord2D(x+i, y+j));
							}
						}
					}
				}
			}
		}

		return newMask;
	}

	public void setOcularMagnification(double ocular_magnification) {
		appController.ocular_mag = ocular_magnification;
	}

	public void setObjectiveMagnification(double objective_magnification) {
		appController.objective_mag = objective_magnification;
	}

	public void setFieldNumber(double fieldNumber) {
		appController.field_number = fieldNumber;
	}

	private double getScale(FImage img) throws Exception{
		
		double pixel_side_in_um = MetricConversions.getScale(img, field_number, objective_mag, ocular_mag);

		double mice_rbc_diameter_in_px = 80; 

		appController.logMessage("Pixel size: "+pixel_side_in_um+"um");		

		appController.logMessage("Mice RBC diameter: "+mice_rbc_diameter_in_px * pixel_side_in_um);

		return pixel_side_in_um;
	}

	private void displayForTests(FImage band, String title) {
		JFrame frame = DisplayUtilities.display(band);
		frame.setTitle(title);
		frame.setSize(1000, 800);
		frame.setResizable(true);
	}


	

	public FImage threshold2limits(FImage image, float thr1, float thr2) {
		FImage result = new FImage(image.width, image.height);
		for(int i = 0; i < image.width; i++) {
			for(int j = 0; j < image.height; j++) {
				if(image.getPixel(i,j) >= thr1 && image.getPixel(i,j) <= thr2) result.setPixel(i, j, 1f);
			}
		}
		return result;
	}

	/**
	 * 
	 * @param radius
	 * @param sigma
	 * @param threshold
	 * @param cutoff
	 * @throws Exception
	 */
	private void changeParamsDisplaySuperpixels2(int radius, float sigma, float threshold, float cutoff) throws Exception {
		Long start = System.currentTimeMillis();

		String params = "[rad:"+radius+"][sigma:"+sigma+"][thresh:"+threshold+"][cut"+cutoff+"]";
		appController.logMessage(params);
		
		appController.radius = radius;
		appController.sigma = sigma;
		appController.threshold = threshold;
		appController.cutoff = cutoff;

		appController.sample = Transforms.RGB_TO_CIELabNormalised(sample);

		FImage l_star = sample.getBand(0).clone();

		PreprocessField processor = new PreprocessField();
		
		FImage l_star_thresh_image = processor.offGanglion(l_star);
		l_star_thresh_image.normalise();
		
		//The difference between a crop anda a whole image is the mask preparation
		
		//whole field
		//FImage mask = processor.getFieldMask(l_star, 5);
		
		//cropped
		FImage mask = new FImage(l_star_thresh_image.width, l_star_thresh_image.height);
		mask.fill(1f);
		/*
		if(useGUI) scale = JMIRe.getInstance().getScale(mask);
		else scale = appController.getScale(mask);
		*/
		
		scale = 0.07;
		
		/*
		HistogramEq hist = new HistogramEq(101);

		hist.processHistogram(l_star, mask);

		boolean isBimodal = hist.isBimodal(l_star, mask, 101);
		
		log.info("Bimodal: "+isBimodal);
		
		if(isBimodal) {
			appController.logMessage("Bimodal");
			appController.thresh_sample = processor.localAdaptiveThresholdSauvola(l_star, 0.5, threshold, radius);
		} else {
			appController.logMessage("Unimodal");

			hist.equalize(l_star, mask);
			
			appController.thresh_sample = processor.localAdaptiveThresholdSauvola(l_star, 0.5, threshold, radius);
		}
		
		//TODO comentar abaixo se for usar acima
		//appController.thresh_sample = processor.localAdaptiveThresholdSauvola(l_star, 0.5, 0.01, 29);
		
		*/
		appController.thresh_sample = processor.localAdaptiveThresholdSauvola(l_star, 0.5, threshold, radius);
		
		long end = System.currentTimeMillis() - start;
		
		appController.logMessage("Took "+(end/1000)+"s");
		this.displayResultImage();		

	}

	@Override
	public JMIReApp getApplicationPanel() {
		return Applications.LOCAL_THRESH.getApp();
	}

	private void displayResultImage(){
		if(appController.useGUI) {
			if(imageDisplayer != null)	imageDisplayer.dispose();

			/*
			JFrame thresh_displayer = DisplayUtilities.display(thresh_sample);
			thresh_displayer.setTitle(mostRecentFile+" - Threshold");
			thresh_displayer.setSize(1800, 900);
			thresh_displayer.setResizable(true);
			 */

			//green band
			FImage temp = appController.thresh_sample.clone(); 
			temp.multiplyInplace(sample.getBand(0));
			/*
			thresh_sample = new FImage(sample.getWidth(), sample.getHeight());
			thresh_sample.fill(1f);
			FImage temp = sample.getBand(1);
			*/

			imageDisplayer = DisplayUtilities.display(temp);

			imageDisplayer.setSize(900, 900);
			imageDisplayer.setResizable(true);
			imageDisplayer.setTitle(mostRecentFile+" - Result");
			imageDisplayer.getContentPane().getComponent(0).addMouseListener(this);

		}
	}

	/**
	 * clusters names separated by csv_separator
	 * @param clusters
	 */
	public void setSelectedCluster(String clusters_names) {
		this.selected_cluster = clusters_names;
	}


	public void extractFeatures(String dest_dir) throws Exception {

		appController.logMessage("Croping");
		//this.cropTcruzi224();
		
		appController.logMessage("Extracting...");

		if(appController.useGUI) {
			appController.selected_cluster = ((LocalThresholdingPanel)appController.getApplicationPanel()).getSelectedCluster();
		} 

		String clusters = selected_cluster;	

		appController.logMessage("Cluster: "+clusters);
		//log.debug("ZOOM: "+JMIRe.getInstance().getZoomFactor());
		double[] f = LocalThresholdingController.getInstance().getFeaturesVector();
		//Coord2D centroid = LocalThresholdingController.getInstance().getMaskCentroidFromClick();
		Coord2D click = LocalThresholdingController.getInstance().getClick();
		LocalThresholdingController.getInstance().writeData(dest_dir, clusters, click, f);			
		//LocalThresholdingController.getInstance().closeResultDisplayer();
		
		appController.logMessage("Features extraction complete for "+clusters);
	}
	
	private void cropTcruzi224() throws Exception{
		//crop t_cruzi
		String filename;
		if(tcruzi_count < 10) filename = "000"+(tcruzi_count++)+".jpg";
		else if(tcruzi_count < 100) filename = "00"+(tcruzi_count++)+".jpg";
		else if(tcruzi_count < 1000) filename = "0"+(tcruzi_count++)+".jpg";
		else filename = ""+(tcruzi_count++)+".jpg";
		
		Coord2D click = LocalThresholdingController.getInstance().getClick();
		
		String tcruzi_dest_dir = "/home/dmatos/tcruzi_data/data/tcruzi_data_train/unknown/";
		
		MBFImage crop = sample.extractROI((int)click.getX()-111, (int)click.getY()-111, 224, 224);
		ImageUtilities.write(crop, new File(tcruzi_dest_dir+"u"+filename));
		
		/*
		//upper left
		if(unknown_count < 10) filename = "000"+(unknown_count++)+".jpg";
		else if(unknown_count < 100) filename = "00"+(unknown_count++)+".jpg";
		else if(unknown_count < 1000) filename = "0"+(unknown_count++)+".jpg";
		else filename = ""+(unknown_count++)+".jpg";
		
		String unknown_dest_dir = "/home/dmatos/tcruzi_data/data/tcruzi_data_train/unknown/";
		crop = sample.extractROI((int)click.getX()-111-224, (int)click.getY()-111-224, 224, 224);
		ImageUtilities.write(crop, new File(unknown_dest_dir+"u"+filename));
		*/
		
	}

	public void selectImage(
			String filename,
			int radius,
			float sigma,
			float threshold,
			float cutoff) throws Exception {

		if(hasImageBuffered && mostRecentFile != null && mostRecentFile.equals(filename)){
			this.displayResultImage();
			return;
		}

		hasImageBuffered = true;

		mostRecentFile = filename;                   

		if (imageDisplayer != null) {
			imageDisplayer.dispose();
		}

		String absolutePath = appController.sourceDir + File.separator + filename;

		log.debug("Processing image " + filename + " ...");
		File imageFile = new File(absolutePath);
		if (imageFile.exists()) {
			MBFImage image = ImageUtilities.readMBF(imageFile);
			appController.mask = null;
			appController.sample = image.clone();
			appController.changeParamsDisplaySuperpixels2(radius, sigma, threshold, cutoff);
			//SLICSegmentationApp3.getInstance().displayMessage("Done.");

		} else {
			appController.logMessage("Image not found at " + absolutePath);
		}

	}

	public void closeResultDisplayer() {
		if(imageDisplayer != null) {
			imageDisplayer.setVisible(false);
			imageDisplayer.dispose();
		}

	}

	public void saveImage(String destFolder, String filename, FImage image) throws IOException {
		ImageUtilities.write(image, new File(destFolder + File.separator + filename));
	}

	public Coord2D getMaskCentroidFromClick() throws Exception{		
		int x = (int) click.getX() - 100;
		int y = (int) click.getY() - 100;
		ComplexSignalContour2 contourSignal = new ComplexSignalContour2(appController.mask, x, y);
		Coord2D coord = GeometricFeatures.centroid(contourSignal);
		return coord;
	}

	public double[] getFeaturesVector() throws Exception {
		int x = 0; //(int) click[index].getX() - 150;
		int y = 0; //(int) click[index].getY() - 150;

		ComplexSignalContour2 contourSignal = new ComplexSignalContour2(appController.maskExtraction, x, y);
		return this.featuresExtractor.getFeaturesVector(sampleExtraction, appController.maskExtraction, contourSignal, scale);
	}

	public List<String> getFeaturesCSVHeader() {
		return this.featuresExtractor.getFeaturesHeader();
	}

	public String[] getCentroidHeader() {
		String[] header = {"centroid_x", "centroid_y"};
		return header;
	}

	public int countClusterItens(String src_dir, String cluster) throws Exception {

		String filename = src_dir + File.separator + cluster+"_features.csv";

		log.debug("Reading file: " +filename);

		int objectCounter = -1;
		BufferedReader br = new BufferedReader(new FileReader(filename));

		log.debug("Counting objects");
		while (br.readLine() != null) {
			objectCounter++;
		}
		br.close();
		log.debug("Counted");

		return objectCounter;

	}

	public double[][] standardize(double[][] matrix) {
		double[] mean = new double[matrix[0].length];
		double[] stdDev = new double[matrix[0].length];

		for (int i = 0; i < matrix[0].length; i++) {

			mean[i] = 0;
			stdDev[i] = 0;

			for (int j = 0; j < matrix.length; j++) {
				mean[i] += matrix[j][i];
			}
			mean[i] /= matrix.length;

			for (int j = 0; j < matrix.length; j++) {
				stdDev[i] += (matrix[j][i] - mean[i]) * (matrix[j][i] - mean[i]);
			}
			stdDev[i] = Math.sqrt(stdDev[i] / (matrix.length - 1));

			for (int j = 0; j < matrix.length; j++) {
				if (stdDev[i] > 0) {
					matrix[j][i] = (matrix[j][i] - mean[i]) / stdDev[i];
				}
			}
		}
		return matrix;
	}

	public double convertPixel2milimeter(double length_in_pixels, double ppi) {
		final double inch = 25.4;
		double px_in_mm = inch / ppi;
		return length_in_pixels * px_in_mm;
	}

	public double rescaleFromZoom(double value, double zoom) {
		return value / zoom;
	}

	public double convert2micrometer(double length_in_pixels, double ppi) {
		return convertPixel2milimeter(length_in_pixels, ppi) * 1000;
	}

	public double scaleAndConvert2micrometer(double length_in_pixels, double ppi, double zoom) {
		return rescaleFromZoom(convert2micrometer(length_in_pixels, ppi), zoom);
	}

	public boolean writeData(String dest_dir, String cluster, Coord2D click, double[] features) throws Exception {
		if (features == null || features.length == 0) {
			return false;
		}

		writeGroundTruthCentroid(dest_dir, cluster, click); //click instead of centroid for auto extraction
		//int a = writeCentroid2Cluster(dest_dir, cluster, centroid);
		writeFeature2Cluster(dest_dir, cluster, features);
		//writeClickCluster(dest_dir, cluster, click);
		return true;
	}

	public void writeParameters(String dest_dir, String cluster, double[] params) throws Exception {
		new File(dest_dir).mkdirs();

		String filename = dest_dir + File.separator + "local_thresh_parameters.csv";

		File csv = new File(filename);

		if (!csv.exists()) {
			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			writer.append("cluster" + csv_separator + "S" + csv_separator + "m" + csv_separator + "smooth\n");
			writer.flush();
			writer.close();
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));
		if (params.length > 0) {
			writer.append("\n");
		}
		writer.append(cluster);
		for (int i = 0; i < params.length; i++) {
			writer.append(csv_separator + params[i]);
		}
		writer.flush();
		writer.close();
	}


	public int writeGeodesicDistances2CSV(String dest_dir, double[] geodesicDistance) throws IOException {

		new File(dest_dir).mkdirs();

		String filename = dest_dir + File.separator + "geodesic_distances.csv";

		File csv = new File(filename);

		if (!csv.exists()) {
			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			writer.append("geodesic_distances");
			writer.flush();
			writer.close();
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));
		writer.append("\n"+geodesicDistance[0]);
		for(int i = 1; i < geodesicDistance.length; i++) {
			writer.append(csv_separator + geodesicDistance[i]);
		}

		writer.flush();
		writer.close();

		int objectCounter = -1;
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			while (br.readLine() != null) {
				objectCounter++;
			}
			br.close();
		}

		return objectCounter;
	}

	public int writeClickCluster(String dest_dir, String cluster, Coord2D click) throws Exception {
		new File(dest_dir).mkdirs();

		String filename = dest_dir + File.separator + cluster + "_click.csv";

		File csv = new File(filename);

		if (!csv.exists()) {

			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			String[] header = getCentroidHeader();
			writer.append(header[0] + csv_separator + header[1]);
			writer.flush();
			writer.close();
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));
		writer.append("\n");
		writer.append(click.getX() + csv_separator + click.getY());
		writer.flush();
		writer.close();

		int objectCounter = -1;
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			while (br.readLine() != null) {
				objectCounter++;
			}
			br.close();
		}

		return objectCounter;
	}

	public String[] getGroundTruthHeader() {
		String[] header = {"filename","x","y","structure"};
		return header;
	}


	public void writeGroundTruthCentroid(String dest_dir, String cluster, Coord2D centroid) throws IOException {

		new File(dest_dir).mkdirs();

		String filename = dest_dir + File.separator + "ground_truth.csv";

		File csv = new File(filename);

		if (!csv.exists()) {

			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			String[] header = getGroundTruthHeader();
			writer.append(header[0]);
			for(int i = 1; i < header.length; i++) {
				writer.append(csv_separator + header[i]);
			}

			writer.flush();
			writer.close();
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));
		writer.append(
				"\n"
						+mostRecentFile + csv_separator
						+centroid.getX() + csv_separator
						+ centroid.getY() + csv_separator
						+ cluster);
		writer.flush();
		writer.close();		
	}

	public int writeCentroid2Cluster(String dest_dir, String cluster, Coord2D centroid) throws Exception {

		new File(dest_dir).mkdirs();

		String filename = dest_dir + File.separator + cluster + "_centroid.csv";

		File csv = new File(filename);

		if (!csv.exists()) {

			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			String[] header = getCentroidHeader();
			writer.append(header[0] + csv_separator + header[1]);
			writer.flush();
			writer.close();
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));
		writer.append("\n");
		writer.append(centroid.getX() + csv_separator + centroid.getY());
		writer.flush();
		writer.close();

		int objectCounter = -1;
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			while (br.readLine() != null) {
				objectCounter++;
			}
			br.close();
		}

		return objectCounter;
	}

	public int writeFeature2Cluster(String dest_dir, String cluster, double[] features) throws Exception {
		int objectCounter = 0;

		String filename = dest_dir + File.separator + cluster+"_features.csv";

		File csv = new File(filename);

		if (!csv.exists()) {

			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			List<String> header = getFeaturesCSVHeader();
			writer.append(header.get(0));
			for (int i = 1; i < header.size(); i++) {
				writer.append(csv_separator + header.get(i));
			}
			writer.flush();
			writer.close();
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));

		if (features.length > 0) {
			writer.append("\n");
		}

		writer.append(features[0] + "");
		for (int i = 1; i < features.length; i++) {
			writer.append(csv_separator + features[i]);
		}
		writer.append(csv_separator + cluster);
		writer.flush();
		writer.close();

		objectCounter = -1;
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			while (br.readLine() != null) {
				objectCounter++;
			}
			br.close();
		}

		return objectCounter;
	}

	public Coord2D getClick() {
		return appController.click;
	}

	public Coord2D getNearestPointFromCentroid(FImage mask, Coord2D centroid) throws Exception {
		ComplexSignalContour2 contourSignal = new ComplexSignalContour2(mask, 0, 0);

		Coord2D nearest = null;

		double min_dist = Double.MAX_VALUE;
		double dist = 0.0;
		for(int i = 0; i < contourSignal.getXSignal().length; i++) {			
			dist = centroid.getEuclideanDistance(new Coord2D(contourSignal.getXSignal()[i], contourSignal.getYSignal()[i]));
			if(dist < min_dist) {
				min_dist = dist;
				nearest = new Coord2D(contourSignal.getXSignal()[i], contourSignal.getYSignal()[i]);
			}
		}
		return nearest;
	}


	public void selectCoordinates(int x, int y) {

		appController.logMessage("Image clicked at " + x + ", " + y);

		if (x >= 0 && x < appController.sample.getWidth()
				&& y >= 0 && y < appController.sample.getHeight()) {

			appController.logMessage("pixel value: " + appController.thresh_sample.getPixel(x, y));
			
			if(appController.thresh_sample.getPixel(x, y) == 0) return;

			int limit = 2000;
			appController.mask = localThresholding.getClusterMask(appController.thresh_sample,x, y, limit);

			//TODO extractFeatures deve usar maskExtract e sampleExtract para não sobrescrever mask e sample
			//minimize area
			appController.maskExtraction  = appController.mask.extractROI(x-256, y-256, 512, 512);
			appController.sampleExtraction = appController.sample.extractROI(x-256, y-256, 512, 512);
			
			appController.logMessage("# of pixels: "+appController.maskExtraction.sum());

			appController.click = new Coord2D(x,y);

			//if(appController.mask != null) {
				appController.logMessage("Ready for extraction");
				/*
				if(imageDisplayer != null) imageDisplayer.dispose();
				imageDisplayer = DisplayUtilities.display(appController.mask);		
				imageDisplayer.setSize(1000,800);
				imageDisplayer.setResizable(true);
				imageDisplayer.setTitle(mostRecentFile+" SELECTED MASK");
				*/
			//}

		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		if (e.getButton() == MouseEvent.BUTTON3) {			

			Point2d p = ((DisplayUtilities.ImageComponent)imageDisplayer.getContentPane().getComponent(0)).getCurrentMouseImagePosition();

			int x = (int) p.getX();
			int y = (int) p.getY();

			appController.selectCoordinates(x, y);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		return;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		return;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		return;
	}

	@Override
	public void mouseExited(MouseEvent e) {
		return;
	}


	public void changeSelectedCluster() {

		//log.debug("I'm going through change...");		

		String dest_folder = this.destDir;
		String cluster = ((LocalThresholdingPanel)appController.getApplicationPanel()).getSelectedCluster();

		log.debug("CHANGING SELECTED CLUSTER TO: "+cluster);

		try {
			log.debug("reading file for CLUSTER: "+cluster);
			int count = appController.countClusterItens(dest_folder, cluster);
			if (count < 0) {
				JMIRe.getInstance().updateObjectsCount(0);
				return;
			}

			JMIRe.getInstance().updateObjectsCount(count);
		} catch (Exception ex) {
			JMIRe.getInstance().updateObjectsCount(0);
			appController.logMessage("Error! Could not read " + dest_folder + File.separator + cluster+"_features.csv");
			ex.printStackTrace();
		}

		//log.debug("I'm done with change..."+num_of_selected_clusters);

	}      

}
