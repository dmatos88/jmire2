package br.mire.segmentation.threshold.controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.connectedcomponent.ConnectedComponentLabeler;
import org.openimaj.image.pixel.ConnectedComponent;
import org.openimaj.image.pixel.Pixel;
import org.openimaj.image.processing.convolution.FGaussianConvolve;
import org.openimaj.image.processing.threshold.OtsuThreshold;

import br.mire.segmentation.threshold.view.ThresholdSegmentationApp3;
import br.mire.utils.image.Binarize;

public class ThresholdSegmentationAppController extends javax.swing.JFrame implements
MouseListener {

	private static final long serialVersionUID = 1L;
	private static ThresholdSegmentationAppController appController = null;
	private String destDir;
	private String sourceDir;
	private float threshold;
	private FImage sample;
        private FImage mask;
	private List<ConnectedComponent> ccList;
	
	private static JFrame imageDisplayer = null;

	private final int nbins = 59;
	private final String masksFolder = "masks";
	private final String crops = "crops";

	private ThresholdSegmentationAppController() {

	}

	public static synchronized ThresholdSegmentationAppController getInstance() {
		if (appController == null) {
			appController = new ThresholdSegmentationAppController();
		}
		return appController;
	}

	public void run() {

		//file chooser
		this.sourceDir = this.chooseFile();
		
		while(sourceDir == null) {
			System.out.println("No source folder choosen.");
			this.sourceDir = this.chooseFile();
		}
		
		this.destDir = this.sourceDir;
		
		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				ThresholdSegmentationApp3.getInstance().populateJTree(appController.getFileList());
				ThresholdSegmentationApp3.getInstance().setVisible(true);
			}
		});
	}

	private String chooseFile() {
		//Create a file chooser
		JFileChooser fc = new JFileChooser();
		String dir = "";

		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int returnVal = fc.showOpenDialog(ThresholdSegmentationAppController.this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			dir = file.getAbsolutePath();
		} else {
			dir = null;
		}

		return dir;
	}

	public String changeSrcDir() {
		appController.sourceDir = appController.chooseFile();
		return appController.sourceDir;
	}

	public String changeDestDir() {
		appController.destDir = appController.chooseFile();
		return appController.destDir;
	}


	public List<String> getFileList() {

		List<String> filenames = new LinkedList<String>();
		File dir = new File(appController.sourceDir);

		if(dir.isDirectory()) {
			ThresholdSegmentationApp3.getInstance().displayMessage("Retrieving files for "+appController.sourceDir);
			for(File f : dir.listFiles()) {    			
				ThresholdSegmentationApp3.getInstance().displayMessage("Reading file "+f.getName());
				if(!f.isDirectory())
					filenames.add(f.getName());
			}
		}
		ThresholdSegmentationApp3.getInstance().displayMessage(filenames.size()+" files found");
		Collections.sort(filenames);
		return filenames;
	}
	
	public static void main(String[] args) throws IOException {
		

		MBFImage sample;
		String samplesPath = "/home/dmatos/workspace2/JMIRe/fields/";
		String field = "field[012]";
		String sample_count = "count[1]";
		String imgName = field + sample_count + ".jpg";
		sample = ImageUtilities.readMBF(new File(samplesPath + imgName));
		
		float sigma = 1.5f;
		
		FImage r = sample.getBand(0).normalise();		
		FImage g = sample.getBand(1).normalise();
		FImage b =  sample.getBand(2).normalise();
		
		FGaussianConvolve gauss = new FGaussianConvolve(sigma);
		gauss.processImage(r);
		float threshold = 0.6f * OtsuThreshold.calculateThreshold(r, 100);
		Binarize.processInPlace(r, threshold);
		
		gauss.processImage(g);
		threshold = 0.6f * OtsuThreshold.calculateThreshold(g, 100);
		Binarize.processInPlace(g, threshold);
		
		gauss.processImage(b);
		threshold = 0.6f * OtsuThreshold.calculateThreshold(b, 100);
		Binarize.processInPlace(b, threshold);
		
		r.addInplace(g.addInplace(b));			

		ImageUtilities.write(r, new File("otsu_thresh_result_normalized.jpg"));
		
		ConnectedComponentLabeler cclabeler = new ConnectedComponentLabeler(
				ConnectedComponentLabeler.Algorithm.TWO_PASS,
				threshold,
				ConnectedComponent.ConnectMode.CONNECT_4);	
		
		cclabeler.analyseImage(r);
		
		ThresholdSegmentationAppController appController = ThresholdSegmentationAppController.getInstance();
		
		appController.ccList = cclabeler.getComponents();
		

		System.out.println("list of components: "+appController.ccList.size());

		FImage mask;
		
		mask = new FImage(sample.getWidth(), sample.getHeight());
		mask.fill(0f);
		
		float colorInterval = 100.0f / appController.ccList.size(); 
		float color = colorInterval;
		for(ConnectedComponent cc : appController.ccList){
			System.out.println("color: "+color);
			for(Pixel p : cc.getPixels()){
				mask.setPixel(p.x, p.y, color);				
			}			
			color += colorInterval;
		}

		imageDisplayer = DisplayUtilities.display(mask);	
		imageDisplayer.setResizable(true);
		imageDisplayer.setSize(1200,600);
		
	}

	private void displayCCList() {

		ConnectedComponentLabeler cclabeler = new ConnectedComponentLabeler(
				ConnectedComponentLabeler.Algorithm.TWO_PASS,
				threshold,
				ConnectedComponent.ConnectMode.CONNECT_4);
		//appController.ccList = cclabeler.findComponents(sample);
		
		Binarize.processInPlace(sample, threshold);
		
		cclabeler.analyseImage(sample);
		
		appController.ccList = cclabeler.getComponents();
		
		System.out.println("list of components: "+appController.ccList.size());

		mask = new FImage(sample.width, sample.height);
		mask.fill(0f);
		
		float colorInterval = 100.0f / appController.ccList.size(); 
		float color = colorInterval;
		for(ConnectedComponent cc : appController.ccList){
			System.out.println("color: "+color);
			for(Pixel p : cc.getPixels()){
				mask.setPixel(p.x, p.y, color);				
			}			
			color += colorInterval;
		}

		imageDisplayer = DisplayUtilities.display(mask);
		imageDisplayer.setResizable(true);
		imageDisplayer.setSize(1200,600);
		
		MouseListener m = imageDisplayer.getContentPane().getComponent(0).getMouseListeners()[0];
		imageDisplayer.getContentPane().getComponent(0).removeMouseListener(m);
		imageDisplayer.getContentPane().getComponent(0).addMouseListener(this);
	}

	public void changeThresholdAndDisplayCC(float threshold){
		appController.threshold = threshold / 100.0f;
                appController.displayCCList();
	}

	public int selectImage(String filename) throws IOException {
		
		if(imageDisplayer != null) {
			imageDisplayer.dispose();
		}
		
		String absolutePath = appController.sourceDir + File.separatorChar + filename;
		
		ThresholdSegmentationApp3.getInstance().displayMessage("Processing image "+filename+" ...");
		
		File imageFile = new File(absolutePath);
		if (imageFile.exists()) {
			
			FImage image = ImageUtilities.readF(imageFile);
			appController.sample = image.clone();
			appController.threshold = OtsuThreshold.calculateThreshold(sample, appController.nbins);
			appController.threshold *= 0.7;
                        appController.displayCCList();
			ThresholdSegmentationApp3.getInstance().displayMessage("Done.");
			return (int) (threshold * 100.0);
		}
		return -1;
	}

	public boolean saveImage(String destFolder, FImage image){
		//TODO save image  and mask   
		return false;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		ThresholdSegmentationApp3.getInstance().displayMessage("Image clicked at "+e.getX()+", "+e.getY());
		int x = e.getX();
		int y = e.getY();
		if (x >= 0 && x < appController.sample.getWidth()
				&& y >= 0 && y < appController.sample.getHeight()) {
			ThresholdSegmentationApp3.getInstance().displayMessage("pixel value: "+appController.sample.getPixel(x, y));
			if(appController.mask.getPixel(x, y) > 0f){				
				for(ConnectedComponent cc : appController.ccList){
					if(cc.find(x, y)){
						FImage segmentedImage = new FImage(sample.width, sample.height);
						segmentedImage.fill(0f);
						mask = segmentedImage.clone();

						for(Pixel p : cc.getPixels()){
							segmentedImage.setPixel(p.x, p.y, appController.sample.getPixel(p));
							mask.setPixel(p.x, p.y, 1f);  
							//TODO save segment and mask to it's propper folders
						}
						imageDisplayer.dispose();
						
						/*
						imageDisplayer = DisplayUtilities.display(segmentedImage);
						imageDisplayer.setResizable(true);
						imageDisplayer.setSize(1200,600);
						*/
					}
				}
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		return;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		return;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		return;
	}

	@Override
	public void mouseExited(MouseEvent e) {
		return;
	}

}
