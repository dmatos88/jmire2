package br.mire.segmentation.threshold.controller;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class ThresholdSegmentationAppStarter {

	public static void main( String args[] ){

            //Schedule a job for the event dispatch thread:
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    //Turn off metal's use of bold fonts
                    UIManager.put("swing.boldMetal", Boolean.FALSE); 
                    ThresholdSegmentationAppController app = ThresholdSegmentationAppController.getInstance();
                    app.run();
                }
            });
	}
}
