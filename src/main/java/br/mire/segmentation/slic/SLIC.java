package br.mire.segmentation.slic;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.processing.convolution.FGaussianConvolve;

import br.mire.segmentation.edges.LoGEdgeDetection;
import br.mire.utils.image.CIELab;
import br.mire.utils.image.MIRePixel;
import br.mire.utils.math.Coord2D;

public class SLIC {

	private List<Node> seeds;
	private Node[][] imageAsNodes;
	//private int[][] edges;
	private int N;
	private int S;
	private double m;
	private float[][] floodFillAux;

	public SLIC() {
		seeds = new ArrayList<Node>(10);
		this.m = 4.0;
	}

	/**
	 * Color/Distance adherence
	 *
	 * @param m small for color priority, big for distance
	 */
	public void setM(double m) {
		this.m = m;
	}

	public double getM() {
		return m;
	}

	public final Node[][] getSuperpixels() {
		return this.imageAsNodes;
	}
	
	public final List<Node> getCenters(){
		return this.seeds;
	}

	/**
	 *
	 * @param S superpixel max height
	 * @param image
	 * @param edges binary matrix where 1 is an edge
	 */
	public void initializedSeeds(int S, final MBFImage image, int[][] edges) {

		//parameters of grid of cluster
		this.N = image.getWidth() * image.getHeight();
		this.S = S;
		imageAsNodes = new Node[image.getWidth()][image.getHeight()];

		int cx, cy, seedCounter = 0;

		CIELab labSpace = new CIELab();
		MIRePixel pixel_xy;

		for (int x = 0; x < image.getWidth(); x++) {
			for (int y = 0; y < image.getHeight(); y++) {

				pixel_xy = new MIRePixel();
				pixel_xy.setCoord(new Coord2D(x, y));
				pixel_xy.setColor(
						labSpace.convertFromRGB(
								image.getBand(0).getPixel(x, y), //red
								image.getBand(1).getPixel(x, y), //green
								image.getBand(2).getPixel(x, y) //blue
								));

				imageAsNodes[x][y] = new Node(pixel_xy);

				if (x % S == 0 && y % S == 0 && x != 0 && y != 0) {

					cx = x;
					cy = y;

					if (edges[x][y] == 1) {
						for (int j1 = x - 1; j1 < x + 1; j1++) {
							for (int j2 = y - 1; j2 < y + 1; j2++) {
								if (edges[j1][j2] == 0) {
									cx = j1;
									cy = j2;
									break;
								}
							}
						}
					}

					pixel_xy = new MIRePixel();
					pixel_xy.setCoord(new Coord2D(cx, cy));
					pixel_xy.setColor(
							labSpace.convertFromRGB(
									image.getBand(0).getPixel(cx, cy), //red
									image.getBand(1).getPixel(cx, cy), //green
									image.getBand(2).getPixel(cx, cy) //blue
									));

					Node seed = new Node(pixel_xy);
					seed.setLabel(seedCounter);
					seeds.add(seed);
					seedCounter++;
				}
			}
		}

	}
	
	
	private void floodFill(int x, int y, int label) {
		
		if(this.floodFillAux[y][x] == 1.0f) return;
		
		if(seeds.get(imageAsNodes[x][y].label).label != label) return;
		
		this.floodFillAux[y][x] = 1.0f;
		
		for(int i = -1; i < 2; i++) {
			for(int j = -1; j < 2; j++) {
				if(x + i >= 0 && y + j >= 0) {
					int next_x = x+i;
					int next_y = y+j;
					if(next_x < floodFillAux.length && next_y < floodFillAux[0].length) {
						floodFill(next_x, next_y, label);
					}
				}
			}
		}
	}
	

	public FImage getClusterMask(int x, int y) {

		
		int label = seeds.get(imageAsNodes[x][y].label).label;
		
		System.out.println("label "+label);
		
		if(label == -1) return null;
		
		this.floodFillAux = new float[imageAsNodes[0].length][imageAsNodes.length];
		
		floodFill(x, y, label);
		
		FImage image = new FImage(floodFillAux);
		
		/** For test purposes
		try {
			ImageUtilities.write(image, new File("mask.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		//*/
		
		return image;
	}

	public Node computeSuperpixel(Node center) {
		int centerX = (int)center.pixel.getCoord().getX();
		int centerY = (int)center.pixel.getCoord().getY();

		int xmax, ymax, xmin, ymin;
		double Lmax, Amax, Bmax, Lmin, Amin, Bmin;

		xmax = ymax = Integer.MIN_VALUE;
		xmin = ymin = Integer.MAX_VALUE;
		Lmax = Bmax = Amax = Double.MIN_VALUE;
		Lmin = Bmin = Amin = Double.MAX_VALUE;

		//System.out.println(centerX+" , "+centerY);
		for (int i = centerX - S; i < centerX + S - 1 && i < imageAsNodes.length; i++) {
			for (int j = centerY - S; j < centerY + S - 1 && j < imageAsNodes[0].length; j++) {
				if (i >= 0 && j >= 0) {
					double distFromCenter = center.calcDistance(imageAsNodes[i][j], S, m);
					if (distFromCenter < imageAsNodes[i][j].getDistance()) {
						imageAsNodes[i][j].setDistance(distFromCenter);
						imageAsNodes[i][j].setLabel(center.getLabel());

						xmax = Integer.max(i, xmax);
						xmin = Integer.min(i, xmin);
						ymax = Integer.max(j, ymax);
						ymin = Integer.min(j, ymin);
						Lmax = Double.max(imageAsNodes[i][j].pixel.getColor().getL_star(), Lmax);
						Lmin = Double.min(imageAsNodes[i][j].pixel.getColor().getL_star(), Lmin);
						Bmax = Double.max(imageAsNodes[i][j].pixel.getColor().getB_star(), Bmax);
						Bmin = Double.min(imageAsNodes[i][j].pixel.getColor().getB_star(), Bmin);
						Amax = Double.max(imageAsNodes[i][j].pixel.getColor().getA_star(), Amax);
						Amin = Double.min(imageAsNodes[i][j].pixel.getColor().getA_star(), Amin);
					}

				}
			}
		}

		CIELab color = new CIELab();
		color.setL_star((Lmax + Lmin) / 2.0);
		color.setB_star((Bmax + Bmin) / 2.0);
		color.setA_star((Amax + Amin) / 2.0);

		Coord2D coord = new Coord2D((xmax + xmin) / 2, (ymax + ymin) / 2);

		MIRePixel px = new MIRePixel();
		px.setColor(color);
		px.setCoord(coord);

		Node newCenter = new Node(px);
		newCenter.label = center.label;

		return newCenter;
	}


	public void compute(double epson, int maxIter) throws Exception {
		if (seeds == null) {
			throw new Exception("Seeds not initialized.");
		}

		int iter = 0;

		do {
			//System.out.println("iter "+iter);
			for (int i = 0; i < seeds.size(); i++) {
				Node newCenter = this.computeSuperpixel(seeds.get(i));
				seeds.set(i, newCenter);
			}
			iter++;
		} while (iter < maxIter);

	}

	public MBFImage getColoredSuperpixels() {

		MBFImage image = new MBFImage(imageAsNodes.length, imageAsNodes[0].length);

		int bandMax = 3;

		//float inc = 1.0f / seeds.size();
		//int label = 0;
		
		float[] rgb = null;

		for (int i = 0; i < imageAsNodes.length; i++) {
			for (int j = 0; j < imageAsNodes[0].length; j++) {

				if(imageAsNodes[i][j].label >= 0
						&& seeds.get(imageAsNodes[i][j].label).label >= 0) {
					
					Node seedNode = seeds.get(imageAsNodes[i][j].label);
					//label = seedNode.label;
					
					rgb = seedNode.pixel.getColor().convert2RGB();
					
					//System.out.println(rgb[0]+" + "+rgb[1]+" + "+rgb[2]);
				}
				
				//image.getBand(label % bandMax).setPixel(i, j, (label + 1) * inc * 0.7f);
				//image.getBand((label + 1) % bandMax).setPixel(i, j,(label + 1) * inc * 1.5f);
				//image.getBand((label + 2) % bandMax).setPixel(i, j,(label + 1) * inc * 1.5f);
			
				image.getBand(0).setPixel(i, j, rgb[0]);
				image.getBand(1).setPixel(i, j, rgb[1]);
				image.getBand(2).setPixel(i, j, rgb[2]);
			}
		}

		return image;
	}

	public static void main(String[] args) throws Exception {
		MBFImage img;
		String samplesPath = "/home/dmatos/workspace2/JMIRe/samples/";
		String field = "field[009]";
		String sample = "sample[007]";
		String imgName = field + sample + ".jpg";
		img = ImageUtilities.readMBF(new File(samplesPath + imgName));
		//pixel in 72 dpi is 0.35278mm length
		int S = 40; //72,54->20
		double m = 15.0;//72,54->5

		double error = 0.00000001;
		int maxiter = 10;

		SLIC slic = new SLIC();

		FGaussianConvolve gauss = new FGaussianConvolve((float) 1.5f);
		gauss.processImage(img.getBand(0));
		gauss.processImage(img.getBand(1));
		gauss.processImage(img.getBand(2));

		double sigma = 4;
		int[][] edges = LoGEdgeDetection.getEdges(img, sigma);

		slic.setM(m);

		slic.initializedSeeds(S, img, edges);

		slic.compute(error, maxiter);

		MBFImage superpixels = slic.getColoredSuperpixels();

		MBFImage blend = superpixels.clone();

		FImage edgesImg = new FImage(edges.length, edges[0].length);
		for (int i = 0; i < edges.length; i++) {
			for (int j = 0; j < edges[0].length; j++) {
				edgesImg.setPixel(i, j, (float) edges[i][j]);
				if (edges[i][j] == 1) {
					blend.getBand(0).setPixel(i, j, 1.0f);
					blend.getBand(1).setPixel(i, j, 1.0f);
					blend.getBand(2).setPixel(i, j, 1.0f);
				}
			}
		}

		//DisplayUtilities.display(superpixels).setResizable(true);
		DisplayUtilities.display(blend).setResizable(true);

		//ImageUtilities.write(superpixels, new File(field+sample+"superpixels.jpg"));
		//ImageUtilities.write(edgesImg,new File(field+sample+"edges.jpg"));
		//ImageUtilities.write(blend, new File(field+sample+"blend.jpg"));
		DisplayUtilities.display(superpixels);

	}
}
