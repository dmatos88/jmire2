package br.mire.segmentation.slic;

import br.mire.utils.image.CIELab;
import br.mire.utils.image.MIRePixel;
import br.mire.utils.math.Coord2D;

public class Node {
	protected MIRePixel pixel;
	protected int label;
	protected double distance;
	protected boolean checked;

	public Node() {
		this.label = -1;
		this.distance = Double.POSITIVE_INFINITY;
		this.pixel = new MIRePixel();
		this.checked = false;
	}

	public Node(MIRePixel px) {
		this.label = -1;
		this.distance = Double.POSITIVE_INFINITY;
		this.pixel = px;
		this.checked = false;
	}

	public boolean isChecked(){
		return checked;
	}

	public void setAsChecked(){
		checked = true;
	}

	public void setAsUnchecked(){
		checked = false;
	}

	/**
	 * 
	 * @param n other node to calculate distance
	 * @param S Math.sqrt(N/K)
	 * @param m user defined color threshold (small to color adherence, big to compactness)
	 * @return distance between this node e other node
	 */
	public double calcDistance(Node n, double S, double m) {
		CIELab nColor = n.pixel.getColor();
		Coord2D nCoord = n.pixel.getCoord();
		double dc = this.pixel.getColor().getColorEuclideanDistance(nColor);
		double ds = this.pixel.getCoord().getEuclideanDistance(nCoord);

		ds = ds / S;

		return Math.sqrt(dc*dc+ds*ds*m*m);		
	}

	public int getLabel() {
		return label;
	}

	public void setLabel(int newLabel) {
		this.label = newLabel;
	}

	public double getDistance() {
		return this.distance;
	}

	public void setDistance(double newDist) {
		this.distance = newDist;
	}

}
