package br.mire.segmentation.contour;

import br.mire.utils.math.Complex;

public class MyChainPoint {

	/* reverse order
	private static final Complex[] ccfactor = {
				new Complex(1, 0),
				new Complex(1, -1),
				new Complex(0,-1),
				new Complex(-1,-1),
				new Complex(-1, 0),
				new Complex(-1,1),
				new Complex(0,1),
				new Complex(1,1)
			}; 
	 */


	private static final Complex[] ccfactor = {
			new Complex(0,-1), 	//x, y-1
			new Complex(1, -1), //x+1, y-1
			new Complex(1, 0), 	//x+1, y
			new Complex(1,1),	//x+1, y+1
			new Complex(0,1),	//x, y+1
			new Complex(-1,1),	//x-1, y+1
			new Complex(-1, 0), //x-1, y
			new Complex(-1,-1) //x-1,y-1
	}; 


	public static Complex chainPoint(Complex u0, int cc) {
		Complex u1 = u0.plus(ccfactor[cc]); 
		return u1;
	}

	public static int ccFactor(Complex object_px, Complex background_px) {
		Complex chainPointCoord = background_px.minus(object_px);
		for(int i = 0; i < 8; i++) {
			if(chainPointCoord.equals(ccfactor[i])) return i;
		}
		return -1;
	}
}
