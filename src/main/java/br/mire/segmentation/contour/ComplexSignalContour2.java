package br.mire.segmentation.contour;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.swing.WindowConstants;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.processing.convolution.FGaussianConvolve;

import br.mire.utils.file_managers.DataStorage;
import br.mire.utils.math.Complex;
import br.mire.utils.ploters.ScatterPlot;

public class ComplexSignalContour2 {

	private double[] xSignal = null;
	private double[] ySignal = null;
	private int[] chain_code = null;

	public ComplexSignalContour2(FImage img, int initial_x, int initial_y) throws Exception{		
		this.getContour(img, initial_x, initial_y);
	}	

	public final double[] getXSignal() {
		return this.xSignal;
	}

	public final double[] getYSignal() {
		return this.ySignal;
	}
	
	public final int[] getChaincode() {
		return this.chain_code;
	}

	private void getContour(FImage img, int initial_x, int initial_y) throws Exception{

		Complex objectPx = null;
		Complex backgroundPx = null;
		
		boolean find = true;
		for(int y = initial_y; y < img.height; y++) {
			for(int x = initial_x; x < img.width; x++) {
				//binarize
				if(img.getPixel(x,y) <= 0.3f) img.setPixel(x, y, 0f);
				else img.setPixel(x, y, 1f);

				if(x-1 <= 0 || y-1 <= 0 || y+1 >= img.height-1 || x+1 >= img.width-1 ) {
					img.setPixel(x, y, 0f);
				}

				//find 1st px
				if(find && img.getPixel(x, y) == 1) {
					objectPx = new Complex(x, y);
					backgroundPx = new Complex(x-1, y);
					find = false;
				}
			}
		}

		LinkedList<Complex> u = new LinkedList<Complex>();
		u.add(backgroundPx);
		
		LinkedList<Integer> chaincode = new LinkedList<Integer>();
		
		boolean closed_contour = false;
		
		int cc = 1;		
		while(!closed_contour) {
			Complex next_px_2_check = MyChainPoint.chainPoint(objectPx, cc%8);
			
			int next_x = next_px_2_check.reIntValue();
			int next_y = next_px_2_check.imIntValue();
			
			if(next_x >= 0 && next_y >= 0 && next_x < img.getWidth() && next_y < img.getHeight() 
					&& img.getPixel(next_x, next_y) == 1) {
				//it's a object pixel
				objectPx = next_px_2_check;
				cc = MyChainPoint.ccFactor(objectPx, backgroundPx);
				
			} else {
				//it's a background pixel
				backgroundPx = next_px_2_check;
				u.add(backgroundPx);
				chaincode.add(cc%8);
				if(backgroundPx.equals(u.get(0))) closed_contour = true;
			}
			
			cc++;
		}		

		this.xSignal = new double[u.size()];
		this.ySignal = new double[u.size()];
		this.chain_code = new int[chaincode.size()+1];
		
		int i = 0;
		for(Complex c : u) {
			this.chain_code[i] = chaincode.get(i%chaincode.size());
			this.xSignal[i] = c.reIntValue();
			this.ySignal[i++] = c.imIntValue();
		}		
	}

	public static void saveData(String absolutePath, int[] x, int[] y) throws Exception {

		if(x.length != y.length) {
			//System.err.println("x and y must have same length");
			return;
		}

		Map<String, Integer[]> xy = new HashMap<String, Integer[]>();

		Integer[] newX = new Integer[x.length];
		Integer[] newY = new Integer[y.length];
		for(int i = 0; i < x.length; i++) {
			newY[i] = y[i];
			newX[i] = x[i];
		}

		xy.put("x", newX);
		xy.put("y", newY);

		DataStorage.storeData(xy, absolutePath);
	}

	public static void saveData(String absolutePath, Map<String, double[]> xy) {

	}


	public static void plotterTest() {
		String absolutePath = "/home/dmatos/samples/binaryObjects/s001b.jpeg";
		File f = new File(absolutePath);
		if(f.exists()) {
			FImage image;
			try {
				image = ImageUtilities.readF(f);
				//Map<String, double[]> xy = ComplexSignalContour.getContour(image, 1.1f);
				ComplexSignalContour2 contourSignal = new ComplexSignalContour2(image, 0, 0);

				final XYSeries contour = new XYSeries( "contour" );          
				for(int i = 0; i < contourSignal.getXSignal().length; i++ ) {
					contour.add( (double)contourSignal.getXSignal()[i] ,(double) contourSignal.getYSignal()[i] );
				}

				final XYSeriesCollection dataset = new XYSeriesCollection( );          
				dataset.addSeries( contour );
				ScatterPlot.scatterPlotDataset(dataset, "Complex Signal", "x", "y");

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			System.err.println(absolutePath+" not found");
		}
	}



	public static void main(String[] args) {
		ComplexSignalContour2.plotterTest();
	}
}