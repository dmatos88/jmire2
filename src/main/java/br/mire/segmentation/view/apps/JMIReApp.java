/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.mire.segmentation.view.apps;

import javax.swing.JPanel;

/**
 *
 * @author dmatos
 */
public interface JMIReApp {
    
    public void initialize(String dest_folder);
    
    public void selectImage(String filepath);
    
    public String getDestFolder();
    
    public JPanel getAppPanel();
    
    public String getAppName();
    
}
