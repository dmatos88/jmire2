package br.mire.segmentation.edges;

import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;

import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;

import br.mire.utils.image.CIELab;
import br.mire.utils.image.Matrix2FImage;

public class Sobel {
	
	public static double[][] process(double[][] imgMatrix, double threshold){
		
		int nlin = imgMatrix.length;
		int ncol = imgMatrix[0].length;
		double gx, gy;
		
		double[][] matrix = new double[nlin][ncol];
		
		for(int i = 0; i < nlin; i++) {
			for(int j = 0; j < ncol; j++) {
				
				if(i == 0 || j == 0 || i == nlin-1 || j == ncol-1)
					matrix[i][j] = 0;
				else {
					gx = -imgMatrix[i-1][j-1]-2.0*imgMatrix[i-1][j]-imgMatrix[i-1][j+1]
						+imgMatrix[i+1][j-1]+2.0*imgMatrix[i+1][j]+imgMatrix[i+1][j+1];
					
					gy = imgMatrix[i-1][j-1]+2.0*imgMatrix[i][j-1]+imgMatrix[i+1][j-1]
							-imgMatrix[i-1][j+1]-2.0*imgMatrix[i][j+1]-imgMatrix[i+1][j+1];
										
					matrix[i][j] = Math.sqrt(gx*gx+gy+gy);
					
					if(matrix[i][j] < threshold) matrix[i][j] = 0.0;
				}
				
			}
		}
	
		return matrix;
	}

	
	public static double[][] getColorMagnitudeMatrix(MBFImage image) {
		double[][] matrix = new double[image.getWidth()][image.getHeight()];
		
		CIELab pixel0 = new CIELab();
		pixel0.setA_star(0);
		pixel0.setB_star(0);
		pixel0.setL_star(0);
		double magnitude = 0.0;
		
		for(int i = 0; i < image.getWidth(); i++) {
			for(int j = 0; j < image.getHeight(); j++) {
				double red = image.getBand(0).getPixel(i,j);
				double green = image.getBand(1).getPixel(i,j);
				double blue = image.getBand(2).getPixel(i,j);
				
				magnitude = pixel0.getColorEuclideanDistance(pixel0.convertFromRGB(red, green, blue));
				//magnitude = pixel.getColorChebyshevDistance(pixel.convertFromRGB(red, green, blue));
				matrix[i][j] = magnitude;
				//matrix[i][j] = pixel0.convertFromRGB(red, green, blue).getL_star();
			}
		}
		
		return matrix;
	}

	public static void main(String[] args) throws IOException {
		MBFImage img;
		//img = ImageUtilities.readMBF(new File("/home/dmatos/workspace2/JMIRe/samples/s007.jpeg"));
		img = ImageUtilities.readMBF(new File("/home/dmatos/workspace2/testSegment.jpeg"));
		
		double threshold = 3;
		
		double[][] imgMatrix = Sobel.getColorMagnitudeMatrix(img);
		
		double[][] matrix = Sobel.process(imgMatrix, threshold);

		FImage img2 = Matrix2FImage.matrix2Image(matrix);
		
		DisplayUtilities.display(img2).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
}
