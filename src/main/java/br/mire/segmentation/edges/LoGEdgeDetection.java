package br.mire.segmentation.edges;

import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;

import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;

import com.frank.math.Complex;
import com.frank.math.FFT2D;

import br.mire.utils.image.CIELab;
import br.mire.utils.image.Matrix2FImage;
import br.mire.utils.math.Fftshift;
import br.mire.utils.math.FrequencyGaussian;

public class LoGEdgeDetection {

	public static Complex[][] getFourierLoG2d(int nlines, int mcols, double sigma){

		Complex[][] LoG = new Complex[nlines][mcols];

		int N = nlines;
		int M = mcols;

		int j = 0;
		for(double r = -Math.floor(M/2); r <= M - Math.floor(M/2); r+=1, j++) {
			int i = 0;
			for(double s = -Math.floor(N/2); s <= N - Math.floor(N/2); s+=1, i++) {	
				double R = (s*s+r*r);
				LoG[i][j] = new Complex(  
						-1.0 * (1.0 / (Math.PI * sigma*sigma*sigma*sigma))
						* (1.0-(R/(2*sigma*sigma)))
						* Math.exp(-R/(2*sigma*sigma)), 0);
			}
		}

		return LoG;
	}

	public static Complex[][] process(final Complex[][] matrix, double sigma) {

		Complex[][] matrix2 = matrix;

		Complex[][] maskB = FrequencyGaussian.getFourierLoG2d(matrix2.length, matrix2[0].length, sigma);

		maskB = FFT2D.fft(maskB);

		maskB = Fftshift.fftshift(maskB);

		//DisplayUtilities.display(TP02_LoG.matrix2Image(maskB)).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		matrix2 = FFT2D.fft(matrix2);

		matrix2 = Fftshift.fftshift(matrix2);

		//DisplayUtilities.display(TP02_LoG.matrix2Image(matrix2)).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		for(int i = 0; i < matrix2.length; i++) {
			for(int j = 0; j < matrix2[0].length; j++) {
				matrix2[i][j] =  matrix2[i][j].multiply(maskB[i][j]);
			}
		}

		//DisplayUtilities.display(TP02_LoG.matrix2Image(matrix2)).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);;

		matrix2 = FFT2D.ifft( Fftshift.ifftshift(matrix2) );

		matrix2 = Fftshift.fftshift(matrix2);

		return matrix2;
	}

	public static int[][] zeroCrossBorders(Complex[][] matrix, double threshold) {
		
		int[][] edges = new int[matrix.length][matrix[0].length];
		
		for(int i = 0; i < matrix.length-1; i++)
			for(int j = 0; j < matrix[0].length-1; j++)
				if(matrix[i][j].real < threshold && matrix[i][j+1].real >= threshold
				|| matrix[i][j].real >= threshold && matrix[i][j+1].real < threshold
				|| matrix[i][j].real >= threshold && matrix[i+1][j].real < threshold
				|| matrix[i][j].real < threshold && matrix[i+1][j].real >= threshold
						)
					edges[i][j] = 1;
				else
					edges[i][j] = 0;
		
		return edges;
	}

	public static Complex[][] getColorMagnitudeMatrix(MBFImage image) {
		Complex[][] matrix = new Complex[image.getWidth()][image.getHeight()];

		CIELab pixel0 = new CIELab();
		pixel0.setA_star(0);
		pixel0.setB_star(0);
		pixel0.setL_star(0);
		double magnitude = 0.0;

		for(int i = 0; i < image.getWidth(); i++) {
			for(int j = 0; j < image.getHeight(); j++) {
				double red = image.getBand(0).getPixel(i,j);
				double green = image.getBand(1).getPixel(i,j);
				double blue = image.getBand(2).getPixel(i,j);

				magnitude = pixel0.getColorEuclideanDistance(pixel0.convertFromRGB(red, green, blue));
				//magnitude = pixel.getColorChebyshevDistance(pixel.convertFromRGB(red, green, blue));
				matrix[i][j] = new Complex(magnitude);
				//matrix[i][j] = new Complex(pixel0.convertFromRGB(red, green, blue).getL_star());
			}
		}

		return matrix;
	}

	public static int[][] getEdges(MBFImage img, double sigma){
		float threshold = 0.001f;

		Complex[][] matrixTemp = LoGEdgeDetection.getColorMagnitudeMatrix(img);

		Complex[][] matrix = LoGEdgeDetection.process(matrixTemp, sigma);

		return LoGEdgeDetection.zeroCrossBorders(matrix, threshold);
		
	}

	public static void main(String[] args) throws IOException {
		MBFImage img;
		img = ImageUtilities.readMBF(new File("/home/dmatos/samples/field[005]sample[005].jpg"));
		//img = ImageUtilities.readMBF(new File("/home/dmatos/workspace2/testSegment.jpeg"));
		double sigma = 2;
		float threshold = 0.001f;

		int[][] edges = LoGEdgeDetection.getEdges(img, sigma);

		FImage img2 = Matrix2FImage.matrix2Image(edges);

		DisplayUtilities.display(img2).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

}



