package br.mire.segmentation.local_thresholding;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.ColourSpace;
import org.openimaj.image.connectedcomponent.ConnectedComponentLabeler;
import org.openimaj.image.pixel.ConnectedComponent;
import org.openimaj.image.pixel.Pixel;
import org.openimaj.image.processing.convolution.FGaussianConvolve;
import org.openimaj.ml.clustering.FloatCentroidsResult;
import org.openimaj.ml.clustering.assignment.HardAssigner;
import org.openimaj.ml.clustering.kmeans.FloatKMeans;

import br.mire.ml.classifier.tcruzi.Prediction;
import br.mire.segmentation.controllers.LocalThresholdingController;

public class LocalThresholding {

	class Px{
		double value;
		int x;
		int y;
		public Px(int x, int y, float value) {
			this.x = x;
			this.y = y;
			this.value = value;
		}
	}

	private float[][] floodFillMatrix;
	private FImage auxImage;

	public FImage circleUnderMeanThresh(FImage image, int radius, float threshold) {

		double mean = 0;
		double count = 0;
		int xi, yj;

		FImage result = new FImage(image.width, image.height);

		for(int x = radius+1; x < image.width-radius; x++) {
			for(int y = radius+1; y < image.height-radius; y++) {
				mean = 0;
				count = 0;
				for(int i = -radius; i < radius; i++) {
					for(int j = -radius; j < radius; j++) {
						xi = x + i;
						yj = y + j;
						double e = (xi-x)*(xi-x)+(yj-y)*(yj-y);
						if(Math.sqrt(e) < radius && image.getPixel(xi,yj) < 1f) {
							mean = mean + image.getPixel(xi, yj);
							count += 1;
						}
					}
				}				
				if(image.getPixel(x,y) <= mean/count * threshold) result.setPixel(x, y, 1f);
			}
		}

		return result;

	}
	public FImage maskedUnderMeanThresh(FImage image, FImage mask, int radius, float threshold) {

		double mean = 0;
		int xi, yj;

		FImage result = new FImage(image.width, image.height);

		List<Px> pxList = new LinkedList<Px>();

		for(int x = 0; x < image.width; x++) {
			for(int y = 0; y < image.height; y++) {
				mean = 0;
				pxList.clear();
				for(int i = -radius; i < radius; i++) {
					for(int j = -radius; j < radius; j++) {
						xi = x + i;
						yj = y + j;
						if(xi >= 0 && xi < image.width && yj < image.height && yj >= 0 && mask.getPixel(xi, yj) > 0) {							
							mean = mean + image.getPixel(xi, yj);
							pxList.add(new Px(xi,yj,image.getPixel(xi, yj)));
						}
					}
				}
				double count = pxList.size();
				if(count > 0) {
					mean = mean/count;
					for(Px px : pxList) {
						if(	px.value <= mean * threshold) {
							result.setPixel(px.x, px.y, 1.f);
						}
					}
				}	

			}
		}

		return result;

	}

	public FImage maskedAboveMeanThresh(FImage image, FImage mask, int radius, float threshold) {

		double mean = 0;
		int xi, yj;

		FImage result = new FImage(image.width, image.height);

		List<Px> pxList = new LinkedList<Px>();

		for(int x = 0; x < image.width; x++) {
			for(int y = 0; y < image.height; y++) {
				mean = 0;
				pxList.clear();
				for(int i = -radius; i < radius; i++) {
					for(int j = -radius; j < radius; j++) {
						xi = x + i;
						yj = y + j;
						if(xi >= 0 && xi < image.width && yj < image.height && yj >= 0 && mask.getPixel(xi, yj) > 0) {							
							mean = mean + image.getPixel(xi, yj);
							pxList.add(new Px(xi,yj,image.getPixel(xi, yj)));
						}
					}
				}
				double count = pxList.size();
				if(count > 0) {
					mean = mean/count;
					for(Px px : pxList) {
						if(	px.value >= mean * threshold) {
							result.setPixel(px.x, px.y, 1.f);
						}
					}
				}	

			}
		}

		return result;


	}


	public FImage maskedUnderCrossMeanThresh(FImage image, FImage mask, int radius, float threshold) {

		double mean = 0;
		int xi, yj;

		FImage result = new FImage(image.width, image.height);

		List<Px> pxList = new LinkedList<Px>();

		for(int x = 0; x < image.width; x++) {
			for(int y = 0; y < image.height; y++) {
				mean = 0;
				pxList.clear();
				for(int i = -radius; i < radius; i++) {
					xi = x + i;
					yj = y;
					if(xi >= 0 && xi < image.width && yj < image.height && yj >= 0 && mask.getPixel(xi, yj) > 0) {							
						mean = mean + image.getPixel(xi, yj);
						pxList.add(new Px(xi,yj,image.getPixel(xi, yj)));
					}
				}
				for(int j = -radius; j < radius; j++) {
					xi = x;
					yj = y + j;
					if(xi >= 0 && xi < image.width && yj < image.height && yj >= 0 && mask.getPixel(xi, yj) > 0) {							
						mean = mean + image.getPixel(xi, yj);
						pxList.add(new Px(xi,yj,image.getPixel(xi, yj)));
					}
				}
				double count = pxList.size();
				if(count > 0) {
					mean = mean/count;
					for(Px px : pxList) {
						if(	px.value <= mean * threshold) {
							result.setPixel(px.x, px.y, 1.f);
						}
					}
				}	

			}
		}

		return result;

	}


	/**
	 * 
	 * @param imageMatrix is a matrix with the grayscale values
	 */
	public float[][] meanThresh(final FImage image, int radius, float sigma, float threshold, float cutoff) {

		int width = image.getWidth();
		int height = image.getHeight();

		float[][] imageMatrix = new float[height][width];

		FImage auxImg = image.clone();
		FGaussianConvolve gauss = new FGaussianConvolve(sigma);
		gauss.processImage(auxImg);

		int x_pos, y_pos;
		float region_mean, region_counter;
		float px_value;

		final int window_shift = 1;

		for(int x = 0; x < width; x+=window_shift) {
			for(int y = 0; y < height; y+=window_shift) {
				region_mean = 0.0f;
				region_counter = 0.0f;

				for(int wx = -radius; wx < radius; wx++) {
					for(int wy = -radius; wy < radius; wy++) {
						x_pos = x + wx;
						y_pos = y + wy;
						if(x_pos > 0 && y_pos > 0 && x_pos < width && y_pos < height) {
							px_value = auxImg.getPixel(x_pos, y_pos);
							region_mean += px_value;
							region_counter += 1.0f;
						}
					}
				}

				if(region_counter > 0) {
					region_mean /= region_counter;			

					for(int wx = -radius; wx < radius; wx++) {
						for(int wy = -radius; wy < radius; wy++) {
							x_pos = x + wx;
							y_pos = y + wy;
							if(x_pos > 0 && y_pos > 0 && x_pos < width && y_pos < height) {
								px_value = auxImg.getPixel(x_pos, y_pos);
								//System.out.println(px_value/region_mean);

								if(px_value <= cutoff) {
									if(px_value / region_mean <= threshold){
										imageMatrix[y_pos][x_pos] = 1.0f;

									}
									else imageMatrix[y_pos][x_pos] = 0f;
								} 
								else {
									imageMatrix[y_pos][x_pos] = 0f;
								}
							}
						}
					}
				}
			}
		}
		return imageMatrix;
	}

	private void floodFill(int x, int y, int limit, int counter) {

		if(counter >= limit) return;

		if(this.auxImage.getPixel(x,y) <= 0f) return;

		if(this.floodFillMatrix[y][x] == 1.0f) return;

		this.floodFillMatrix[y][x] = 1.0f;

		for(int i = -1; i < 2; i++) {
			for(int j = -1; j < 2; j++) {
				if(x + i >= 0 && y + j >= 0) {
					int next_x = x+i;
					int next_y = y+j;
					if(next_y < floodFillMatrix.length && next_x < floodFillMatrix[0].length) {
						floodFill(next_x, next_y, limit, counter + 1);
					}
				}
			}
		}
	}


	public FImage getClusterMask(FImage image, int x, int y, int limit) throws StackOverflowError{


		if(image == null) return null;

		auxImage = image.clone();

		if(auxImage.getPixel(x,y) == 0f) {
			System.out.println("x:"+x+" y:"+y+" [x,y]:"+auxImage.getPixel(x,y));
			return null;
		}

		this.floodFillMatrix = new float[image.getHeight()][image.getWidth()];

		try {
			floodFill(x, y, limit, 0);
		}	catch(StackOverflowError e) {
			System.err.println("ERROR: Flood Fill Stack Overflow");
			return null;
		}


		FImage output = new FImage(floodFillMatrix);
		
		ConnectedComponentLabeler cclabeler = new ConnectedComponentLabeler(
				ConnectedComponentLabeler.Algorithm.TWO_PASS,
				0.5f,
				ConnectedComponent.ConnectMode.CONNECT_8);	

		cclabeler.analyseImage(output);

		List<ConnectedComponent>ccList = cclabeler.getComponents();
		
		LocalThresholdingController.getInstance().logMessage("ccList size: "+ccList.size());

		output.zero();
		
		ConnectedComponent cc = ccList.get(0);
		
		Iterator<Pixel> pit = cc.getPixels().iterator();
		
		while(pit.hasNext()) {
			Pixel p = pit.next();
			output.setPixel(p.x, p.y, 1.0f);
		}
		
		/** For test purposes
		try {
			ImageUtilities.write(image, new File("mask.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		//*/

		return output;
	}

	public static void main(String[] args) throws Exception {


		MBFImage img;
		String samplesPath = "/home/dmatos/samples_fields/";
		String field = "field[004]";
		String sample = "count[3]";
		String imgName = field + sample + ".jpg";
		img = ImageUtilities.readMBF(new File(samplesPath + imgName)); //.extractCenter(1500, 1000, 400, 400);;

		img = ColourSpace.convert(img, ColourSpace.CIE_Lab);

		long start = System.currentTimeMillis();


		FImage L_star_image = img.getBand(0);
		L_star_image.normalise();

		//DisplayUtilities.display(L_star_image).setTitle("L");

		FImage a_star_image = img.getBand(1);
		a_star_image.normalise();		
		a_star_image.inverse();

		//DisplayUtilities.display(a_star_image).setTitle("A");

		FImage b_star_image = img.getBand(2);
		b_star_image.normalise();

		//DisplayUtilities.display(b_star_image).setTitle("B");


		FloatKMeans cluster = FloatKMeans.createExact(6);

		float[][] imageData = img.getPixelVectorNative(new float[img.getWidth() * img.getHeight()][3]);

		FloatCentroidsResult result = cluster.cluster(imageData);

		HardAssigner<float[],?,?> assigner = result.defaultHardAssigner();
		for (int y=0; y<img.getHeight(); y++) {
			for (int x=0; x<img.getWidth(); x++) {
				float[] pixel = img.getPixelNative(x, y);
				int centroid = assigner.assign(pixel);
				img.setPixelNative(x, y, result.centroids[centroid]);
			}
		}

		//img = ColourSpace.convert(img, ColourSpace.RGB);
		DisplayUtilities.display(img);
		/*
		FImage edges = SUSANEdgeDetector.smoothCircularSusan(b_star_image, 0.5, 0.5, 34);
		DisplayUtilities.display(edges);
		 */

		/*
		 * SOBEL
		 * 
		FSobel fsobel = new FSobel(2f);
		fsobel.analyseImage(L_star_image);
		FImage ldx = fsobel.dx.clone();
		FImage ldy = fsobel.dy.clone();
		fsobel = new FSobel(2f);
		fsobel.analyseImage(a_star_image);
		FImage adx = fsobel.dx.clone();
		FImage ady = fsobel.dy.clone();		
		fsobel = new FSobel(2f);
		fsobel.analyseImage(b_star_image);		
		FImage bdx = fsobel.dx.clone();
		FImage bdy = fsobel.dy.clone();

		//DisplayUtilities.display(dx).setTitle("Dx");;
		//DisplayUtilities.display(dy).setTitle("Dy");;

		ldx.addInplace(ldy);
		adx.addInplace(ady);
		bdx.addInplace(bdy);

		FImage final_image = ldx.add(adx.add(bdx));
		 */

		/*
		 // Otsu
		float lthresh = OtsuThreshold.calculateThreshold(L_star_image, 100);
		float athresh = OtsuThreshold.calculateThreshold(a_star_image, 100);
		float bthresh = OtsuThreshold.calculateThreshold(b_star_image, 100);

		L_star_image.threshold(lthresh);
		a_star_image.threshold(athresh);
		b_star_image.threshold(bthresh);

		DisplayUtilities.display(L_star_image).setSize(800, 600);
		DisplayUtilities.display(a_star_image).setSize(800, 600);
		DisplayUtilities.display(b_star_image).setSize(800, 600);
		//*/

		/*

		LocalThresholding local_thresh = new LocalThresholding();

		int radius = 40;
		float sigma = 1.5f;
		float threshold = 0.6f;
		float cutoff = 0.54f;

		FImage L_star_result = new FImage(local_thresh.meanThresh(L_star_image, radius, sigma, threshold, cutoff));
		FImage a_star_result = new FImage(local_thresh.meanThresh(a_star_image, radius, sigma, threshold, cutoff));
		FImage b_star_result = new FImage(local_thresh.meanThresh(b_star_image, radius, sigma, threshold, cutoff));

		FImage summed = b_star_result.add(L_star_result.add(a_star_result));

		ConnectedComponentLabeler cclabeler = new ConnectedComponentLabeler(
				ConnectedComponentLabeler.Algorithm.TWO_PASS,
				cutoff,
				ConnectedComponent.ConnectMode.CONNECT_4);	

		cclabeler.analyseImage(summed);


		List<ConnectedComponent>ccList = cclabeler.getComponents();		
		 */
		//TODO apresentar os testes realizados aqui na dissertação

		/*
		System.out.println("list of components: "+ ccList.size());

		long total_time = (System.currentTimeMillis()-start) / 1000;

		System.out.println("Processing time in seconds: "+total_time);

		MBFImage mask;

		mask = new MBFImage(img.getWidth(), img.getHeight());

		float colorInterval = 100.0f / ccList.size(); 
		float color = colorInterval;
		int layer = 0;
		for(ConnectedComponent cc : ccList){
			//System.out.println("color: "+color);
			for(Pixel p : cc.getPixels()){
				mask.getBand(layer%3).setPixel(p.x, p.y, color);
			}			
			color += colorInterval;
			layer++;
		}

		DisplayUtilities.display(mask);	

		ImageUtilities.write(mask, new File("local_thresh_result.jpg"));
		 */

		//DisplayUtilities.display(L_star_image);
		//DisplayUtilities.display(a_star_image);
		//DisplayUtilities.display(b_star_image);

		/*
		MBFImage temp = new MBFImage(width, height);

		temp.getBand(0).addInplace(L_star_image);
		temp.getBand(1).addInplace(a_star_image);
		temp.getBand(2).addInplace(b_star_image);

		DisplayUtilities.display(temp);
		 */
		//DisplayUtilities.display(summed);



		//ImageUtilities.write(summed, new File("local_thresh_result.jpg"));


		//System.exit(0);
	}
}
