/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.mire.segmentation.application;

import java.awt.CardLayout;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreeSelectionModel;

import org.apache.log4j.Logger;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;

import br.mire.features.extraction.GeometricFeatures;
import br.mire.segmentation.contour.ComplexSignalContour2;
import br.mire.segmentation.controllers.JMIReController;
import br.mire.segmentation.view.apps.JMIReApp;
import br.mire.segmentation.view.apps.LocalThresholdingPanel;
import br.mire.segmentation.view.apps.SLICPanel;
import br.mire.utils.math.MetricConversions;
import br.mire.utils.ploters.ScatterPlot;
import br.mire.utils.tests.TCruziGiemsaTester;

/**
 *
 * @author dmatos
 */
public class JMIRe extends javax.swing.JFrame {

	static Logger log = Logger.getLogger(JMIRe.class.getName());
	
	private static final long serialVersionUID = 1L;
	private DefaultMutableTreeNode rootNode = null;
	private static JMIRe appViewer = null;

	public enum Applications {
		SLIC("SLIC", new SLICPanel()),
		LOCAL_THRESH("Local Thresholding", new LocalThresholdingPanel());

		private final String name;
		private final JMIReApp app;

		Applications(String name, JMIReApp app) {
			this.name = name;
			this.app = app;
		}

		public String getName() {
			return this.name;
		}

		public JMIReApp getApp() {
			return this.app;
		}

	}

	private JMIRe() {
		//Java Microscope Image Recognition Segmentation and Features Extraction
		super("JMIRe SFX");

		initComponents();		
		this.jTextAreaLog.setAutoscrolls(true);
		appViewer = this;
		log.info("Initializing JMIRe application...");

	}    


	public void run() {

        
		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				JMIReController jcontroller = JMIReController.getController(Applications.LOCAL_THRESH);

				log.debug("STARTED");
				jcontroller.readAppConfigFile();
				log.debug("FINISHED");
				
				log.debug("Using src folder: "+jcontroller.getSrcDir());				

				log.debug("Updating file list... (it may take a while");
				List<String> filelist = jcontroller.getFileList();
				log.debug("Done");

				JMIRe.getInstance().populateJTree(filelist);
				JMIRe.getInstance().initializeApps();

				getInstance().jTextFieldSrcFolder.setText(jcontroller.getSrcDir());

				JMIRe.getInstance().setVisible(true);

				CardLayout card = (CardLayout) jPanelCards.getLayout();
				card.show(jPanelCards, Applications.LOCAL_THRESH.getName());
			}
		});
	}

	public void changeApplication(String appName){
		CardLayout card = (CardLayout) jPanelCards.getLayout();
		card.show(jPanelCards, appName);		
	}

	private void initializeApps() {

		MenuAppsListener menuAppsListener = new MenuAppsListener();	

		System.out.print("Populating menus: ");

		JMenuItem slic = new JMenuItem(Applications.SLIC.getName());
		slic.addMouseListener(menuAppsListener);
		jMenuApplications.add(slic);

		SLICPanel slicPanel = (SLICPanel)Applications.SLIC.getApp();
		slicPanel.initialize(Applications.SLIC.getApp().getDestFolder());
		jPanelCards.add(slicPanel, Applications.SLIC.getName());

		JMenuItem thresh = new JMenuItem(Applications.LOCAL_THRESH.getName());                
		thresh.addMouseListener(menuAppsListener);

		jMenuApplications.add(thresh);

		LocalThresholdingPanel threshPanel = (LocalThresholdingPanel) Applications.LOCAL_THRESH.getApp();
		threshPanel.initialize(Applications.LOCAL_THRESH.getApp().getDestFolder());
		jPanelCards.add(threshPanel, Applications.LOCAL_THRESH.getName());

		//this.changeSelectedCluster();

		System.out.println("done.");
	}

	public static synchronized JMIRe getInstance() {
		if (appViewer == null) {
			appViewer = new JMIRe();
		}
		return appViewer;
	}

	/**
	 * Also removes the salt around the field
	 * @param field
	 * @return
	 * @throws Exception
	 */
	public double getScale(FImage img) throws Exception{

		int x = 1;
		int y = img.height / 2;

		//DisplayUtilities.display(img);
		
		ComplexSignalContour2 contourSignal = new ComplexSignalContour2(img, x, y);

		/*			

		final XYSeries contour = new XYSeries( "contour" );          
		for(int i = 0; i < contourSignal.getXSignal().length; i++ ) {
			contour.add( (double)contourSignal.getXSignal()[i] ,(double) contourSignal.getYSignal()[i] );
		}

		final XYSeriesCollection dataset = new XYSeriesCollection( );          
		dataset.addSeries( contour );
		ScatterPlot.scatterPlotDataset(dataset, "JMIRe.getScale(FImage field)", "x", "y");
		//*/

		double[] axes = GeometricFeatures.principalAxis(contourSignal);

		double field_number = (Integer) JSpinnerFieldNumber.getValue();
		double objective_mag = (Integer) jSpinnerObjectiveMagnification.getValue();
		double ocular_mag = (Integer) jSpinnerOcularMagnification.getValue();


		/*
		double real_field_diameter = field_number / objective_mag;
		double pixel_side_in_mm = real_field_diameter / axes[0]; ///((axes[0] + axes[1]) / 2.0);
		double pixel_side_in_um = pixel_side_in_mm * objective_mag * ocular_mag;
		 */
		
		double pixel_side_in_um = MetricConversions.getScale(img, field_number, objective_mag, ocular_mag);
		
		double mice_rbc_diameter_in_px = 80; 

		displayMessage("Pixel size: "+pixel_side_in_um+"um");		

		displayMessage("Mice RBC diameter: "+mice_rbc_diameter_in_px * pixel_side_in_um);

		return pixel_side_in_um;
	}

	public void populateJTree(List<String> filenames) {

		getInstance().rootNode = new DefaultMutableTreeNode();
		for (String filename : filenames) {
			rootNode.add(new DefaultMutableTreeNode(filename));
		}		

		getInstance().jTree1 = new javax.swing.JTree(rootNode);

		getInstance().jTree1.setSelectionModel(new DefaultTreeSelectionModel());

		getInstance().jTree1.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) getInstance().jTree1.getLastSelectedPathComponent();

				/* if nothing is selected */
				if (node.equals(getInstance().rootNode)) {
					return;
				}

				/* retrieve the node that was selected */
				Object nodeInfo = node.getUserObject();

				String imageFName = (String) nodeInfo;

				try {
					JMIRe.getInstance().displayMessage("Image " + imageFName + " selected.");

					JMIReController.getController().getApplicationPanel().selectImage(imageFName);

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});

		getInstance().jTree1.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		getInstance().jScrollPane1.setViewportView(getInstance().jTree1);
		getInstance().update(getInstance().getGraphics());
	}

	public void displayMessage(String msg) {
		this.jTextAreaLog.append(msg + "\n");		
		getInstance().update(getInstance().getGraphics());
	}

	public int getObjectiveMagnification() {
		return (Integer) jSpinnerObjectiveMagnification.getValue();
	}

	public void updateObjectsCount(int count) {
		jTextFieldObjectCounter.setText("" + count);
	}


	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaLog = new javax.swing.JTextArea();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanelCards =  new JPanel(new CardLayout())
        ;
        panel1 = new java.awt.Panel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jTextFieldSrcFolder = new javax.swing.JTextField();
        jTextFieldObjectCounter = new javax.swing.JTextField();
        jSpinnerObjectiveMagnification = new javax.swing.JSpinner();
        jSpinnerOcularMagnification = new javax.swing.JSpinner();
        JSpinnerFieldNumber = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuApplications = new javax.swing.JMenu();
        jMenuTools = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(900, 600));
        setSize(new java.awt.Dimension(100, 500));
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.X_AXIS));

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jTextAreaLog.setColumns(20);
        jTextAreaLog.setRows(5);
        jScrollPane2.setViewportView(jTextAreaLog);

        jSplitPane1.setBottomComponent(jScrollPane2);

        jSplitPane2.setDividerLocation(256);

        jPanelCards.setVisible(true);
        jPanelCards.setLayout(new java.awt.CardLayout());
        jSplitPane2.setLeftComponent(jPanelCards);

        jTree1.setMinimumSize(new java.awt.Dimension(80, 68));
        jScrollPane1.setViewportView(jTree1);

        jTextFieldSrcFolder.setEditable(false);
        jTextFieldSrcFolder.setText("/home/dmatos/src");

        jTextFieldObjectCounter.setEditable(false);
        jTextFieldObjectCounter.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldObjectCounter.setText("0");

        jSpinnerObjectiveMagnification.setModel(new javax.swing.SpinnerNumberModel(100, 10, 100, 10));

        jSpinnerOcularMagnification.setModel(new javax.swing.SpinnerNumberModel(10, 1, 40, 10));

        JSpinnerFieldNumber.setModel(new javax.swing.SpinnerNumberModel(20, 1, 50, 1));

        jLabel5.setText("Objeto #");

        jLabel8.setText("Objective magnification");

        jLabel7.setText("Ocular magnification");

        jLabel9.setText("Field number");

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldObjectCounter, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSpinnerOcularMagnification, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JSpinnerFieldNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSpinnerObjectiveMagnification, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldSrcFolder, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE))
                .addContainerGap())
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {JSpinnerFieldNumber, jSpinnerObjectiveMagnification, jSpinnerOcularMagnification, jTextFieldObjectCounter});

        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldObjectCounter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jSpinnerObjectiveMagnification, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jSpinnerOcularMagnification, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(JSpinnerFieldNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldSrcFolder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {JSpinnerFieldNumber, jSpinnerObjectiveMagnification, jSpinnerOcularMagnification, jTextFieldObjectCounter});

        jSplitPane2.setRightComponent(panel1);

        jSplitPane1.setLeftComponent(jSplitPane2);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 730, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel2);

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenuApplications.setText("Applications");
        jMenuBar1.add(jMenuApplications);

        jMenuTools.setText("Tools");
        jMenuBar1.add(jMenuTools);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSpinner JSpinnerFieldNumber;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenuApplications;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuTools;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanelCards;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner jSpinnerObjectiveMagnification;
    private javax.swing.JSpinner jSpinnerOcularMagnification;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTextArea jTextAreaLog;
    private javax.swing.JTextField jTextFieldObjectCounter;
    private javax.swing.JTextField jTextFieldSrcFolder;
    private javax.swing.JTree jTree1;
    private java.awt.Panel panel1;
    // End of variables declaration//GEN-END:variables
}
