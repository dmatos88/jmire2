/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.mire.segmentation.application;

import br.mire.segmentation.controllers.JMIReController;
import java.awt.CardLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JMenuItem;

/**
 *
 * @author dmatos
 */
public class MenuAppsListener implements MouseListener{

  @Override
	public void mouseClicked(MouseEvent e) {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void mousePressed(MouseEvent e) {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			if (e.getButton() == MouseEvent.BUTTON1) {
				JMenuItem menuItem = (JMenuItem) e.getSource();
				String appName = menuItem.getText();

				if (appName.equals(JMIRe.Applications.SLIC.getName())) {
					JMIRe.getInstance().displayMessage("Switching to SLIC segmentation app");  
					JMIReController.getController(JMIRe.Applications.SLIC).readAppConfigFile();
                                        JMIRe.getInstance().changeApplication(JMIRe.Applications.SLIC.getName());
				} else if (appName.equals(JMIRe.Applications.LOCAL_THRESH.getName())) {
					JMIRe.getInstance().displayMessage("Switching to Local Thresholding by average value app");
					JMIReController.getController(JMIRe.Applications.LOCAL_THRESH).readAppConfigFile();
                                        JMIRe.getInstance().changeApplication(JMIRe.Applications.LOCAL_THRESH.getName());
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void mouseExited(MouseEvent e) {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
    
}
