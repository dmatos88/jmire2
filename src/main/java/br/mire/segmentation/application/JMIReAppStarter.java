package br.mire.segmentation.application;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import br.mire.segmentation.application.JMIRe.Applications;

public class JMIReAppStarter {

	public static void main( String args[] ){

            //Schedule a job for the event dispatch thread:
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    //Turn off metal's use of bold fonts
                    UIManager.put("swing.boldMetal", Boolean.FALSE); 
                    JMIRe app = JMIRe.getInstance();
                    app.run();
                }
            });
	}
}
