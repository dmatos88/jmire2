mvn install:install-file -DgroupId=net.semanticmetadata -DartifactId=lire -Dversion=0.9.4beta -Dpackaging=jar -Dfile=./lib/net.semanticmetadata/lire/0.9.4beta/lire-0.9.4beta.jar

mvn install:install-file -DgroupId=com.frank.math -DartifactId=math4j -Dversion=1.0 -Dpackaging=jar -Dfile=./lib/math4j.jar

mvn install:install-file -DgroupId=jcl -DartifactId=jcl -Dversion=1.0 -Dpackaging=jar -Dfile=./lib/jclUser.jar

mvn install:install-file -DgroupId=JavaMI -DartifactId=JavaMI -Dversion=1.0 -Dpackaging=jar -Dfile=./lib/JavaMI/JavaMI/1.0/JavaMI-1.0.jar

mvn clean

mvn install -X

mvn package

cp target/JMIRe-1.0-SNAPSHOT-jar-with-dependencies.jar jmire.jar


