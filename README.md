# JMIRe

Java 1.8
Maven

## Instalação

Execute o script mvn-install-dependencies.sh

## Arquivo de configuração

Edite o arquivo jmire_local_thresh.properties.

SRC_DIR: caminho das imagens que serão processadas
DEST_DIR: caminho dos resultados das features

### Exemplo de arquivo de configuração

CLUSTERS=5
CLUSTER_0=KINETOPLAST
CLUSTER_1=KINETOPLAST_CELL_BODY
CLUSTER_2=NUCLEUS
CLUSTER_3=NUCLEUS_CELL_BODY
CLUSTER_4=T_CRUZI
DEST_DIR=/home/dmatos/dest2
SRC_DIR=/home/dmatos/tcruzi_data/fields/train

## Execução da aplicação de segmentação e extração de descritores

mvn exec:java@jmire